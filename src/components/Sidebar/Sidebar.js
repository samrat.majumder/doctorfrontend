/*!

=========================================================
* Light Bootstrap Dashboard React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { useLocation, NavLink ,Link} from "react-router-dom";


import { Nav,Image } from "react-bootstrap";
import {icon} from "react-bootstrap"
import logo from "assets/img/reactlogo.png";

function Sidebar({ color, image, routes }) {
  const location = useLocation();
  const a=JSON.parse(localStorage.getItem('token'));



// const d = () =>{
//   if(a.user.role === "Admin"){
//     return(
//       <li>
      
//       <NavLink
      
//       to={"/admin" + "/umanagement"}
//       className="nav-link"
//         activeClassName="active"
      
      
//       >
//       <i className="nc-icon nc-alien-33"/>
      
//       UserManagement
//       </NavLink>
//       </li>
      
//         )
//   }


// }



  const activeRoute = (routeName) => {
    return location.pathname.indexOf(routeName) > -1 ? "active" : "";
  };
  return (
    <div className="sidebar" data-image={image} data-color={color}>
      <div
        className="sidebar-background"
        style={{
          backgroundImage: "url(" + image + ")",
        }}
      />
      <div className="sidebar-wrapper">
        <div className="logo d-flex align-items-center justify-content-start">
          <a
            href="https://www.creative-tim.com?ref=lbd-sidebar"
            className="simple-text logo-mini mx-1"
          >
            <div className="logo-img">
              <Image
                src={require("assets/img/abc.png").default}
                alt="..."
                roundedCircle 
              />
            </div>
          </a>
          <a className="simple-text" href="http://www.creative-tim.com">
          A.B.C Centre
          </a>
        </div>
        <Nav>
      
          {/* {routes.map((prop, key) => {
            if (!prop.redirect)
              return (
                <li
                  className={
                    prop.upgrade
                      ? "active active-pro"
                      : activeRoute("/admin"+ "/dashboard")

                      // : activeRoute(prop.layout + prop.path)
                  }
                  key={key}
                > */}
                <li>
<NavLink
                  
                  to={"/admin" + "/dashboard"}
                  className="nav-link"
                    activeClassName="active"


                  >
               <i className="nc-icon nc-chart-pie-35"/>

                    Dashboard
                  </NavLink>
</li>



 {
routes.slice(0,1).map(() => {
  console.log(a.user.role);
  console.log("user calling me");
  if(a.user.role === "Admin")
  
  return(
<li>

<NavLink

to={"/admin" + "/umanagement"}
className="nav-link"
  activeClassName="active"


>
<i className="nc-icon nc-alien-33"/>

UserManagement
</NavLink>
</li>

  )
  
}


  )

}

{/* <li>

                  <NavLink
                  
                  to={"/admin" + "/umanagement"}
                  className="nav-link"
                    activeClassName="active"


                  >
               <i className="nc-icon nc-alien-33"/>

               UserManagement
                  </NavLink>
</li> */}
<li>
                  <NavLink
                  
                  to={"/admin" + "/pmanagement"}
                  className="nav-link"
                    activeClassName="active"


                  >
               <i className="nc-icon nc-alien-33"/>

               PatientManagement
                  </NavLink>  
</li>
<li>
                  <NavLink
                  to={"/admin" + "/followup"}
                  className="nav-link"
                    activeClassName="active"


                  >
               <i className="nc-icon nc-vector"/>

               Follow-up View
                  </NavLink>
                  </li>

                  {/* <NavLink
                    to={prop.layout + prop.path}
                    className="nav-link"
                    activeClassName="active"
                  >
                  
                
                 
                    <i className={prop.icon} />
                      <p>{prop.name}</p> 

                
                   

                  </NavLink> */}
                {/* </li> */}
                
              {/* );
            return null;
           })} */}
        </Nav>
      </div>
    </div>
  );
}

export default Sidebar;
