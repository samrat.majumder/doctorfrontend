

// import React, { Component } from 'react'
// import axios from 'axios'
// import { login } from './UserFunctions'


// class Login1 extends Component{
//     constructor() {
//         super()
//         this.state = {
//           email: '',
//           password: '',
//           errors: {}
//         }

//     this.onChange = this.onChange.bind(this)
//     this.onSubmit = this.onSubmit.bind(this)
        
//     }


//     onChange(e) {
//         this.setState({ [e.target.name]: e.target.value })
//       }
    
    
//       onSubmit(e) {
//         e.preventDefault()
    
//         const user = {
//           email: this.state.email,
//           password: this.state.password
//         }



//     //   axios.post(`https://abcapi.vidaria.in/userlogin`, { user })
//     //   .then(res => {
//     //     this.props.history.push(`/admin/dashboard`)
//     //     console.log(res);
//     //     console.log(res.data);
//     //   })
//         // }

//     login(user).then(res => {
      
//         this.props.history.push(`/home`)
      
//     })
      



//     render() 
    
        
//     {
//         return(
//             <div className="container">
//                 <div className="row">
//                     <div className="col-md-6">
//             <form>
//             <h3>Sign In</h3>

//             <div className="form-group">
//                 <label>Email address</label>
//                 <input type="email" className="form-control" placeholder="Enter email" />
//             </div>

//             <div className="form-group">
//                 <label>Password</label>
//                 <input type="password" className="form-control" placeholder="Enter password" />
//             </div>

//             <div className="form-group">
//                 <div className="custom-control custom-checkbox">
//                     <input type="checkbox" className="custom-control-input" id="customCheck1" />
//                     <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
//                 </div>
//             </div>

//             <button type="submit" className="btn btn-primary btn-block">Submit</button>
//             <p className="forgot-password text-right">
//                 Forgot <a href="#">password?</a>
//             </p>
//         </form>
//         </div>
//         </div>
//         </div>
//         )

//     }

// }
// export default Login;



// import React, { Component } from 'react'
// import { login } from './UserFunctions'

// class Login extends Component {
//   constructor() {
//     super()
//     this.state = {
//       email: '',
//       password: '',
//       errors: {}
//     }

//     this.onChange = this.onChange.bind(this)
//     this.onSubmit = this.onSubmit.bind(this)
//   }

//   onChange(e) {
//     this.setState({ [e.target.name]: e.target.value })
//   }


//   onSubmit(e) {
//     e.preventDefault()

//     const user = {
//       email: this.state.email,
//       password: this.state.password
//     }


    
//     login(user).then(res => {
      
//         this.props.history.push(`/admin/dashboard`)
      
//     })
//   }

//   render() {
//     return (

//       <div className="container">
//         <div className="row">
//           <div className="col-md-6 mt-5 mx-auto">
//             <form noValidate onSubmit={this.onSubmit}>
//               <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
//               <div className="form-group">
//                 <label htmlFor="email">Email address</label>
//                 <input
//                   type="email"
//                   className="form-control"
//                   name="email"
//                   placeholder="Enter email"
//                   value={this.state.email}
//                   onChange={this.onChange}
//                 />
//               </div>
//               <div className="form-group">
//                 <label htmlFor="password">Password</label>
//                 <input
//                   type="password"
//                   className="form-control"
//                   name="password"
//                   placeholder="Password"
//                   value={this.state.password}
//                   onChange={this.onChange}
//                 />
//               </div>
//               <button
//                 type="submit"
//                 className="btn btn-lg btn-primary btn-block"
//               >
//                 Sign in
//               </button>
//             </form>
//           </div>
//         </div>
//       </div>
//     )
//   }
// }

// export default Login














import React from "react"
import {Redirect} from "react-router-dom"
import axios from "axios"
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Table
} from "react-bootstrap";
// import './login.css'
import './app.css';


export default class Login extends React.Component{

    constructor(){
        super()
        let loggedIn = false
       
        
        const token = localStorage.getItem("token")
        if(token) loggedIn = true

        this.state = {
            email: "",
            password: "",
            loggedIn,
            error: ""
        }
        this.onChange =  this.onChange.bind(this)
        this.formSubmit = this.formSubmit.bind(this)
    }

    opensweetalert1()
    {
      Swal.fire({
        title: 'provide correct Email',
        position: 'top-end',
     icon: 'warning', 
    showConfirmButton: false,
    timer: 1200
        
      })
    }
  



    opensweetalert2()
    {
      Swal.fire({
        title: 'provide correct password',
        position: 'top-end',
     icon: 'warning', 
    showConfirmButton: false,
    timer: 1200
        
      })
    }


    opensweetalert()
    {
      Swal.fire({
        title: 'Server Down Please restart server',
        text: "fail",
        type: 'fail',
        icon: 'error',
     
        
      })
    }
  



    opensweetalert3()
    
    {
    const a=JSON.parse(localStorage.getItem('token'));
    console.log(a.user.username)
      Swal.fire({
        title:"Welcome "+a.user.username,
        position: 'top-end',
     icon: 'success', 
    showConfirmButton: false,
    timer: 1200
        
      })
    }








    onChange(ev){
        this.setState({
            [ev.target.name]: ev.target.value
        })
    }




    async formSubmit(ev){
        ev.preventDefault()
        const {email, password} = this.state
        try {
            const token = await axios.post("https://abcapi.vidaria.in/userlogin?X-AUTH=abc123", {email, password}).catch(error => {
                if (!error.response) {
                  // network error
                  this.errorStatus = this.opensweetalert();
              } else {
                  this.errorStatus = error.response.data.message;
              }
          
              })
            console.log(token)

            const a = token.data
            
            localStorage.setItem("token", JSON.stringify(token.data))

            if (a.user) 
            {
                console.log("logged in")
                this.setState({
                    loggedIn: true
                })

            }
            else if(a.message) {
                console.log("not logged in")
                this.setState({
                    loggedIn: false
                })
                if (a.message == "Wrong Password") {
                    this.opensweetalert2()
                }else if(a.message == "Wrong Email") {
                    this.opensweetalert1()
                }else if(a.message == "Fail") {
                    this.opensweetalert3()
                }
            }
        

            
        } catch (err) {
            this.setState({
                error: err.message
            })
        }
    }









    render(){
   
       
        // if(this.state.loggedIn === true){
        //     const a=JSON.parse(localStorage.getItem('token'));
        //     const admin="admin@gmail.com";
        //     if (admin===a.user.email){
        //         console.log(a.user.username)
            
        //         console.log("im here")
        //         this.opensweetalert3();
        //         return <Redirect to="/admin/dashboard" />

        //     }else{
        //         return("here will be user dashboard")
        //     }
   
            
        // }



        if(this.state.loggedIn === true){
            
            console.log("im here")
            this.opensweetalert3();
            return <Redirect to="/admin/dashboard" />
            
        }

        return(


            <div className="maincontainer">
            <div class="container-fluid">
                <div class="row no-gutter">
                   
                    <div class="col-md-6 d-none d-md-flex bg-image"></div>
                    
                    <div class="col-md-6 bg-light">
                        <div class="login d-flex align-items-center py-5">
                           
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-10 col-xl-7 mx-auto">
                                        <h3 class="display-4">A.B.C Centre!</h3>
                                        <p class="text-muted mb-4">Dedicated to Humanity</p>
                                        <form onSubmit={this.formSubmit}>
                                            <div class="form-group mb-3">
                                                <input id="inputEmail" type="email" placeholder="Email address" required="" autofocus="" class="form-control rounded-pill border-0 shadow-sm px-4"
                            
                                             value={this.state.email} onChange={this.onChange} name="email" 
                                                
                                                
                                                />
                                            </div>
                                            <div class="form-group mb-3">
                                                <input 
                                                id="inputPassword" type="password" placeholder="Password" required="" class="form-control rounded-pill border-0 shadow-sm px-4 text-primary" 
                                                value={this.state.password} onChange={this.onChange} name="password"
                                                />
                                            
                                            
                                            </div>
                                            <div class="custom-control custom-checkbox mb-3">
                                                <input id="customCheck1" type="checkbox" checked class="custom-control-input" />
                                                {/* <label for="customCheck1" class="custom-control-label">Remember password</label> */}
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Sign in</button>
                                            <div class="text-center d-flex justify-content-between mt-4"><p> Road No.1, Banjara Hills,
Hyderabad-500034    <a href="#" class="font-italic text-muted"> 
                                                    <u>contact us </u></a></p></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>



//       <div className="container">
//         <div className="row">
//           <div className="col-md-6 mt-5 mx-auto">
// <section class="forms-section"/>
//   <div class="forms">
//     <div class="form-wrapper is-active">
//       <button type="button" class="switcher switcher-login">
//        Please  Login
//         <span class="underline"></span>
//       </button>
//       <form class="form form-login" onSubmit={this.formSubmit}>
//         <fieldset>
//           <p>Please, enter your email and password for login.</p>
//           <div class="input-block">
//             <label for="login-email">E-mail</label>
//             <input  type="email"
//           placeholder="email" value={this.state.email} onChange={this.onChange} name="email"required />
//           </div>
//           <div class="input-block">
//             <label for="login-password">Password</label>
//             <input  type="password" placeholder="password" value={this.state.password} onChange={this.onChange} name="password" required />
//           </div>
//         </fieldset>
//         <button type="submit" class="btn-login">Login</button>
//       </form>
//     </div>
//    </div>


//             </div>
//             </div>
//         </div>
        )
    }
}

























// {/* main */}
 {/* <Form onSubmit={this.formSubmit}>

<Form.Group>
<Form.Label>Username</Form.Label><br></br>
<Form.Control
          type="email"
          placeholder="email" value={this.state.email} onChange={this.onChange} name="email"></Form.Control>
</Form.Group>


<Form.Group>
<Form.Label>Password</Form.Label><br></br>
<Form.Control
          type="password" placeholder="password" value={this.state.password} onChange={this.onChange} name="password"></Form.Control>
</Form.Group>

<Button variant="primary" type="submit">
    Submit
  </Button>
                {this.state.error}
            </Form>   */}



{/* end main */}