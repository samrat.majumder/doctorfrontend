import React from "react";

import axios from "axios";
import Modal from "react-bootstrap/Modal";
import Main from "../views/FollowupForm/Main2"
import { Link } from "react-router-dom";

// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
} from "react-bootstrap";

//Bootstrap and jQuery libraries
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery';


class FollowupManagement extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:[],
      

    }

  }

  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide })
}  



clearTable = () => {
  this.setState({ users: [] })
}


  componentDidMount(){

    axios.get(`https://abcapi.vidaria.in/allrfpatientdetails?X-AUTH=abc123`)
    .then((res)=>{
      console.log(res)
      $('#example').DataTable().destroy();
      this.setState({
users:res.data.rheumatology_followup_patient
   
      
        
      }, () => {
        $('#example').DataTable();
    })
    })
    axios.get( `https://abcapi.vidaria.in/allrpatientdetails?X-AUTH=abc123`)
    .then((res)=>{
      console.log(res)
      this.setState({
        user:res.data.rheumatology_patient,
      
        
      })
    })



    // axios.get( `https://abcapi.vidaria.in/allfollowuprheumadetails?pid=2`)
    // .then((res)=>{
    //   console.log(res)
    //   this.setState({
    //     users:res.data.RheumatologyFollowup,
      


    $(document).ready(function () {
      $('#example').DataTable(

      );
    });



        
    //   })
    // })
  }
  // delete(id){
  //   axios.delete(`https://abcapi.vidaria.in/userdelete?X-AUTH=abc123&${this.props.match.params.id}`)
  //   .then(()=>{
  //     this.componentDidMount();
  //   })
  // }


  delete(id,e){
    var del=" Rheumatology Follow-up Patient deleted"
    const a=JSON.parse(localStorage.getItem('token'));
    console.log(a.user.username)
  
    e.preventDefault()
    Swal.fire({
      title: 'Do you want to delete the changes?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Delete`,
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('user deleted!', '', 'success')
          axios.delete(`https://abcapi.vidaria.in/followuprheumadelete?X-AUTH=abc123&id=${id}`)
          axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`,{
  
            activity :del,
            user_id:a.user.id,
      
          })
          .then(() => {
          this.componentDidMount();
  
          });
      } else if (result.isDenied) {
        Swal.fire('user record is safe !', '', 'info')
      }
    })
  }

  render()

 {
  return (
    <>


<Modal 
                
                size="lg"
                
                show={this.state.showHide}>
                    <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
                 </Modal.Header>
                    <Modal.Body>


<Main />

                    </Modal.Body>
                    <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer>
                </Modal>


      <Container fluid>
        <div className="container">
          <div className="row" >
{/*            
      <Button variant="info" className="btn-fill" onClick={() => this.handleModalShowHide()} type="submit" name="action" style={{ float: "right" }}>
                Add Patient Followup
                </Button>  */}
                </div>
                </div>
                <br></br>
        <Row>
          <Col md="12">
            <Card className="strpied-tabled-with-hover">
              <Card.Header>
                <Card.Title as="h4">Rheumatology Follow-up PatientManagement</Card.Title>
                <p className="card-category">
                  Here is a Patient data
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
              <table id="example" className="table table-striped table-bordered table-sm row-border hover mb-5">                  <thead>
                    <tr>
                    <th className="border-0">User ID</th>
                    <th className="border-0">Code</th>

                     <th className="border-0">Patient Name</th>
                     <th className="border-0">Patient ID</th>
                      <th className="border-0">Phone</th>
                      <th className="border-0">Address</th>
                      <th className="border-0">Date of Subscribe</th>
                      <th className="border-0">View</th>
                      <th className="border-0">Delete</th>
                      <th className="border-0">Add-attachments</th>
                      <th className="border-0">Prescription</th>






            
                    </tr>
                  </thead>
                  <tbody>
                  {
            this.state.users.map(user=>
              <tr key={user.id}>
               <td>{user.user.id}</td>

                <td>{user.code}</td>
                <td>{user.pname}</td>
                <td>{user.pid}</td>

                <td>{user.phone}</td>
                <td>{user.address}</td>
                <td>{user.date}</td>
                


                <td>
                <Link to={`rfollowpatient/edit/${user.pid}`}>
             <Button  variant="primary" className="btn-fill" type="submit" name="action">Edit</Button>
             </Link>
                </td>



                <td>
                <Button onClick={(e)=>this.delete(user.id ,e)} variant="danger" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Delete
                </Button>
                </td>
                <td>
                <Link to={`rfpatient/edit/${user.pid}`}>
                <Button  variant="warning" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Add Attchments
                </Button>
                </Link>
                </td>
                <td>
                <Link to={`rfpatientprint/edit/${user.pid}`}>
                <Button  variant="success" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Print
                </Button>
                </Link>
                </td>
              </tr>
              )
          }

                  </tbody>
                </table>
              </Card.Body>
            </Card>
          </Col>
          {/* <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">Table on Plain Background</Card.Title>
                <p className="card-category">
                  Here is a subtitle for this table
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <thead>
                    <tr>
                      <th className="border-0">ID</th>
                      <th className="border-0">Name</th>
                      <th className="border-0">Salary</th>
                      <th className="border-0">Country</th>
                      <th className="border-0">City</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Dakota Rice</td>
                      <td>$36,738</td>
                      <td>Niger</td>
                      <td>Oud-Turnhout</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Minerva Hooper</td>
                      <td>$23,789</td>
                      <td>Curaçao</td>
                      <td>Sinaai-Waas</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Sage Rodriguez</td>
                      <td>$56,142</td>
                      <td>Netherlands</td>
                      <td>Baileux</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Philip Chaney</td>
                      <td>$38,735</td>
                      <td>Korea, South</td>
                      <td>Overland Park</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Doris Greene</td>
                      <td>$63,542</td>
                      <td>Malawi</td>
                      <td>Feldkirchen in Kärnten</td>
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Mason Porter</td>
                      <td>$78,615</td>
                      <td>Chile</td>
                      <td>Gloucester</td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col> */}
        </Row>
      </Container>
    </>
  );
}
}

export default FollowupManagement;




// class App extends React.Component {
//   constructor(props){
//     super(props);
//     this.state ={
//       users:[],
//       id:0,
//       username: "",
//       email: "",  
//       phone: "",
//       password: "",
//       role:"",
//       speciality:""
//     }

//   }
//   componentDidMount(){
//     axios.get("https://abcapi.vidaria.in/alluserdetails")
//     .then((res)=>{
//       this.setState({
//         users:res.data.user,
//         id:0,
//         username:'',
//         email:'',
//         phone:'',
//         password:'',
//         role:'',
//         speciality:'',
        
//       })
//     })
//   }
//   submit(event,id){
//     event.preventDefault();
//     if(id === 0){
//       axios.post("https://abcapi.vidaria.in/adduser",{
//         username:this.state.username,
//         email:this.state.email,
//         phone:this.state.phone,
//         password:this.state.password,
//         role:this.state.role,
//         speciality:this.state.speciality,
//       })
//       .then((res)=>{
//         this.componentDidMount();
//       })
//     }else{
//       axios.put(`https://abcapi.vidaria.in/userupdate/${id}`,{
//         id:this.state.id,
//         username:this.state.username,
//         email:this.state.email,
//         phone:this.state.phone,
//         password:this.state.password,
//         role:this.state.role,
//         speciality:this.state.speciality,
//       }).then(()=>{
//         this.componentDidMount();
//       })

//     }

//   }
//   delete(id){
//     axios.delete(`https://abcapi.vidaria.in/userdelete/${id}`)
//     .then(()=>{
//       this.componentDidMount();
//     })
//   }
//   edit(id){
//     axios.get(`https://abcapi.vidaria.in/userdetails/${id}`)
//     .then((res)=>{
//       console.log(res.data.user);
//       this.setState({
//         id:res.data.user.id,
//         username:res.data.user.username,
//         email:res.data.user.email,
//         phone:res.data.user.phone,
//         password:res.data.user.password,
//         role:res.data.user.role,
//         speciality:res.data.user.speciality,
//       })
//     })
//   }
//   render(){
//   return (
//     <div className="container" >
    
//     <div className="row">
    
//         <form onSubmit={(e)=>this.submit(e,this.state.id)}>
        
//         <label>Name</label>
//           <input onChange={(e)=>this.setState({username:e.target.value})} name="uname" value={this.state.username} type="text"   />
        

        
//           <label>email</label>
//           <input onChange={(e)=>this.setState({email:e.target.value})} name="email" value={this.state.email}  type="email"   />
        



        
//           <label>phone</label>
//           <input onChange={(e)=>this.setState({phone:e.target.value})} name="phone" value={this.state.phone} type="number"   />
        


        
//           <label>password</label>
//           <input onChange={(e)=>this.setState({password:e.target.value})} name="password" value={this.state.password}  type="password"   />
        


        
//           <label>role</label>
//           <input onChange={(e)=>this.setState({role:e.target.value})} name="role" value={this.state.role}  type="text"   />
        



        


        
//           <label>speciality</label>
//           <input onChange={(e)=>this.setState({speciality:e.target.value})} name="speciality" value={this.state.speciality} type="text"   />
        



        



//         <button className="btn waves-effect waves-light right" type="submit" name="action">Submit
//           <i className="material-icons right">send</i>
//         </button>
//         </form>
//       </div>
      
      
//       <table>
//         <thead>
//           <tr>
//               <th>Name</th>
//               <th>Email</th>
//               <th>Phone</th>
//               <th>Password</th>
//               <th>Role</th>
//               <th>Speciality</th>
//               <th>Date of Subscribe</th>
//               <th>Edit</th>
//               <th>Delete</th>
//           </tr>
//         </thead>

//         <tbody>
//           {
//             this.state.users.map(user=>
//               <tr key={user.id}>
//                 <td>{user.username}</td>
//                 <td>{user.email}</td>
//                 <td>{user.phone}</td>
//                 <td>{user.password}</td>
//                 <td>{user.role}</td>
//                 <td>{user.speciality}</td>
//                 <td>{user.dateofsub}</td>


//                 <td>
//                 <button onClick={(e)=>this.edit(user.id)} class="btn waves-effect waves-light" type="submit" name="action">
//                   <i class="material-icons">edit</i>
//                 </button>
//                 </td>
//                 <td>
//                 <button onClick={(e)=>this.delete(user.id)} class="btn waves-effect waves-light" type="submit" name="action">
//                   <i class="material-icons">delete</i>
//                 </button>
//                 </td>
//               </tr>
//               )
//           }
          
//         </tbody>
//       </table>
      
    
    
//     </div>
//   );
//   }
// }

