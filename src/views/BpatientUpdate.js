import React from "react";
import axios from 'axios'
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Image,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import Swal from 'sweetalert2'
import video from '../assets/final.mp4'
import { Link } from "react-router-dom";


class User extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      products: [],
      users:[],
      id:'',
      pid: '',
      date: '',
      code: '',
      place:'',
      pname:'',
      fhname:'',
      age:'',
      sex:'',
      education:'',
      address:'',
      phone:'',
      district:'',
      state:'',
      area:'',
      referred_by:'',
      occupation:'',
      ethnic:'',
      marital_status:'',
      marital_status_years :'',
      menses_frequency:'',
      menses_loss:'',
      menarche_years :'',
      hystrectomy :'',
      hystrectomy_years:'',
      menopause:'',
      menopause_years:'',
      children:'',
      children_male:'',
      children_female:'',
      abortions:'',
      abortions_number:'',
      abortions_cause:'',
      breastfed:'',
      breastfed_years:'',
      current_lactation:'',
      contraception_methods:'',
      contraception_methods_type:'',
      hormone_treatment:'',
      addiction:'',
      tobacco:'',
      tobacco_years:'',
      smoking:'',
      smoking_years:'',
      alcohol:'',
      alcohol_years:'',
      family_history:'',
      comorbidities:'',
      breastlump:'',
      breastlump_location:'',
      breastlump_size:'',
      overlying_skin:'',
      axillarylump:'',
      axillarylump_side:'',
      matted:'',
      axillarylump_size:'',
      nipple_discharge:'',
      nipple_discharge_frequency:'',
      nipple_discharge_colour:'',
      mastalgia:'',
      mastitis:'',
      ulcer:'',
      nipple_inversion:'',
      others:'',
      duration:'',
      pasthistory:'',
      surgical_history:'',
      drug_history:'',
      drug_allergy:'',
      drug_allergy_type:'',
      bowelhabit:'',
      bladderhabit:'',
      sleep:'',
      appetite:'',
      weight:'',
      height:'',
      bmi:'',
      bp:'',
      pulse:'',
      temp:'',
      respiratory_rate:'',
      health_condition:'',
      examination_remarks:'',
      bra_size:'',
      usg:'',
      mmg:'',
      mri:'',
      fnac:'',
      core_biopsy:'',
      incision_biopsy:'',
      investigation_remarks:'',
      blood_investigation:'',
      diagnosis:'',
      treatment_plan:'',
      user_id:''
    }

  }

  opensweetalert()
  {
    Swal.fire({
      title: 'Patient Updated',
      text: "success",
      type: 'success',
      
    }).then(function() {
      window.location = "/admin/dashboard";
  });
  }




  componentDidMount(){
    axios.get(`https://abcapi.vidaria.in/bpatientdetails?X-AUTH=abc123&pid=${this.props.match.params.id}`)
    .then((res)=>{
      console.log(res)
      this.setState({
        id:res.data.bpatient.id,

        pid:res.data.bpatient.pid,
        date:res.data.bpatient.date,
        code:res.data.bpatient.code,
        place:res.data.bpatient.place,
        pname:res.data.bpatient.pname,
        fhname:res.data.bpatient.fhname,
        age:res.data.bpatient.age,
        sex:res.data.bpatient.sex,
        education:res.data.bpatient.education,
        address:res.data.bpatient.address,
        phone:res.data.bpatient.phone,
        district:res.data.bpatient.district,
        state:res.data.bpatient.state,
        area:res.data.bpatient.area,
        referred_by:res.data.bpatient.referred_by,
        occupation:res.data.bpatient.occupation,
        ethnic:res.data.bpatient.ethnic,
        marital_status:res.data.bpatient.marital_status,
        marital_status_years :res.data.bpatient.marital_status_years,
        menses_frequency:res.data.bpatient.menses_frequency,
        menses_loss:res.data.bpatient.menses_loss,
        menarche_years :res.data.bpatient.menarche_years,
        hystrectomy :res.data.bpatient.hystrectomy,
        hystrectomy_years:res.data.bpatient.hystrectomy_years,
        menopause:res.data.bpatient.menopause,
        menopause_years:res.data.bpatient.menarche_years,
        children:res.data.bpatient.children,
        children_male:res.data.bpatient.children_male,
        children_female:res.data.bpatient.children_female,
        abortions:res.data.bpatient.abortions,
        abortions_number:res.data.bpatient.abortions_number,
        abortions_cause:res.data.bpatient.abortions_cause,
        breastfed:res.data.bpatient.breastfed,
        breastfed_years:res.data.bpatient.breastfed_years,
        current_lactation:res.data.bpatient.current_lactation,
        contraception_methods:res.data.bpatient.contraception_methods,
        contraception_methods_type:res.data.bpatient.contraception_methods_type,
        hormone_treatment:res.data.bpatient.hormone_treatment,
        addiction:res.data.bpatient.addiction,
        tobacco:res.data.bpatient.tobacco,
        tobacco_years:res.data.bpatient.tobacco_years,
        smoking:res.data.bpatient.smoking,
        smoking_years:res.data.bpatient.smoking_years,
        alcohol:res.data.bpatient.alcohol,
        alcohol_years:res.data.bpatient.alcohol_years,
        family_history:res.data.bpatient.family_history,
        comorbidities:res.data.bpatient.comorbidities,
        breastlump:res.data.bpatient.breastlump,
        breastlump_location:res.data.bpatient.breastlump_location,
        breastlump_size:res.data.bpatient.breastlump_size,
        overlying_skin:res.data.bpatient.overlying_skin,
        axillarylump:res.data.bpatient.axillarylump,
        axillarylump_side:res.data.bpatient.axillarylump_side,
        matted:res.data.bpatient.matted,
        axillarylump_size:res.data.bpatient.axillarylump_size,
        nipple_discharge:res.data.bpatient.nipple_discharge,
        nipple_discharge_frequency:res.data.bpatient.nipple_discharge_frequency,
        nipple_discharge_colour:res.data.bpatient.nipple_discharge_colour,
        mastalgia:res.data.bpatient.mastalgia,
        mastitis:res.data.bpatient.mastitis,
        ulcer:res.data.bpatient.ulcer,
        nipple_inversion:res.data.bpatient.nipple_inversion,
        others:res.data.bpatient.others,
        duration:res.data.bpatient.duration,
        pasthistory:res.data.bpatient.pasthistory,
        surgical_history:res.data.bpatient.surgical_history,
        drug_history:res.data.bpatient.drug_history,
        drug_allergy:res.data.bpatient.drug_allergy,
        drug_allergy_type:res.data.bpatient.drug_allergy_type,
        bowelhabit:res.data.bpatient.bowelhabit,
        bladderhabit:res.data.bpatient.bladderhabit,
        sleep:res.data.bpatient.sleep,
        appetite:res.data.bpatient.appetite,
        weight:res.data.bpatient.weight,
        height:res.data.bpatient.height,
        bmi:res.data.bpatient.bmi,
        bp:res.data.bpatient.bp,
        pulse:res.data.bpatient.pulse,
        temp:res.data.bpatient.temp,
        respiratory_rate:res.data.bpatient.respiratory_rate,
        health_condition:res.data.bpatient.health_condition,
        examination_remarks:res.data.bpatient.examination_remarks,
        bra_size:res.data.bpatient.bra_size,
        usg:res.data.bpatient.usg,
        mmg:res.data.bpatient.mmg,
        mri:res.data.bpatient.mri,
        fnac:res.data.bpatient.fnac,
        core_biopsy:res.data.bpatient.core_biopsy,
        incision_biopsy:res.data.bpatient.incision_biopsy,
        investigation_remarks:res.data.bpatient.investigation_remarks,
        blood_investigation:res.data.bpatient.blood_investigation,
        diagnosis:res.data.bpatient.diagnosis,
        treatment_plan:res.data.bpatient.treatment_plan,
        user_id:res.data.bpatient.user.id
      
        
      })
    })
    
    axios.get(`https://abcapi.vidaria.in/battachmentslist?X-AUTH=abc123&pid=${this.props.match.params.id}`)
    .then(res => {
      const products = res.data.Battachments;
      console.log(products)
      for (let i = 0; i < products.length; i++) {
        this.setState({ products });
     
      }
    });
  }   


  
  submit = e => {
    e.preventDefault()

    // alert('hello')
    axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`,{

      activity :"Updated breast cancer patient",
      user_id:this.state.user_id,

    })

    axios.put(`https://abcapi.vidaria.in/updatebpatient?X-AUTH=abc123&pid=${this.props.match.params.id}`,{

      id:this.state.id,
      pid:this.state.pid,
      date:this.state.date,
      code:this.state.code,
      place:this.state.place,
      pname:this.state.pname,
      fhname:this.state.fhname,
      age:this.state.age,
      sex:this.state.sex,
      education:this.state.education,
      address:this.state.address,
      phone:this.state.phone,
      district:this.state.district,
      state:this.state.state,
      area:this.state.area,
      referred_by:this.state.referred_by,
      occupation:this.state.occupation,
      ethnic:this.state.ethnic,
      marital_status:this.state.marital_status,
      marital_status_years :this.state.marital_status_years,
      menses_frequency:this.state.menses_frequency,
      menses_loss:this.state.menses_loss,
      menarche_years :this.state.menarche_years,
      hystrectomy :this.state.hystrectomy,
      hystrectomy_years:this.state.hystrectomy_years,
      menopause:this.state.menopause,
      menopause_years:this.state.menarche_years,
      children:this.state.children,
      children_male:this.state.children_male,
      children_female:this.state.children_female,
      abortions:this.state.abortions,
      abortions_number:this.state.abortions_number,
      abortions_cause:this.state.abortions_cause,
      breastfed:this.state.breastfed,
      breastfed_years:this.state.breastfed_years,
      current_lactation:this.state.current_lactation,
      contraception_methods:this.state.contraception_methods,
      contraception_methods_type:this.state.contraception_methods_type,
      hormone_treatment:this.state.hormone_treatment,
      addiction:this.state.addiction,
      tobacco:this.state.tobacco,
      tobacco_years:this.state.tobacco_years,
      smoking:this.state.smoking,
      smoking_years:this.state.smoking_years,
      alcohol:this.state.alcohol,
      alcohol_years:this.state.alcohol_years,
      family_history:this.state.family_history,
      comorbidities:this.state.comorbidities,
      breastlump:this.state.breastlump,
      breastlump_location:this.state.breastlump_location,
      breastlump_size:this.state.breastlump_size,
      overlying_skin:this.state.overlying_skin,
      axillarylump:this.state.axillarylump,
      axillarylump_side:this.state.axillarylump_side,
      matted:this.state.matted,
      axillarylump_size:this.state.axillarylump_size,
      nipple_discharge:this.state.nipple_discharge,
      nipple_discharge_frequency:this.state.nipple_discharge_frequency,
      nipple_discharge_colour:this.state.nipple_discharge_colour,
      mastalgia:this.state.mastalgia,
      mastitis:this.state.mastitis,
      ulcer:this.state.ulcer,
      nipple_inversion:this.state.nipple_inversion,
      others:this.state.others,
      duration:this.state.duration,
      pasthistory:this.state.pasthistory,
      surgical_history:this.state.surgical_history,
      drug_history:this.state.drug_history,
      drug_allergy:this.state.drug_allergy,
      drug_allergy_type:this.state.drug_allergy_type,
      bowelhabit:this.state.bowelhabit,
      bladderhabit:this.state.bladderhabit,
      sleep:this.state.sleep,
      appetite:this.state.appetite,
      weight:this.state.weight,
      height:this.state.height,
      bmi:this.state.bmi,
      bp:this.state.bp,
      pulse:this.state.pulse,
      temp:this.state.temp,
      respiratory_rate:this.state.respiratory_rate,
      health_condition:this.state.health_condition,
      examination_remarks:this.state.examination_remarks,
      bra_size:this.state.bra_size,
      usg:this.state.usg,
      mmg:this.state.mmg,
      mri:this.state.mri,
      fnac:this.state.fnac,
      core_biopsy:this.state.core_biopsy,
      incision_biopsy:this.state.incision_biopsy,
      investigation_remarks:this.state.investigation_remarks,
      blood_investigation:this.state.blood_investigation,
      diagnosis:this.state.diagnosis,
      treatment_plan:this.state.treatment_plan,
      user_id:this.state.user_id
    }).then((res)=>{
      console.log(res.data.success)
      if(res.data.success == "false"){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: res.data.message,
          
        })
      }else{
     this.opensweetalert()
      }
    })
  }
  


  render()




{

  return (
    <>
      <Container fluid>
        <Row>
          <Col md="8">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Edit Profile</Card.Title>
              </Card.Header>
              <Card.Body>
                <Form onSubmit={this.submit}>


                <Row>
          <Col md="6">
         

          {/* <Form.Group>
        <Form.Label>Patient Id</Form.Label><br></br>
          <Form.Control
           type="number"
           readOnly
           name="pid"                          
           placeholder="patient id"
           value={this.state.pid}
           onChange={(e)=>this.setState({pid:e.target.value})}></Form.Control>
          </Form.Group> */}



          <Form.Group>
          <Form.Label>Date</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="date"
          placeholder="Date"
          value={this.state.date}
          onChange={(e)=>this.setState({date:e.target.value})}  />
          </Form.Group>
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="code"
          placeholder="Code"
          value={this.state.code}
          onChange={(e)=>this.setState({code:e.target.value})}/>
          </Form.Group>
</Col>
</Row>
<Col md="12">

          <Form.Group>
          <Form.Label>place</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="place"
          placeholder="place"
          value={this.state.place}
          onChange={(e)=>this.setState({place:e.target.value})}/>
          </Form.Group>
          </Col>
        


          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>Patient Name</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="pname"
          placeholder="pname"
          value={this.state.pname}
          onChange={(e)=>this.setState({pname:e.target.value})}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Father or Husband name</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="fhname"
          placeholder="fhname"
          value={this.state.fhname}
          onChange={(e)=>this.setState({fhname:e.target.value})}/>
          </Form.Group>
</Col>
<Col md="6">

<Form.Group>
          <Form.Label>Age</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="age"  
          placeholder="Age"
          value={this.state.age}
          onChange={(e)=>this.setState({age:e.target.value})}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Gender</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="sex"
          placeholder="Gender"
          value={this.state.sex}
          onChange={(e)=>this.setState({sex:e.target.value})}/>
          </Form.Group>

</Col>
</Row>



<Row>
  <Col md="6">
  <Form.Group>
            <Form.Label>Education</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="education"
          placeholder="Education"
          value={this.state.education}
          onChange={(e)=>this.setState({education:e.target.value})}
          />
          </Form.Group>

          <Form.Group>
          <Form.Label>Address</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="address"
          placeholder="Address"
          value={this.state.address}
          onChange={(e)=>this.setState({address:e.target.value})}
          />
          </Form.Group>

</Col>
<Col md="6">
<Form.Group>
          <Form.Label>Phone</Form.Label><br></br>
          <Form.Control  
          type="number"
          name="phone"  
          placeholder="Phone"
          value={this.state.phone}
          onChange={(e)=>this.setState({phone:e.target.value})}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>District</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="district"
          placeholder="District"
          value={this.state.district}
          onChange={(e)=>this.setState({district:e.target.value})}
          />
          </Form.Group>

</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>
          <Form.Label>State</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="state"
          placeholder="State"
          value={this.state.state}
          onChange={(e)=>this.setState({state:e.target.value})}
          />
          </Form.Group>


          <Form.Group>
          <Form.Label>Area</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="area"
          placeholder="Area"
          value={this.state.area}
          onChange={(e)=>this.setState({area:e.target.value})}
          />
          </Form.Group>

          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>Referred_by</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="referred_by"
          placeholder="Referred by"
          value={this.state.referred_by}
          onChange={(e)=>this.setState({referred_by:e.target.value})}
          />
          </Form.Group>

          <Form.Group>
          <Form.Label>Occupation</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="occupation"
          placeholder="Occupation"
          value={this.state.occupation}
          onChange={(e)=>this.setState({occupation:e.target.value})}
          />
          </Form.Group>


</Col>
</Row>
          

            <Row>
  <Col md="6">
  <Form.Group>
          <Form.Label>Ethnic</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="ethnic"
            placeholder="ethnic"
          value={this.state.ethnic}
          onChange={(e)=>this.setState({ethnic:e.target.value})}
          
           />
          </Form.Group>

          <Form.Group>
          <Form.Label>Marital_status</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="marital_status"
            placeholder="Marital status"
          value={this.state.marital_status}
          onChange={(e)=>this.setState({marital_status:e.target.value})}
          
           />
          </Form.Group>

         </Col> 
      
         <Col md="6">
         <Form.Group>
          <Form.Label>marital_status_years</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="marital_status_years"
            placeholder="marital status years"
          value={this.state.marital_status_years}
          onChange={(e)=>this.setState({marital_status_years:e.target.value})}  
         />
          </Form.Group>

          <Form.Group>
          <Form.Label>menses_frequency</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="menses_frequency"
            placeholder="menses frequency"
          value={this.state.menses_frequency}
          onChange={(e)=>this.setState({menses_frequency:e.target.value})}
           />
          </Form.Group>

          </Col>
          </Row>



          <Row>

			  
  <Col md="6">   
  <Form.Group>
          <Form.Label>menses_loss</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="menses_loss"
            placeholder="menses loss"
          value={this.state.menses_loss}
          onChange={(e)=>this.setState({menses_loss:e.target.value})}
    />
          </Form.Group>
          <Form.Group>
          <Form.Label>menarche_years</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="menarche_years"
            placeholder="menarche year"
          value={this.state.menarche_years}
          onChange={(e)=>this.setState({menarche_years:e.target.value})}  
          />
          </Form.Group>
          </Col>
          <Col md="6"> 
          <Form.Group>  

          <Form.Label>hystrectomy</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="hystrectomy"
            placeholder="hystrectomy"
          value={this.state.hystrectomy}
          onChange={(e)=>this.setState({hystrectomy:e.target.value})}
        />
          </Form.Group>
          <Form.Group>
          <Form.Label>hystrectomy_years</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="hystrectomy_years"
            placeholder="hystrectomy year"
          value={this.state.hystrectomy_years}
          onChange={(e)=>this.setState({hystrectomy_years:e.target.value})}  
           />
          </Form.Group>

</Col>
</Row>




<Row>
          <Col md="6">
       
          <Form.Group>
          <Form.Label>menopause</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="menopause"
             placeholder="menopause"
          value={this.state.menopause}
          onChange={(e)=>this.setState({menopause:e.target.value})}  					
  
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>menopause_years</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="menopause_years"
              placeholder="menopause year"
          value={this.state.menopause_years}
          onChange={(e)=>this.setState({menopause_years:e.target.value})}   						
/>
           </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>

          <Form.Label>no of children</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="children"
              placeholder="no of children"
          value={this.state.children}
          onChange={(e)=>this.setState({children:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>children_male</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="children_male"
              placeholder="children_male"
          value={this.state.children_male}
          onChange={(e)=>this.setState({children_male:e.target.value})}   						
     />
           </Form.Group>

</Col>
</Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>children_female</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="children_female"
              placeholder="children_female "
          value={this.state.children_female}
          onChange={(e)=>this.setState({children_female:e.target.value})}   						
 />
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions</Form.Label><br></br>
           <Form.Control 
           type="text" 	
           name="abortions"
              placeholder="abortions"
          value={this.state.abortions}
          onChange={(e)=>this.setState({abortions:e.target.value})}  					
/>
           </Form.Group>
</Col>
<Col md="6">
<Form.Group>
          <Form.Label>abortions_number</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="abortions_number"
              placeholder="abortions_number"
          value={this.state.abortions_number}
          onChange={(e)=>this.setState({abortions_number:e.target.value})}   						
    />
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions_cause</Form.Label><br></br>
           <Form.Control 
           type="text" 	
           name="abortions_cause"
              placeholder="abortions_cause"
          value={this.state.abortions_cause}
          onChange={(e)=>this.setState({abortions_cause:e.target.value})}  					
/>
           </Form.Group>
         </Col>
         </Row>




         <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>breastfed</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="breastfed"	
               placeholder="breastfed"
          value={this.state.breastfed}
          onChange={(e)=>this.setState({breastfed:e.target.value})}  					
 />
          </Form.Group>

          <Form.Group>
           <Form.Label>breastfed_years</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="breastfed_years" 
               placeholder="breastfed_years"
          value={this.state.breastfed_years}
          onChange={(e)=>this.setState({breastfed_years:e.target.value})}  							
       />
          </Form.Group>
</Col>

<Col md="6">

<Form.Group>
           <Form.Label>current_lactation</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="current_lactation"	
               placeholder="current_lactation"
          value={this.state.current_lactation}
          onChange={(e)=>this.setState({current_lactation:e.target.value})}  					
  />
          </Form.Group>

          <Form.Group>
           <Form.Label>contraception_methods</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="contraception_methods"	
               placeholder="contraception_methods"
          value={this.state.contraception_methods}
          onChange={(e)=>this.setState({contraception_methods:e.target.value})}  					
/>
          </Form.Group>
         </Col>
         </Row>



       <Row>
          <Col md="6">
          <Form.Group>
         <Form.Label>contraception_methods_type</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="contraception_methods_type"	
               placeholder="contraception_methods_type"
          value={this.state.contraception_methods_type}
          onChange={(e)=>this.setState({contraception_methods_type:e.target.value})}  					
/>
          </Form.Group>
          <Form.Group>
           <Form.Label>hormone_treatment</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="hormone_treatment"	
               placeholder="hormone_treatment"
          value={this.state.hormone_treatment}
          onChange={(e)=>this.setState({hormone_treatment:e.target.value})}  					
/>
          </Form.Group>
          </Col>

          <Col md="6">
          <Form.Group>
           <Form.Label>addiction</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="addiction"	
               placeholder="addiction"
          value={this.state.addiction}
          onChange={(e)=>this.setState({addiction:e.target.value})}  					
/>
          </Form.Group>
          <Form.Group>
           <Form.Label>tobacco</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="tobacco"	
               placeholder="tobacco"
          value={this.state.tobacco}
          onChange={(e)=>this.setState({tobacco:e.target.value})}  					
/>
          </Form.Group>

          </Col>
          </Row>


          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>tobacco_years</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="tobacco_years" 
             placeholder="tobacco_years"
          value={this.state.tobacco_years}
          onChange={(e)=>this.setState({tobacco_years:e.target.value})}  						
/>
          </Form.Group>
          <Form.Group>
          <Form.Label>smoking</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="smoking"	
             placeholder="smoking"
          value={this.state.smoking}
          onChange={(e)=>this.setState({smoking:e.target.value})}  				
/>
          </Form.Group>
          </Col>

          <Col md="6">
          <Form.Group>
          <Form.Label>smoking_years</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="smoking_years" 
             placeholder="smoking_years"
          value={this.state.smoking_years}
          onChange={(e)=>this.setState({smoking_years:e.target.value})}  						
 />
          </Form.Group>
          <Form.Group>
          <Form.Label>alcohol</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="alcohol"	
             placeholder="alcohol"
          value={this.state.alcohol}
          onChange={(e)=>this.setState({alcohol:e.target.value})}  				
 />
          </Form.Group>
          </Col>

          </Row>

          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>alcohol_years</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="alcohol_years" 
             placeholder="alcohol_years"
          value={this.state.alcohol_years}
          onChange={(e)=>this.setState({alcohol_years:e.target.value})}  						
   />
          </Form.Group>
          <Form.Group>
          <Form.Label>family_history</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="family_history"	
             placeholder="family_history"
          value={this.state.family_history}
          onChange={(e)=>this.setState({family_history:e.target.value})}  				
/>
          </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>
          <Form.Label>comorbidities</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="comorbidities"	
             placeholder="comorbidities"
          value={this.state.comorbidities}
          onChange={(e)=>this.setState({comorbidities:e.target.value})}  				
/>
          </Form.Group>

          
          <Form.Group>
          <Form.Label>breastlump</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="breastlump"	
             placeholder="breastlump"
          value={this.state.breastlump}
          onChange={(e)=>this.setState({breastlump:e.target.value})}  				
/>




</Form.Group>
          </Col>
          </Row>


<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>breastlump_location</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="breastlump_location"        placeholder="breastlump_location"
          value={this.state.breastlump_location}
          onChange={(e)=>this.setState({breastlump_location:e.target.value})}   						
     />
           </Form.Group>
          <Form.Group>
          <Form.Label>breastlump_size</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="breastlump_size"        placeholder="breastlump_size"
          value={this.state.breastlump_size}
          onChange={(e)=>this.setState({breastlump_size:e.target.value})}   						
/>
           </Form.Group>
</Col>





<Col md="6">
<Form.Group>
          <Form.Label>overlying_skin</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="overlying_skin"        placeholder="overlying_skin"
          value={this.state.overlying_skin}
          onChange={(e)=>this.setState({overlying_skin:e.target.value})}   						
  />
           </Form.Group>

          <Form.Group>
          <Form.Label>axillarylump</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="axillarylump"        placeholder="axillarylump"
          value={this.state.axillarylump}
          onChange={(e)=>this.setState({axillarylump:e.target.value})}   						
/>
           </Form.Group>
</Col>
</Row>


<Row>
          <Col md="6">
          <Form.Group>

          <Form.Label>axillarylump_side</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="axillarylump_side"        placeholder="axillarylump_side"
          value={this.state.axillarylump_side}
          onChange={(e)=>this.setState({axillarylump_side:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>matted</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="matted"        placeholder="matted"
          value={this.state.matted}
          onChange={(e)=>this.setState({matted:e.target.value})}   						
/>
           </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>axillarylump_size</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="axillarylump_size"        placeholder="axillarylump_size"
          value={this.state.axillarylump_size}
          onChange={(e)=>this.setState({axillarylump_size:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>nipple_discharge</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="nipple_discharge"        placeholder="nipple_discharge"
          value={this.state.nipple_discharge}
          onChange={(e)=>this.setState({nipple_discharge:e.target.value})}   						
 />
           </Form.Group>

          </Col>
          </Row>
        
          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>nipple_discharge_frequency</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="nipple_discharge_frequency"        placeholder="nipple_discharge_frequency"
          value={this.state.nipple_discharge_frequency}
          onChange={(e)=>this.setState({nipple_discharge_frequency:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>nipple_discharge_colour</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="nipple_discharge_colour"        placeholder="nipple_discharge_colour"
          value={this.state.nipple_discharge_colour}
          onChange={(e)=>this.setState({nipple_discharge_colour:e.target.value})}   						
    />
           </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>mastalgia</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="mastalgia"        placeholder="mastalgia"
          value={this.state.mastalgia}
          onChange={(e)=>this.setState({mastalgia:e.target.value})}   						
/>
           </Form.Group>

          <Form.Group>
          <Form.Label>mastitis</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="mastitis"        placeholder="mastitis"
          value={this.state.mastitis}
          onChange={(e)=>this.setState({mastitis:e.target.value})}   						
/>
           </Form.Group>
 </Col>
</Row>


<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>ulcer</Form.Label>
           <Form.Control 
           type="text" 	
           name="ulcer"	
           placeholder="ulcer"
          value={this.state.ulcer}
          onChange={(e)=>this.setState({ulcer:e.target.value})}  				
/>
          </Form.Group>


          <Form.Group>
          <Form.Label>nipple_inversion</Form.Label>
           <Form.Control 
           type="text" 	
           name="nipple_inversion"	
           placeholder="nipple_inversion"
          value={this.state.nipple_inversion}
          onChange={(e)=>this.setState({nipple_inversion:e.target.value})}  				
/>
          </Form.Group>


</Col>
<Col md="6">
<Form.Group>
          <Form.Label>others</Form.Label>
           <Form.Control 
           type="text" 	
           name="others"	
           placeholder="others"
          value={this.state.others}
          onChange={(e)=>this.setState({others:e.target.value})}  				
/>
          </Form.Group>

          <Form.Group>
          <Form.Label>duration</Form.Label>
           <Form.Control 
           type="text" 	
           name="duration"	
           placeholder="duration"
          value={this.state.duration}
          onChange={(e)=>this.setState({duration:e.target.value})}  				
   />
          </Form.Group>
</Col>
</Row>


<Row>

          <Col md="6">
          <Form.Group>
          <Form.Label>pasthistory</Form.Label>
           <Form.Control 
           type="text" 	
           name="pasthistory"	
           placeholder="pasthistory"
          value={this.state.pasthistory}
          onChange={(e)=>this.setState({pasthistory:e.target.value})}  				
/>
          </Form.Group>

          <Form.Group>
          <Form.Label>surgical_history</Form.Label>
           <Form.Control 
           type="text" 	
           name="surgical_history"	
           placeholder="surgical_history"
          value={this.state.surgical_history}
          onChange={(e)=>this.setState({surgical_history:e.target.value})}  				
/>
          </Form.Group>
          </Col>


<Col md="6">
<Form.Group>
        <Form.Label>drug_history</Form.Label>
           <Form.Control 
           type="text" 	
           name="drug_history"	
           placeholder="drug_history"
          value={this.state.drug_history}
          onChange={(e)=>this.setState({drug_history:e.target.value})}  				
 />
          </Form.Group>


          <Form.Group>
          <Form.Label>drug_allergy</Form.Label>
           <Form.Control 
           type="text" 	
           name="drug_allergy"	
           placeholder="drug_allergy"
          value={this.state.drug_allergy}
          onChange={(e)=>this.setState({drug_allergy:e.target.value})}  				
/>
          </Form.Group>
</Col>

</Row>



<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>drug_allergy_type</Form.Label>
          <Form.Control 
          type="text" 	
          name="drug_allergy_type"	
          placeholder="drug_allergy_type"
          value={this.state.drug_allergy_type}
          onChange={(e)=>this.setState({drug_allergy_type:e.target.value})}  				
    />
          </Form.Group>

          <Form.Group>
          <Form.Label>bowelhabit</Form.Label>
          <Form.Control 
          type="text" 	
          name="bowelhabit"	
          placeholder="bowelhabit"
          value={this.state.bowelhabit}
          onChange={(e)=>this.setState({bowelhabit:e.target.value})}  				
/>
          </Form.Group>

</Col>

<Col md="6">
<Form.Group>
          <Form.Label>bladderhabit</Form.Label>
          <Form.Control 
          type="text" 	
          name="bladderhabit"	
          placeholder="bladderhabit"
          value={this.state.bladderhabit}
          onChange={(e)=>this.setState({bladderhabit:e.target.value})}  				
     />
          </Form.Group>
          <Form.Group>
          <Form.Label>sleep</Form.Label>
          <Form.Control 
          type="text" 	
          name="sleep"	
          placeholder="sleep"
          value={this.state.sleep}
          onChange={(e)=>this.setState({sleep:e.target.value})}  				
/>
          </Form.Group>

   </Col>
   </Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>appetite</Form.Label>
          <Form.Control 
          type="text" 	
          name="appetite"	
          placeholder="appetite"
          value={this.state.appetite}
          onChange={(e)=>this.setState({appetite:e.target.value})}  				
/>
          </Form.Group>
          <Form.Group>
          <Form.Label>weight</Form.Label>
          <Form.Control 
          type="number"
          name="weight" 
          placeholder="weight"
          value={this.state.weight}
          onChange={(e)=>this.setState({weight:e.target.value})}  						
  />
          </Form.Group>
</Col>
<Col md="6">
<Form.Group>
          <Form.Label>height</Form.Label>
          <Form.Control 
          type="number"
          name="height" 
          placeholder="height"
          value={this.state.height}
          onChange={(e)=>this.setState({height:e.target.value})}  						
/>
          </Form.Group>
          <Form.Group>
          <Form.Label>bmi</Form.Label>
          <Form.Control 
          type="text" 	
          name="bmi"	
          placeholder="bmi"
          value={this.state.bmi}
          onChange={(e)=>this.setState({bmi:e.target.value})}  				
/>
          </Form.Group>
</Col>
</Row>


<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>bp</Form.Label>
         <Form.Control 
         type="number"
         name="bp" 
         placeholder="bp"
          value={this.state.bp}
          onChange={(e)=>this.setState({bp:e.target.value})}  						
/>
         </Form.Group>


          <Form.Group>
          <Form.Label>pulse</Form.Label>
         <Form.Control 
         type="number"
         name="pulse" 
         placeholder="pulse"
          value={this.state.pulse}
          onChange={(e)=>this.setState({pulse:e.target.value})}  						
/>
         </Form.Group>
       </Col>
       <Col md="6">
       <Form.Group>
          <Form.Label>temp</Form.Label>
         <Form.Control 
         type="text" 	
         name="temp"	
         placeholder="temp"
          value={this.state.temp}
          onChange={(e)=>this.setState({temp:e.target.value})}  				
  />
         </Form.Group>
          <Form.Group>
          <Form.Label>respiratory_rate</Form.Label>
         <Form.Control 
         type="number"
         name="respiratory_rate" 
         placeholder="respiratory_rate"
          value={this.state.respiratory_rate}
          onChange={(e)=>this.setState({respiratory_rate:e.target.value})}  						
/>
         </Form.Group>
          
          </Col>

          </Row>

          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>health_condition</Form.Label>
         <Form.Control 
         type="text" 	
         name="health_condition"	
         placeholder="health_condition"
          value={this.state.health_condition}
          onChange={(e)=>this.setState({health_condition:e.target.value})}  				
/>
         </Form.Group>
          <Form.Group>
          <Form.Label>examination_remarks</Form.Label>
         <Form.Control 
         type="text" 	
         name="examination_remarks"	
         placeholder="examination_remarks"
          value={this.state.examination_remarks}
          onChange={(e)=>this.setState({examination_remarks:e.target.value})}  				
/>
         </Form.Group>
          </Col>
         
          <Col md="6">
          <Form.Group>
          <Form.Label>bra_size</Form.Label>
         <Form.Control 
         type="text" 	
         name="bra_size"	
         placeholder="bra_size"
          value={this.state.bra_size}
          onChange={(e)=>this.setState({bra_size:e.target.value})}  				
 />
         </Form.Group>
          <Form.Group>
          <Form.Label>usg</Form.Label>
         <Form.Control 
         type="text" 	
         name="usg"	
         placeholder="usg"
          value={this.state.usg}
          onChange={(e)=>this.setState({usg:e.target.value})}  				
/>
         </Form.Group>
</Col>
</Row>




<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>mmg</Form.Label>
         <Form.Control 
         type="text" 	
         name="mmg"	
         placeholder="mmg"
          value={this.state.mmg}
          onChange={(e)=>this.setState({mmg:e.target.value})}  				
 />
          </Form.Group>
          <Form.Group>
          <Form.Label>mri</Form.Label>
         <Form.Control 
         type="text" 	
         name="mri"	
         placeholder="mri"
          value={this.state.mri}
          onChange={(e)=>this.setState({mri:e.target.value})}  				
 />
          </Form.Group>
          </Col>
          <Col md="6">

          <Form.Group>
          <Form.Label>fnac</Form.Label>
         <Form.Control 
         type="text" 	
         name="fnac"	
         placeholder="fnac"
          value={this.state.fnac}
          onChange={(e)=>this.setState({fnac:e.target.value})}  				
  />
          </Form.Group>
          <Form.Group>
          <Form.Label>core_biopsy</Form.Label>
         <Form.Control 
         type="text" 	
         name="core_biopsy"	
         placeholder="core_biopsy"
          value={this.state.core_biopsy}
          onChange={(e)=>this.setState({core_biopsy:e.target.value})}  				
/>
        
          </Form.Group>
        </Col>
        </Row>

        <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>incision_biopsy</Form.Label>
         <Form.Control 
         type="text" 	
         name="incision_biopsy"	
         placeholder="incision_biopsy"
          value={this.state.incision_biopsy}
          onChange={(e)=>this.setState({incision_biopsy:e.target.value})}  				
 />
          </Form.Group>
          <Form.Group>
          <Form.Label>investigation_remarks</Form.Label>
         <Form.Control 
         type="text" 	
         name="investigation_remarks"	
         placeholder="investigation_remarks"
          value={this.state.investigation_remarks}
          onChange={(e)=>this.setState({investigation_remarks:e.target.value})}  				
 />
          </Form.Group>
          </Col>

          <Col md="6">
          <Form.Group>
          <Form.Label>blood_investigation</Form.Label>
         <Form.Control 
         type="text" 	
         name="blood_investigation"	
         placeholder="blood_investigation"
          value={this.state.blood_investigation}
          onChange={(e)=>this.setState({blood_investigation:e.target.value})}  				
/>
          </Form.Group>
          <Form.Group>
          <Form.Label>diagnosis</Form.Label>
         <Form.Control 
         type="text" 	
         name="diagnosis"	
         placeholder="diagnosis"
          value={this.state.diagnosis}
          onChange={(e)=>this.setState({diagnosis:e.target.value})}  				
/>
          </Form.Group>
          </Col>
          </Row>




        <Row>
        
          <Col md="6">
          <Form.Group>
          <Form.Label>treatment_plan</Form.Label>
         <Form.Control 
         type="text" 	
         name="treatment_plan"	
         placeholder="treatment_plan"
          value={this.state.treatment_plan}
          onChange={(e)=>this.setState({treatment_plan:e.target.value})}  				
  />
          </Form.Group>

          </Col>
          <Col md="6">
          <Form.Group>
          <Form.Label>User Id</Form.Label>
         <Form.Control 
         type="number"
         name="user_id" 
         placeholder="User Id"
          value={this.state.user_id}
          onChange={(e)=>this.setState({user_id:e.target.value})}  	
          disabled					
/>
          </Form.Group>

</Col>
</Row>



<Button
                    className="btn-fill pull-right"
                    type="submit"
                    name=""
                    variant="info"
                    // onClick={this.opensweetalert()}

                  >
                    Update User
                  </Button>

                       </Form>


                       </Card.Body>
                       </Card>

                       </Col>

                    </Row>
                  
                    <div>
                        <h4>Attachments</h4>



        {
          this.state.products.map((i, index) => {
            console.log(i.attachment)

          var image= i.attachment.toString()

            var ext = image.split(".").pop()
            console.log(ext)
            if (ext == "jpeg" || ext == "png" || ext == "jpg"){
              console.log(i.attachment)

              return(
                <div>
                   <Row>

    <Col xs={4} md={4}>
      <Image src={`https://abcapi.vidaria.in/files/${i.attachment}?X-AUTH=abc123`}   fluid /><br></br><br></br>

      </Col>
      </Row>
                  </div>
                  
              )

            }else{
            console.log("im also coming")
              return(
                <video width="750" height="500" controls >
                <source src={`https://abcapi.vidaria.in/files/${i.attachment}?X-AUTH=abc123`} type="video/mp4"/>
          </video>
              )
              
   

            }
  
    })
  }
  </div>



      </Container>
    </>
  );
}
}

export default User;
