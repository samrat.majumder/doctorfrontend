import React from "react";
import axios from 'axios'
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Image,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import Swal from 'sweetalert2'
import { Link } from "react-router-dom";
import PDF from './PDF';


class User extends React.Component {
 state ={
      
      users:[],
   
      pid: '',
      date: '',
      code: '',
      pname:'',
      age:'',
      sex:'',
      address:'',
      phone:'',
      diagnosis:'',
      treatment_plan:'',
      input1:'',
      input2:'',
      input3:'',
      input4:'',
      input5:'',
      input6:'',
      input7:'',
      input8:'',
      input9:'',
      input10:'',
      Precautions:'',
      postSubmitted: false


      
    }

  
onChange = input => e => {

        this.setState({
            [input]: e.target.value
        });
    }



  opensweetalert()
  {
    Swal.fire({
      title: 'Patient Updated',
      text: "success",
      type: 'success',
      
    }).then(function() {
      window.location = "/admin/dashboard";
  });
  }




  componentDidMount(){
    axios.get(`https://abcapi.vidaria.in/allfollowuprheumadetails?X-AUTH=abc123&pid=${this.props.match.params.id}`)
    .then((res)=>{
      console.log(res)
      this.setState({
      
        pid:res.data.RheumatologyFollowup[0].pid,
        date:res.data.RheumatologyFollowup[0].date,
        code:res.data.RheumatologyFollowup[0].code,
        place:res.data.RheumatologyFollowup[0].place,
        pname:res.data.RheumatologyFollowup[0].pname,
        age:res.data.RheumatologyFollowup[0].age,
        sex:res.data.RheumatologyFollowup[0].sex,
        address:res.data.RheumatologyFollowup[0].address,
        phone:res.data.RheumatologyFollowup[0].phone,
        // diagnosis:res.data.RheumatologyFollowup[0].diagnosis,
        // treatment_plan:res.data.RheumatologyFollowup[0].treatment_plan,
        
        user_id:res.data.RheumatologyFollowup[0].user.id
      
        
      })
    })
    
   
  }   

sunmitPost = (e) => {
        
    e.preventDefault();

        this.setState({
            postSubmitted: true
        });
        console.log("yes coming")

    }


  


  render()




{

  return (
    <>
     {  !this.state.postSubmitted ? 

       (<Container fluid>
    
            <Card>
              <Card.Header>
                <Card.Title as="h4">Abc centre details</Card.Title>
              </Card.Header>
              <Card.Body>
                <Form method="post">


                <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>Date</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="date"
          placeholder="Date"
          value={this.state.date}
          onChange={this.onChange('date')} />
          </Form.Group>
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="code"
          placeholder="Code"
          value={this.state.code}
          onChange={this.onChange('code')}          />
          </Form.Group>
</Col>
</Row>


<Row>
<Col md="6">

          {/* <Form.Group>
          <Form.Label>place</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="place"
          placeholder="place"
          value={this.state.place}
          onChange={this.onChange('place')}/>
          </Form.Group> */}
          </Col>
              
          <Col md="12">
          <Form.Group>
          <Form.Label>Patient Name</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="pname"
          placeholder="pname"
          value={this.state.pname}
          onChange={this.onChange('pname')}/>
          </Form.Group>

      
</Col>
</Row>


<Row>
<Col md="6">

<Form.Group>
          <Form.Label>Age</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="age"  
          placeholder="Age"
          value={this.state.age}
          onChange={this.onChange('age')}/>
          </Form.Group>

</Col>
<Col md="6">
          <Form.Group>
          <Form.Label>Gender</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="sex"
          placeholder="Gender"
          value={this.state.sex}
          onChange={this.onChange('sex')}/>
          </Form.Group>

</Col>
</Row>



<Row>
  <Col md="6">


          <Form.Group>
          <Form.Label>Address</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="address"
          placeholder="Address"
          value={this.state.address}
          onChange={this.onChange('address')}
          />
          </Form.Group>

</Col>
<Col md="6">
<Form.Group>
          <Form.Label>Phone</Form.Label><br></br>
          <Form.Control  
          type="number"
          name="phone"  
          placeholder="Phone"
          value={this.state.phone}
          onChange={this.onChange('phone')}/>
          </Form.Group>

       

</Col>
</Row> 


        <Row>
 

          <Col md="6">
          <Form.Group>
          <Form.Label>diagnosis</Form.Label>
         <Form.Control 
         type="text" 	
         name="diagnosis"	
         placeholder="diagnosis"
          onChange={this.onChange('diagnosis')}			
/>
          </Form.Group>
          </Col>
          




        
        
          <Col md="6">
          <Form.Group>
          <Form.Label>treatment_plan</Form.Label>
         <Form.Control 
         type="text" 	
         name="treatment_plan"	
         placeholder="treatment_plan"
          onChange={this.onChange('treatment_plan')}  />
          </Form.Group>

</Col>
</Row>


<Row>
    <Col md="12">
       
    <Form.Group>
    1)
         <Form.Control 
         type="text" 	
         name="input1"
         onChange={this.onChange('input1')} 				
  />
          </Form.Group>
    </Col>


    <Col md="12">
       
       <Form.Group>
       2)
            <Form.Control 
            type="text" 	
         
            name="input2"
            onChange={this.onChange('input2')}				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       3)
            <Form.Control 
            type="text" 	
            name="input3"
            onChange={this.onChange('input3')} 				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       4)
            <Form.Control 
            type="text" 	
            name="input4"
            onChange={this.onChange('input4')}  				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       5)
            <Form.Control 
            type="text" 	
            name="input5"
            onChange={this.onChange('input5')}				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       6)
            <Form.Control 
            type="text" 	
            name="input6"
            onChange={this.onChange('input6')}				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       7)
            <Form.Control 
            type="text" 	
            name="input7"
            onChange={this.onChange('input7')} 				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       8)
            <Form.Control 
            type="text" 	
            name="input8"
            onChange={this.onChange('input8')}  				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       9)
            <Form.Control 
            type="text"
            name="input9" 	
            onChange={this.onChange('input9')}				
     />
             </Form.Group>
       </Col>


       <Col md="12">
       
       <Form.Group>
       10)
            <Form.Control 
            type="text" 	
            name="input10"
            onChange={this.onChange('input10')}				
     />
             </Form.Group>
       </Col>


</Row>

<Row>
    <Col md="12">
    <Form.Group>
          <Form.Label>Precautions</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="Precautions"
          onChange={this.onChange('Precautions')}/>
          </Form.Group>
    </Col>
</Row>


<Button className="btn-fill pull-right" type="button" onClick={this.sunmitPost} name="" variant="info">
                    Print Details
                  </Button>

                       </Form>


                       </Card.Body>
                       </Card>

                    
                  




      </Container>)

 : (
                        <PDF 
                        pid={this.state.pid} 
                        date={this.state.date}
                        code={this.state.code}
                        pname={this.state.pname}
                        age={this.state.age}
                        sex={this.state.sex}
                        address={this.state.address}
                        phone={this.state.phone}
                        diagnosis={this.state.diagnosis}
                        treatment_plan={this.state.treatment_plan}
                        input1={this.state.input1}
                        input2={this.state.input2}
                        input3={this.state.input3}

                        input4={this.state.input4}
                        input5={this.state.input5}
                        input6={this.state.input6}
                        input7={this.state.input7}
                        input8={this.state.input8}
                        input9={this.state.input9}
                        input10={this.state.input10}
                        Precautions={this.state.Precautions}

                        
                        />
                    )
 }
    </>
  );
}
}

export default User;
