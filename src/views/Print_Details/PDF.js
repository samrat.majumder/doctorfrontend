import React from 'react';
import Pdf from "react-to-pdf";
import Swal from 'sweetalert2'

const ref = React.createRef();


const opensweetalert = () =>
{
  Swal.fire({
    title: 'Patient record Downloaded',
    text: "success",
    type: 'success',
    
  }).then(function() {
    window.location = "/admin/dashboard";
});
}

const PDF = (props) => {

    console.log(props)


  return (
    <>
      <div className="Post" ref={ref}>
        <h3 style={{"color":"red"}}>A.B.C Patient prescription</h3>
        <p>Patient ID        ----   {props.pid}</p>
        <p>Patient Name      ----           {props.pname}</p>
        <p>Date              ----          {props.date}</p>
        <p>Age               ----          {props.age}</p>
        <p>Gender            ----           {props.sex}</p>
        <p>Address           ----           {props.address}</p>
        <p>Diagnosis         ----           {props.diagnosis}</p>
        <p>Treatment Plan    ----             {props.treatment_plan}</p>
        <h4 style={{"color":"blue"}}>Medicine List </h4>
        <p>1)                ----            {props.input1}</p>
        <p>2)                ----              {props.input2}</p>
        <p>3)                ----              {props.input3}</p>
        <p>4)                ----             {props.input4}</p>
        <p>5)                ----            {props.input5}</p>
        <p>6)                ----           {props.input6}</p>
        <p>7)                ----           {props.input7}</p>
        <p>8)                ----         {props.input8}</p>
        <p>9)                ----         {props.input9}</p>
        <p>10)               ----        {props.input10}</p>
        <p>Precautions       ----   {props.Precautions}</p>





        <img src={props.image} alt={props.title} />
        <p>{props.content}</p>
      </div>
      <Pdf targetRef={ref} filename="prescription.pdf">
        {({ toPdf }) => <button onClick={ () => {toPdf();opensweetalert()}}>Print</button>}
      </Pdf>
    </>
  );
}

export default PDF;