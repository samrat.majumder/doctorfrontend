
 
// import React, { Component } from 'react';
// import { Form, Button } from 'semantic-ui-react';

// class Step2 extends Component {
// 	saveAndContinue = (e) => {
// 		e.preventDefault();
// 		this.props.nextStep();
// 	}

// 	back = (e) => {
// 		e.preventDefault();
// 		this.props.prevStep();
// 	}

// 	render() {
// 		const { values } = this.props
// 		return (
// 			<Form color='blue' >
// 				<h1 className="ui centered">Enter Personal Details</h1>
// 				<Form.Field>
// 					<label>password</label><br></br>
// 					<input placeholder='password'
// 						onChange={this.props.handleChange('password')}
// 						defaultValue={values.password}
// 					/>
// 				</Form.Field>
// 				<Form.Field>
// 					<label>role</label><br></br>
// 					<input placeholder='role'
// 						onChange={this.props.handleChange('role')}
// 						defaultValue={values.role}
// 					/>
// 				</Form.Field>
// 				<Form.Field>
// 					<label>speciality</label><br></br>
// 					<input placeholder='speciality'
// 						onChange={this.props.handleChange('speciality')}
// 						defaultValue={values.speciality}
// 					/>
// 				</Form.Field>
// 				<Button onClick={this.back}>Back</Button>
// 				<Button onClick={this.saveAndContinue}>Save And Continue </Button>
// 			</Form>
// 		)
// 	}
// }

// export default Step2;





import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class SocialMedia extends Component {
  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render() 
  {
		const { values } = this.props
    return (
      <Container fluid>
      <h2>Patient Information</h2>

        <Row>

        <Form>
  
        <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>ulcer</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('ulcer')}
 defaultValue={values.ulcer}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>nipple_inversion</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('nipple_inversion')}
 defaultValue={values.nipple_inversion}/>
          </Form.Group>


</Col>
<Col md="6">
<Form.Group>
          <Form.Label>others</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('others')}
defaultValue={values.others}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>duration</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('duration')}
 defaultValue={values.duration}/>
          </Form.Group>
</Col>
</Row>


<Row>

          <Col md="6">
          <Form.Group>
          <Form.Label>pasthistory</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('pasthistory')}
 defaultValue={values.pasthistory}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>surgical_history</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('surgical_history')}
defaultValue={values.surgical_history}/>
          </Form.Group>
          </Col>


<Col md="6">
<Form.Group>
        <Form.Label>drug_history</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('drug_history')}
 defaultValue={values.drug_history}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>drug_allergy</Form.Label>
           <Form.Control type="text" 						onChange={this.props.handleChange('drug_allergy')}
 defaultValue={values.drug_allergy}/>
          </Form.Group>
</Col>

</Row>
{/* 

<Row>
          <Col md="6">
          <label>drug_allergy_type</label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('drug_allergy_type')}
defaultValue={values.drug_allergy_type}/>
          <br/>

          <label>bowelhabit</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('bowelhabit')}
 defaultValue={values.bowelhabit}/>
          <br/>

</Col>

<Col md="6">
          <label>bladderhabit</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('bladderhabit')}
 defaultValue={values.bladderhabit}/>
          <br/>

          <label>sleep</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('sleep')}
 defaultValue={values.sleep}/>
          <br/>

   </Col>
   </Row>

<Row>
          <Col md="6">
          <label>appetite</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('appetite')}
defaultValue={values.appetite}/>
          <br/>
          <label>weight</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('weight')}
 defaultValue={values.weight}/>
          <br/>
</Col>
<Col md="6">
          <label>height</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('height')}
defaultValue={values.height}/>
          <br/>

          <label>bmi</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('bmi')}
 defaultValue={values.bmi}/>
          <br/>
</Col>
</Row>

<Row>
          <Col md="6">
          <label>bp</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('bp')}
defaultValue={values.bp}/>
          <br/>

          <label>pulse</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('pulse')}
defaultValue={values.pulse}/>
          <br/>
       </Col>
       <Col md="6">
          <label>temp</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('temp')}
defaultValue={values.temp}/>
          <br/>
          <label>respiratory_rate</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('respiratory_rate')}
 defaultValue={values.respiratory_rate}/>
          <br/>
          
          </Col>

          </Row>

          <Row>
          <Col md="6">

          <label>health_condition</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('health_condition')}
defaultValue={values.health_condition}/>
          <br/>

          <label>examination_remarks</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('examination_remarks')}
defaultValue={values.examination_remarks}/>
          <br/>
          </Col>

          <Col md="6">
          <label>bra_size</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('bra_size')}
 defaultValue={values.bra_size}/>
          <br/>

          <label>usg</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('usg')}
defaultValue={values.usg}/>
          <br/>
</Col>
</Row>


<Row>
          <Col md="6">

          <label>mmg</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mmg')}
defaultValue={values.mmg}/>
          <br/>

          <label>mri</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mri')}
 defaultValue={values.mri}/>
          <br/>
          </Col>
          <Col md="6">


          <label>fnac</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('fnac')}
 defaultValue={values.fnac}/>
          <br/>

          <label>core_biopsy</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('core_biopsy')}
defaultValue={values.core_biopsy}/>
        
          <br/>
        </Col>
        </Row>

        <Row>
          <Col md="6">

          <label>incision_biopsy</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('incision_biopsy')}
defaultValue={values.incision_biopsy}/>
          <br/>
          <label>investigation_remarks</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('investigation_remarks')}
           defaultValue={values.investigation_remarks}/>
          <br/>
          </Col>

          <Col md="6">
          <label>blood_investigation</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('blood_investigation')}
defaultValue={values.blood_investigation}/>
          <br/>

          <label>diagnosis</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('diagnosis')}
defaultValue={values.diagnosis}/>
          <br/>
          </Col>
          </Row>




        <Row>
          <Col md="6">
          <label>treatment_plan</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('treatment_plan')}
defaultValue={values.treatment_plan}/>
          <br/><br></br>
          <Button onClick={this.back}>Back</Button>

          </Col>
          <Col md="6">
          <label>User Id</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('user_id')}
defaultValue={values.user_id}/>
          <br/><br></br>
          <Button onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row> */}

<Row>
	   <Col md="6">
	   <Button className="btn-fill"  onClick={this.back}>Back</Button>
	 

			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning" onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>
        </Form>
        
        
      

        </Row>
      </Container>
    )

  
  }

}

export default SocialMedia;


  {/* // nextStep(e) {
  //   e.preventDefault()
  //   var data = {
  //     ulcer: this.refs.ulcer.value,
  //     nipple_inversion: this.refs.nipple_inversion.value,
  //     others: this.refs.others.value,
  //     duration: this.refs.duration.value,
  //     pasthistory: this.refs.pasthistory.value,
  //     surgical_history: this.refs.surgical_history.value,
  //     ulcer: this.refs.ulcer.value,
  //     nipple_inversion: this.refs.nipple_inversion.value,
  //     others: this.refs.others.value,
  //     duration: this.refs.duration.value,
  //     pasthistory: this.refs.pasthistory.value,
  //     surgical_history: this.refs.surgical_history.value,
  //     drug_history: this.refs.drug_history.value,
  //     drug_allergy: this.refs.drug_allergy.value,
  //     drug_allergy_type: this.refs.drug_allergy_type.value,
  //     bowelhabit: this.refs.bowelhabit.value,
  //     bladderhabit: this.refs.bladderhabit.value,
  //     sleep: this.refs.sleep.value,
  //     appetite: this.refs.appetite.value,
  //     weight: this.refs.weight.value,
  //     height: this.refs.height.value,
  //     bmi: this.refs.bmi.value,
  //     bp: this.refs.bp.value,
  //     pulse: this.refs.pulse.value,
  //     temp: this.refs.temp.value,
  //     respiratory_rate: this.refs.respiratory_rate.value,
  //     health_condition: this.refs.health_condition.value,
  //     examination_remarks: this.refs.examination_remarks.value,
  //     bra_size: this.refs.bra_size.value,
  //     usgm: this.refs.usgm.value,
  //     mmg: this.refs.mmg.value,
  //     mri: this.refs.mri.value,
  //     fnac: this.refs.fnac.value,
  //     core_biopsy: this.refs.core_biopsy.value,
  //     incision_biopsy: this.refs.incision_biopsy.value,
  //     investigation_remarks: this.refs.investigation_remarks.value,
  //     blood_investigation: this.refs.blood_investigation.value,
  //     diagnosis: this.refs.diagnosis.value,
  //     treatment_plan: this.refs.treatment_plan.value,
  //   }
  //   this.props.saveValues(data);
  //   this.props.nextStep(); */}
