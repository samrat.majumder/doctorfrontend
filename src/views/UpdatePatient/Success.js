import React, { Component } from 'react';
import axios from 'axios';

class Success extends Component{
    constructor(props){
        super(props);
        this.state ={
          users:[],
          id:0,
          uname: "",
          email: "",  
          phone: "",
          password: "",
          role:"",
          speciality:""
        }
    
      }
      componentDidMount(){
        axios.get("https://abcapi.vidaria.in/alluserdetails")
        .then((res)=>{
          this.setState({
            users:res.data.user,
            id:0,
            uname:'',
            email:'',
            phone:'',
            password:'',
            role:'',
            speciality:'',
            
          })
        })
      }
      submit(event,id){
        event.preventDefault();
        if(id === 0){
          axios.post("https://abcapi.vidaria.in/adduser",{
            username:this.state.username,
            email:this.state.email,
            phone:this.state.phone,
            password:this.state.password,
            role:this.state.role,
            speciality:this.state.speciality,
          })
          .then((res)=>{
            this.componentDidMount();
          })
        }else{
          axios.put(`https://abcapi.vidaria.in/userupdate/${id}`,{
            id:this.state.id,
            username:this.state.username,
            email:this.state.email,
            phone:this.state.phone,
            password:this.state.password,
            role:this.state.role,
            speciality:this.state.speciality,
          }).then(()=>{
            this.componentDidMount();
          })
    
        }
    
      }


	render(){
		return(
            <button onClick={this.props.saveAll}>Submit</button>

		)
	}
}

export default Success;