import React from "react";
import { Link } from "react-router-dom";

import axios from 'axios'
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";


class User extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:[],
      id:0,
      username: "",
      email: "",  
      phone: "",
      password: "",
      role:"",
      speciality:""
    }

  }


  componentDidMount(){
    axios.get(`https://abcapi.vidaria.in/userdetails/${this.props.match.params.id}`)
    .then((res)=>{
      console.log(res)
      this.setState({
        users:res.data.user,
          id:res.data.user.id,
          username:res.data.user.username,
          email:res.data.user.email,
          phone:res.data.user.phone,
          password:res.data.user.password,
          role:res.data.user.role,
          speciality:res.data.user.speciality,
      
        
      })
    })
  }
  submit(event,id){
    event.preventDefault();
    if(id === 0){
      axios.post("https://abcapi.vidaria.in/adduser",{
        username:this.state.username,
        email:this.state.email,
        phone:this.state.phone,
        password:this.state.password,
        role:this.state.role,
        speciality:this.state.speciality,
      })
      .then((res)=>{
        this.componentDidMount();
      })
    }else{
      axios.put(`https://abcapi.vidaria.in/userupdate/${this.props.match.params.id}`,{
        id:this.state.id,
        username:this.state.username,
        email:this.state.email,
        phone:this.state.phone,
        password:this.state.password,
        role:this.state.role,
        speciality:this.state.speciality,
      }).then(()=>{
        this.props.history.push(`/admin/dashboard`)
      })

    }

  }
  delete(id){
    axios.delete(`https://abcapi.vidaria.in/userdelete/${id}`)
    .then(()=>{
      this.componentDidMount();
    })
  }
  // edit(id){
  //   axios.get(`https://abcapi.vidaria.in/userdetails/1`)
  //   .then((res)=>{
  //     console.log(res.data.user);
  //     this.setState({
  //       id:res.data.user.id,
  //       username:res.data.user.username,
  //       email:res.data.user.email,
  //       phone:res.data.user.phone,
  //       password:res.data.user.password,
  //       role:res.data.user.role,
  //       speciality:res.data.user.speciality,
  //     })
  //   })
  // }

  render()


{
  return (
    <>
      <Container fluid>
        <Row>
          <Col md="8">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Edit Profile</Card.Title>
              </Card.Header>
              <Card.Body>
                <Form onSubmit={(e)=>this.submit(e,this.state.id)}>
                  <Row>
                    <Col className="px-1" md="3">
                      <Form.Group>
                        <label>Username</label>
                        <Form.Control
                          name="uname"
                          placeholder="Username"
                          type="text"
                          value={this.state.username}
                          onChange={(e)=>this.setState({username:e.target.value})}

                        ></Form.Control>
                      </Form.Group>
                    </Col>


                    <Col className="pl-1" md="4">
                      <Form.Group>
                        <label>
                          Email address
                        </label>
                        <Form.Control
                          placeholder="Email"
                          type="email"
                          value={this.state.email}
                          name="email"
                          onChange={(e)=>this.setState({email:e.target.value})}
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>


                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>Phone</label>
                        <Form.Control
                        name="phone"
                        onChange={(e)=>this.setState({phone:e.target.value})}
                        placeholder="phone"
                        value={this.state.phone}
                        type="number"
                        ></Form.Control>
                      </Form.Group>
                    </Col>

                    <Col className="pl-1" md="6">
                      <Form.Group>
                        <label>Password</label>
                        <Form.Control
                          name="password"
                          placeholder="password"
                          onChange={(e)=>this.setState({password:e.target.value})}
                          value={this.state.password}
                          type="password"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>

                  <Row>
                    <Col md="12">
                      <Form.Group>
                        <label>Role</label>
                        <Form.Control
                         name="role"

                          placeholder="role"
                          type="text"
                          value={this.state.role}
                          onChange={(e)=>this.setState({role:e.target.value})}
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="pr-1" md="4">
                      <Form.Group>
                        <label>Speciality</label>
                        <Form.Control
                        name="speciality"
                        value={this.state.speciality}
                        onChange={(e)=>this.setState({speciality:e.target.value})}
                          placeholder="Speciality"
                          type="text"
                        ></Form.Control>
                      </Form.Group>
                    </Col>

                    {/* <Col className="px-1" md="4">
                      <Form.Group>
                        <label>Country</label>
                        <Form.Control
                          defaultValue="Andrew"
                          placeholder="Country"
                          type="text"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                    <Col className="pl-1" md="4">
                      <Form.Group>
                        <label>Postal Code</label>
                        <Form.Control
                          placeholder="ZIP Code"
                          type="number"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="12">
                      <Form.Group>
                        <label>About Me</label>
                        <Form.Control
                          cols="80"
                          defaultValue="Lamborghini Mercy, Your chick she so thirsty, I'm in
                          that two seat Lambo."
                          placeholder="Here can be your description"
                          rows="4"
                          as="textarea"
                        ></Form.Control>
                      </Form.Group>
                    </Col>*/}
                  </Row> 
                  <Button
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                  >
                    Update Profile
                  </Button>
                  <div className="clearfix"></div>
                </Form>
              </Card.Body>
            </Card>
          </Col>
          <Col md="4">
            <Card className="card-user">
              <div className="card-image">
                <img
                  alt="..."
                  src={
                    require("assets/img/photo-1431578500526-4d9613015464.jpeg")
                      .default
                  }
                ></img>
              </div>
              <Card.Body>
                <div className="author">
                  <a href="#pablo" onClick={(e) => e.preventDefault()}>
                    <img
                      alt="..."
                      className="avatar border-gray"
                      src={require("assets/img/faces/face-3.jpg").default}
                    ></img>
                    <h5 className="title">{this.state.username}</h5>
                  </a>
                  <p className="description">{this.state.email}</p>
                  <p className="description">{this.state.password}</p>
                  <p className="description">{this.state.role}</p>
                  <p className="description">{this.state.speciality}</p>
                  <p className="description">{this.state.phone}</p>




                </div>
                <p className="description text-center" style={{color:'darkgreen'}}>
                  This is the updated User Profile<br></br>
                  <br></br>
                
                </p>
              </Card.Body>
              <hr></hr>
              <div className="button-container mr-auto ml-auto">
                <Button
                  className="btn-simple btn-icon"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  variant="link"
                >
                  <i className="fab fa-facebook-square"></i>
                </Button>
                <Button
                  className="btn-simple btn-icon"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  variant="link"
                >
                  <i className="fab fa-twitter"></i>
                </Button>
                <Button
                  className="btn-simple btn-icon"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  variant="link"
                >
                  <i className="fab fa-google-plus-square"></i>
                </Button>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
}

export default User;
