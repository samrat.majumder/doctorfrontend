import React from "react";
import axios from 'axios'
// react-bootstrap components

import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import Activity from "components/Navbars/Activity";


class UserForm extends React.Component {
  state ={

      username: "",
      email: "",  
      phone: "",
      password: "",
      role:"Admin",
      speciality:"Rheumatology",
      activity:"",
      user_id:'',

      
      
    }



    handleChange = event => {
      this.setState({ name: event.target.value });
    }




    handleSubmit = event => {
      event.preventDefault();


      const user = {
        
        username: this.state.username,
        email: this.state.email,
        phone: this.state.phone,
        password: this.state.password,
        role: this.state.role,
        speciality: this.state.speciality,
        activity :this.state.activity,
        user_id:this.state.user_id,

      };

      axios.post(`https://abcapi.vidaria.in/addactivity`, { user })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })

      axios.post(`https://abcapi.vidaria.in/adduser`, { user })
      .then(res => {

        if (res.data.success == "false"){
  
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: res.data.message,
            
          })
        }

        console.log(res);
        console.log(res.data);
      })

      

  }







  render()


{
  return (
    <>
      <Container fluid>
        <Row>
          <Col className="pr-1" md="6">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Adding User</Card.Title>
              </Card.Header>
              <Card.Body>
                <Form onSubmit={this.handleSubmit} >
                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>Username</label>
                        <Form.Control
                          name="username"
                          placeholder="Username"
                          type="text"
                          value={this.state.username}
                          onChange={(e)=>this.setState({username:e.target.value})}

                        ></Form.Control>
                      </Form.Group>
                    </Col>


                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>
                          Email address
                        </label>
                        <Form.Control
                          placeholder="Email"
                          type="email"
                          value={this.state.email}
                          name="email"
                          onChange={(e)=>this.setState({email:e.target.value})}
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>


                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>Phone</label>
                        <Form.Control
                        name="phone"
                        onChange={(e)=>this.setState({phone:e.target.value})}
                        placeholder="phone"
                        value={this.state.phone}
                        type="number"
                        ></Form.Control>
                      </Form.Group>
                    </Col>

                    <Col className="pl-1" md="6">
                      <Form.Group>
                        <label>Password</label>
                        <Form.Control
                          name="password"
                          placeholder="password"
                          onChange={(e)=>this.setState({password:e.target.value})}
                          value={this.state.password}
                          type="password"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>

                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group controlId="exampleForm.ControlSelect1">
                        <label>Role</label>
                        <Form.Control as="select" name="role" placeholder="role" type="text" value={this.state.role} onChange={(e)=>this.setState({role:e.target.value})} >
                        <option   >Admin</option>
                        <option  >Doctor</option>
                        <option  >Staff</option>
                        </Form.Control>
                      </Form.Group>


                    </Col>
                
                  
                    <Col className="pr-1" md="6">
                      <Form.Group   controlId="exampleForm.ControlSelect1">
                        <label>Speciality</label>
                        <Form.Control as="select" name="speciality" value={this.state.speciality} onChange={(e)=>this.setState({speciality:e.target.value})} placeholder="Speciality" type="text">
                        <option  >Rheumatology</option>
                        <option>Breast Cancer</option>
                        </Form.Control>
                      </Form.Group>
                    </Col>


                  </Row> 



                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>User_id</label>
                        <Form.Control
                        name="user_id"
                        onChange={(e)=>this.setState({user_id:e.target.value})}
                        placeholder="user_id"
                        value={this.state.user_id}
                        type="number"
                        ></Form.Control>
                      </Form.Group>
                    </Col>

                    <Col className="pl-1" md="6">
                      <Form.Group>
                        <label>Activity</label>
                        <Form.Control
                          name="activity"
                          placeholder="activity"
                          onChange={(e)=>this.setState({activity:e.target.value})}
                          value={this.state.activity}
                          type="text"
                        ></Form.Control>
                      </Form.Group>
                    </Col>
                  </Row>




                  <Button
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                  >
                    Add User
                  </Button>
                  <div className="clearfix"></div>
                </Form>
              </Card.Body>
            </Card>
          </Col>
          <Col md="4">
            <Card className="card-user">
              <div className="card-image">
                <img
                  alt="..."
                  src={
                    require("assets/img/photo-1431578500526-4d9613015464.jpeg")
                      .default
                  }
                ></img>
              </div>
              <Card.Body>
                <div className="author">
                  <a href="#pablo" onClick={(e) => e.preventDefault()}>
                    <img
                      alt="..."
                      className="avatar border-gray"
                      src={require("assets/img/faces/face-3.jpg").default}
                    ></img>
                    <h5 className="title">{this.state.username}</h5>
                  </a>
                  <p className="description">{this.state.email}</p>
                  <p className="description">{this.state.password}</p>
                  <p className="description">{this.state.role}</p>
                  <p className="description">{this.state.speciality}</p>
                  <p className="description">{this.state.phone}</p>




                </div>
                <p className="description text-center" style={{color:'darkgreen'}}>
                   Adding Using Data checking<br></br>
                  <br></br>
                
                </p>
              </Card.Body>
              <hr></hr>
              <div className="button-container mr-auto ml-auto">
                <Button
                  className="btn-simple btn-icon"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  variant="link"
                >
                  <i className="fab fa-facebook-square"></i>
                </Button>
                <Button
                  className="btn-simple btn-icon"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  variant="link"
                >
                  <i className="fab fa-twitter"></i>
                </Button>
                <Button
                  className="btn-simple btn-icon"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  variant="link"
                >
                  <i className="fab fa-google-plus-square"></i>
                </Button>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
}

export default UserForm;
