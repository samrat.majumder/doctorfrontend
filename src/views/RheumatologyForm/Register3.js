
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Register2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>

        {/* <Row> */}

        <Form>
  
<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>Ethnic</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('ethnic')}  defaultValue={values.ethnic}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>Marital_status</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('marital_status')} defaultValue={values.marital_status}/>
                    </Form.Group>


         </Col> 
         <Col md="6">
         <Form.Group>

          <Form.Label>marital_status_years</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('marital_status_years')}  defaultValue={values.marital_status_years}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>menses_frequency</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('menses_frequency')} defaultValue={values.menses_frequency}/>
                    </Form.Group>


          </Col>
          </Row>


          <Row>
  <Col md="6">   
  <Form.Group>

          <Form.Label>menses_loss</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('menses_loss')} defaultValue={values.menses_loss}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>menarche_years</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('menarche_years')} defaultValue={values.menarche_years}/>
                    </Form.Group>

          </Col>
          <Col md="6">   

          <Form.Group>

          <Form.Label>hystrectomy</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('hysterectomy')} defaultValue={values.hysterectomy}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>hystrectomy_years</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('hysterectomy_years')} defaultValue={values.hysterectomy_years}/>
                    </Form.Group>
<br></br>





       </Col>
       </Row>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Register2;







