
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class BasicInfo extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>
<h4 style={{"color":"red"}}>please fill all input in this page</h4>
        

        <Form>
  
        <Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>Patient Id</Form.Label><br></br>
          <Form.Control type="number" placeholder="pid" required onChange={this.props.handleChange('pid')}
						defaultValue={values.pid}/>
          </Form.Group>

          {/* <Form.Group>
          <Form.Label>Date</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('date')} defaultValue={values.date}  />
          </Form.Group> */}
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control type="text" placeholder="code" onChange={this.props.handleChange('code')}  defaultValue={values.code}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>place</Form.Label><br></br>
          <Form.Control type="text" placeholder="place"  onChange={this.props.handleChange('place')} defaultValue={values.place}/>
          </Form.Group>
          </Col>
          </Row>


          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>Patient Name</Form.Label><br></br>
          <Form.Control type="text" placeholder="patient name" required  onChange={this.props.handleChange('pname')} defaultValue={values.pname}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Father or Husband name</Form.Label><br></br>
          <Form.Control type="text" placeholder="father or husband name" name="fhname"  onChange={this.props.handleChange('fhname')} defaultValue={values.fhname}/>
          </Form.Group>
</Col>
<Col md="6">

<Form.Group>
          <Form.Label>Age</Form.Label><br></br>
          <Form.Control type="number"   placeholder="age" name="age" maxLength="100" required onChange={this.props.handleChange('age')} defaultValue={values.age}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Gender</Form.Label><br></br>
          <Form.Control type="text"  placeholder="gender" name="sex" required onChange={this.props.handleChange('sex')} defaultValue={values.sex}/>
          </Form.Group>

</Col>
</Row>
{/* 

<Row>
  <Col md="6">
  <Form.Group>

            <Form.Label>Education</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('education')} defaultValue={values.education}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>Address</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('address')} defaultValue={values.address}/>
                    </Form.Group>


</Col>
<Col md="6">
<Form.Group>

          <Form.Label>Phone</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('phone')} defaultValue={values.phone}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>District</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('district')} defaultValue={values.district}/>
                    </Form.Group>


</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>State</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('state')} defaultValue={values.state}/>
                    </Form.Group>


          <Form.Group>


          <Form.Label>Area</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('area')}  defaultValue={values.area}/>
                    </Form.Group>


          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>Referred_by</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('referred_by')}  defaultValue={values.referred_by}/>
                    </Form.Group>

          <Form.Group>

          <Form.Label>Occupation</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('occupation')}  defaultValue={values.occupation}/>
                    </Form.Group>


</Col>
</Row>

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>Ethnic</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('ethnic')}  defaultValue={values.ethnic}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>Marital_status</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('marital_status')} defaultValue={values.marital_status}/>
                    </Form.Group>


         </Col> 
         <Col md="6">
         <Form.Group>

          <Form.Label>marital_status_years</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('marital_status_years')}  defaultValue={values.marital_status_years}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>menses_frequency</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('menses_frequency')} defaultValue={values.menses_frequency}/>
                    </Form.Group>


          </Col>
          </Row>


          <Row>
  <Col md="6">   
  <Form.Group>

          <Form.Label>menses_loss</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('menses_loss')} defaultValue={values.menses_loss}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>menarche_years</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('menarche_years')} defaultValue={values.menarche_years}/>
                    </Form.Group>

          </Col>
          <Col md="6">   

          <Form.Group>

          <Form.Label>hystrectomy</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('hystrectomy')} defaultValue={values.hystrectomy}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>hystrectomy_years</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('hystrectomy_years')} defaultValue={values.hystrectomy_years}/>
                    </Form.Group>
<br></br>





       </Col>
       </Row>


       <Row>
          <Col md="6">
       
          <Form.Group>
          <Form.Label>menopause</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('menopause')}
defaultValue={values.menopause}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>menopause_years</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('menopause_years')}
 defaultValue={values.menopause_years}/>
           </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>

          <Form.Label>no of children</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children')}
defaultValue={values.children}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>children_male</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children_male')}
 defaultValue={values.children_male}/>
           </Form.Group>

</Col>
</Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>children_female</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children_female')}
defaultValue={values.children_female}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('abortions')}
 defaultValue={values.abortions}/>
           </Form.Group>
</Col>
<Col md="6">
<Form.Group>
          <Form.Label>abortions_number</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('abortions_number')}
 defaultValue={values.abortions_number}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions_cause</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('abortions_cause')}
 defaultValue={values.abortions_cause}/>
           </Form.Group>
         </Col>
         </Row>


         <Row>
         <Col md="6">
         <Form.Group>
           <Form.Label>current_lactation</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('current_lactation')}
defaultValue={values.current_lactation}/>
          </Form.Group>

          <Form.Group>
           <Form.Label>contraception_methods</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('contraception_methods')}
defaultValue={values.contraception_methods}/>
          </Form.Group>
             </Col>

             <Col md="6">
             <Form.Group>
         <Form.Label>contraception_methods_type</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('contraception_methods_type')}
defaultValue={values.contraception_methods_type}/>
          </Form.Group>

          <Form.Group>
           <Form.Label>hormone_treatment</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('hormone_treatment')}
defaultValue={values.hormone_treatment}/>
          </Form.Group>
             </Col>
         </Row>

<Row>
<Col md="6">
<Form.Group>
           <Form.Label>addiction</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('addiction')}
defaultValue={values.addiction}/>
          </Form.Group>
          <Form.Group>
           <Form.Label>tobacco</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('tobacco')}
defaultValue={values.tobacco}/>
          </Form.Group>
    </Col>
    <Col md="6">

    <Form.Group>
          <Form.Label>tobacco_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('tobacco_years')}
defaultValue={values.tobacco_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>smoking</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('smoking')}
 defaultValue={values.smoking}/>
          </Form.Group>
        </Col>

</Row>

<Row>
<Col md="6">
<Form.Group>
          <Form.Label>smoking_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('smoking_years')}
defaultValue={values.smoking_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>alcohol</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('alcohol')}
defaultValue={values.alcohol}/>
          </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>alcohol_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('alcohol_years')}
 defaultValue={values.alcohol_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>family_history</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('family_history')}
 defaultValue={values.family_history}/>
          </Form.Group>
</Col>

</Row>

<Row>

<Col md="6">
<Form.Group>
          <Form.Label>Remarks</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('remarks')}
defaultValue={values.remarks}/>
          </Form.Group>

   

</Col>

<Col md="6">
<Form.Group>
          <Form.Label>comorbidities</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('comorbidities')}
defaultValue={values.comorbidities}/>
          </Form.Group>
          <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>


</Row>


 */}






<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>



        </Form>
   
      
      </Container>
    )
  }
}

export default BasicInfo;







