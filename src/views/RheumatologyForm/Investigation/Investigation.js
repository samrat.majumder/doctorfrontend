
import React from "react";
import axios from 'axios'
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Table
} from "react-bootstrap";
import userdetails  from '../../admin';
import Swal from 'sweetalert2'

class Investigation_Main extends React.Component {
    
  constructor(props){
    super(props);
    this.state ={
     
        pid:'',
        user_id:'',


        can_u_dress_urself:'',
        can_u_wash_ur_hair:'',
        can_u_comb_ur_hair:'',
        dressing_score:false,



        can_u_stand_from_chair:'',
        can_u_get_inout_from_bed:'',
        can_u_sit_grossteg_onfloor:'',
        arising_score:false,


        can_u_cut_vegetables:'',
        can_u_lift_glass:'',
        can_u_break_roti_from_1hand:'',
        eating_score:false,


        can_u_walk:'',
        can_u_climb_5steps:'',
        walking_score:false,

        can_u_take_bath:'',
        can_u_wash_dry_urbody:'',
        can_u_get_onoff_toilet:'',
        hygiene_score:false,


        can_u_weigh_2kg:'',
        can_u_bend_and_pickcloths:'',
        reaching_score:false,


        can_u_open_bottle:'',
        can_u_turntaps_onoff:'',
        can_u_open_latches:'',
        grip_score:false,

        can_u_work_office_house:'',
        can_u_run_errands:'',
        can_u_get_inout_of_bus:'',
        activities_score:false,

        patient_assessment_pain:false,
        grip_strength_rt:false,
        grip_strength_hg:false,
        grip_strength_lt:false,
        early_mrng_stiffness:false,
        assess_sleep:'',
        general_health_assessment:false,
        classification_criteria_followig:'',
        diagnosis:'',
        treatment_plan:'',



        hematological_date:'',
        hem_esr:'',
        hem_hb:'',
        hem_tlc:'',
        hem_pleb:'',
        hem_plat:'',
        hem_urine:'',
        hem_others:'',


        hematological_date2:'',
        hem_esr2:'',
        hem_hb2:'',
        hem_tlc2:'',
        hem_pleb2:'',
        hem_plat2:'',
        hem_urine2:'',
        hem_others2:'',


        hematological_date3:'',
        hem_esr3:'',
        hem_hb3:'',
        hem_tlc3:'',
        hem_pleb3:'',
        hem_plat3:'',
        hem_urine3:'',
        hem_others3:'',


        hematological_date4:'',
        hem_esr4:'',
        hem_hb4:'',
        hem_tlc4:'',
        hem_pleb4:'',
        hem_plat4:'',
        hem_urine4:'',
        hem_others4:'',



        hematological_date5:'',
        hem_esr5:'',
        hem_hb5:'',
        hem_tlc5:'',
        hem_pleb5:'',
        hem_plat5:'',
        hem_urine5:'',
        hem_others5:'',


        biochemical_date:'',
        bio_lft:'',
        bio_rft:'',
        bio_crp:'',
        bio_bsl:'',
        bio_ua:'',
        bio_others:'',



        biochemical_date2:'',
        bio_lft2:'',
        bio_rft2:'',
        bio_crp2:'',
        bio_bsl2:'',
        bio_ua2:'',
        bio_others2:'',


        biochemical_date3:'',
        bio_lft3:'',
        bio_rft3:'',
        bio_crp3:'',
        bio_bsl3:'',
        bio_ua3:'',
        bio_others3:'',



        biochemical_date4:'',
        bio_lft4:'',
        bio_rft4:'',
        bio_crp4:'',
        bio_bsl4:'',
        bio_ua4:'',
        bio_others4:'',


        biochemical_date5:'',
        bio_lft5:'',
        bio_rft5:'',
        bio_crp5:'',
        bio_bsl5:'',
        bio_ua5:'',
        bio_others5:'',



        immunological_date:'',
        imm_rf:'',
        imm_accp:'',
        imm_ana:'',
        imm_anawb:'',
        imm_ace:'',
        imm_dsdna:'',
        imm_hlab27:'',
        imm_c3:'',
        imm_c4:'',
        imm_others:'',



        immunological_date2:'',
        imm_rf2:'',
        imm_accp2:'',
        imm_ana2:'',
        imm_anawb2:'',
        imm_ace2:'',
        imm_dsdna2:'',
        imm_hlab272:'',
        imm_c32:'',
        imm_c42:'',
        imm_others2:'',



        immunological_date3:'',
        imm_rf3:'',
        imm_accp3:'',
        imm_ana3:'',
        imm_anawb3:'',
        imm_ace3:'',
        imm_dsdna3:'',
        imm_hlab273:'',
        imm_c33:'',
        imm_c43:'',
        imm_others3:'',



        usg:'',
        xray_chest:'',
        xray_joints:'',
        mri:'',
        radio_remarks:'',
        das28:'',
        sledai:'',
        basdai:'',
        pasi:'',
        bvas:'',
        mrss:'',
        sdai_cdai:'',
        asdas:'',
        dapsa:'',
        vdi:'',
        essdai:'',
        
    
    
    }

  }
  

// changeHandler=(e) => {
//   this.setState({[e.target.name]:e.target.value})
// }


  componentDidMount(){
    axios.get(`http://127.0.0.1:5000/rheumapatientdetails?X-AUTH=abc123&pid=${this.props.match.params.id}`)
    .then((res)=>{
      console.log(res)
      this.setState({
        id:res.data.RheumatologyPatientHistory.id,
user_id:res.data.RheumatologyPatientHistory.user_id,
pid:res.data.RheumatologyPatientHistory.pid
   
      
        
      })
    })
  }


  opensweetalert()
  {
    Swal.fire({
      title: 'Patient record added',
      text: "success",
      type: 'success',
      
    }).then(function() {
      window.location = "/admin/dashboard";
  });
  }

  submit= e => {
    e.preventDefault()
   
    
   
      axios.post("http://127.0.0.1:5000/addrheumapatientinvestigation?X-AUTH=abc123",{


    pid:this.state.pid,



    user_id:this.state.user_id,

    can_u_dress_urself:this.state.can_u_dress_urself,
    can_u_wash_ur_hair:this.state.can_u_wash_ur_hair,



    can_u_comb_ur_hair:this.state.can_u_comb_ur_hair,
    dressing_score:this.state.dressing_score,

    can_u_stand_from_chair:this.state.can_u_stand_from_chair,
    can_u_get_inout_from_bed:this.state.can_u_get_inout_from_bed,


    can_u_sit_grossteg_onfloor:this.state.can_u_sit_grossteg_onfloor,

    arising_score:this.state.arising_score,


    can_u_cut_vegetables:this.state.can_u_cut_vegetables,
    can_u_lift_glass:this.state.can_u_lift_glass,
    can_u_break_roti_from_1hand:this.state.can_u_break_roti_from_1hand,
    eating_score:this.state.eating_score,



    can_u_walk:this.state.can_u_walk,
    can_u_climb_5steps:this.state.can_u_climb_5steps,
    walking_score:this.state.walking_score,






    can_u_take_bath:this.state.can_u_take_bath,
    can_u_wash_dry_urbody:this.state.can_u_wash_dry_urbody,
    can_u_get_onoff_toilet:this.state.can_u_get_onoff_toilet,
    hygiene_score:this.state.hygiene_score,




    can_u_weigh_2kg:this.state.can_u_weigh_2kg,
    can_u_bend_and_pickcloths:this.state.can_u_bend_and_pickcloths,
    reaching_score:this.state.reaching_score,


    can_u_open_bottle:this.state.can_u_open_bottle,




    can_u_turntaps_onoff:this.state.can_u_turntaps_onoff,  
    can_u_open_latches:this.state.can_u_open_latches,
    grip_score:this.state.grip_score,



    can_u_work_office_house:this.state.can_u_work_office_house,



    can_u_run_errands:this.state.can_u_run_errands,
    can_u_get_inout_of_bus:this.state.can_u_get_inout_of_bus,
    activities_score:this.state.activities_score,



    patient_assessment_pain:this.state.patient_assessment_pain,


    grip_strength_rt:this.state.grip_strength_rt,
    grip_strength_hg:this.state.grip_strength_hg,
    grip_strength_lt:this.state.grip_strength_lt,


    early_mrng_stiffness:this.state.early_mrng_stiffness,
    assess_sleep:this.state.assess_sleep,
    general_health_assessment:this.state.general_health_assessment,


    classification_criteria_followig:this.state.classification_criteria_followig,
    diagnosis:this.state.diagnosis,
    treatment_plan:this.state.treatment_plan,



    hematological_date:this.state.hematological_date,
    hem_esr:this.state.hem_esr,
    hem_hb:this.state.hem_hb,
    hem_tlc:this.state.hem_tlc,
    hem_pleb:this.state.hem_pleb,
    hem_plat:this.state.hem_plat,
    hem_urine:this.state.hem_urine,
    hem_others:this.state.hem_others,


    hematological_date2:this.state.hematological_date2,
    hem_esr2:this.state.hem_esr2,
    hem_hb2:this.state.hem_hb2,
    hem_tlc2:this.state.hem_tlc2,
    hem_pleb2:this.state.hem_pleb2,
    hem_plat2:this.state.hem_plat2,
    hem_urine2:this.state.hem_urine2,
    hem_others2:this.state.hem_others2,


    hematological_date3:this.state.hematological_date3,
    hem_esr3:this.state.hem_esr3,
    hem_hb3:this.state.hem_hb3,
    hem_tlc3:this.state.hem_tlc3,
    hem_pleb3:this.state.hem_pleb3,
    hem_plat3:this.state.hem_plat3,
    hem_urine3:this.state.hem_urine3,
    hem_others3:this.state.hem_others3,




    hematological_date4:this.state.hematological_date4,
    hem_esr4:this.state.hem_esr4,
    hem_hb4:this.state.hem_hb4,
    hem_tlc4:this.state.hem_tlc4,
    hem_pleb4:this.state.hem_pleb4,
    hem_plat4:this.state.hem_plat4,
    hem_urine4:this.state.hem_urine4,
    hem_others4:this.state.hem_others4,


    hematological_date5:this.state.hematological_date5,
    hem_esr5:this.state.hem_esr5,
    hem_hb5:this.state.hem_hb5,
    hem_tlc5:this.state.hem_tlc5,
    hem_pleb5:this.state.hem_pleb5,
    hem_plat5:this.state.hem_plat5,
    hem_urine5:this.state.hem_urine5,
    hem_others5:this.state.hem_others5,


    biochemical_date:this.state.biochemical_date,
    bio_lft:this.state.bio_lft,
    bio_rft:this.state.bio_rft,
    bio_crp:this.state.bio_crp,
    bio_bsl:this.state.bio_bsl,
    bio_ua:this.state.bio_ua,
    bio_others:this.state.bio_others,


    biochemical_date2:this.state.biochemical_date2,
    bio_lft2:this.state.bio_lft2,
    bio_rft2:this.state.bio_rft2,
    bio_crp2:this.state.bio_crp2,
    bio_bsl2:this.state.bio_bsl2,
    bio_ua2:this.state.bio_ua2,
    bio_others2:this.state.bio_others2,


    biochemical_date3:this.state.biochemical_date3,
    bio_lft3:this.state.bio_lft3,
    bio_rft3:this.state.bio_rft3,
    bio_crp3:this.state.bio_crp3,
    bio_bsl3:this.state.bio_bsl3,
    bio_ua3:this.state.bio_ua3,
    bio_others3:this.state.bio_others3,




    biochemical_date4:this.state.biochemical_date4,
    bio_lft4:this.state.bio_lft4,
    bio_rft4:this.state.bio_rft4,
    bio_crp4:this.state.bio_crp4,
    bio_bsl4:this.state.bio_bsl4,
    bio_ua4:this.state.bio_ua4,
    bio_others4:this.state.bio_others4,


    biochemical_date5:this.state.biochemical_date5,
    bio_lft5:this.state.bio_lft5,
    bio_rft5:this.state.bio_rft5,
    bio_crp5:this.state.bio_crp5,
    bio_bsl5:this.state.bio_bsl5,
    bio_ua5:this.state.bio_ua5,
    bio_others5:this.state.bio_others5,


    immunological_date:this.state.immunological_date,
    imm_rf:this.state.imm_rf,
    imm_accp:this.state.imm_accp,
    imm_ana:this.state.imm_ana,
    imm_anawb:this.state.imm_anawb,
    imm_ace:this.state.imm_ace,
    imm_dsdna:this.state.imm_dsdna,
    imm_hlab27:this.state.imm_hlab27,
    imm_c3:this.state.imm_c3,
    imm_c4:this.state.imm_c4,
    imm_others:this.state.imm_others,



    immunological_date2:this.state.immunological_date2,
    imm_rf2:this.state.imm_rf2,
    imm_accp2:this.state.imm_accp2,
    imm_ana2:this.state.imm_ana2,
    imm_anawb2:this.state.imm_anawb2,
    imm_ace2:this.state.imm_ace2,
    imm_dsdna2:this.state.imm_dsdna2,
    imm_hlab272:this.state.imm_hlab272,
    imm_c32:this.state.imm_c32,
    imm_c42:this.state.imm_c42,
    imm_others2:this.state.imm_others2,


    immunological_date3:this.state.immunological_date3,
    imm_rf3:this.state.imm_rf3,
    imm_accp3:this.state.imm_accp3,
    imm_ana3:this.state.imm_ana3,
    imm_anawb3:this.state.imm_anawb3,
    imm_ace3:this.state.imm_ace3,
    imm_dsdna3:this.state.imm_dsdna3,
    imm_hlab273:this.state.imm_hlab273,
    imm_c33:this.state.imm_c33,
    imm_c43:this.state.imm_c43,
    imm_others3:this.state.imm_others3,


    usg:this.state.usg,
    xray_chest:this.state.xray_chest,
    xray_joints:this.state.xray_joints,
    mri:this.state.mri,
    radio_remarks:this.state.radio_remarks,
    das28:this.state.das28,
    sledai:this.state.sledai,



    basdai:this.state.basdai,
    pasi:this.state.pasi,
    bvas:this.state.bvas,


    mrss:this.state.mrss,
    sdai_cdai:this.state.sdai_cdai,
    asdas:this.state.asdas,


    dapsa:this.state.dapsa,
    vdi:this.state.vdi,
    essdai:this.state.essdai,


   
      })
      axios.post(`http://127.0.0.1:5000/addactivity?X-AUTH=abc123`,{

        activity :"added Investigation details of Rheumatology  patient",
        user_id:this.state.user_id,
  
      })
      .then(()=>{
        this.opensweetalert()
        this.props.history.push(`/admin/dashboard`)
      })

    }

  
  // delete(id){
  //   axios.delete(`http://127.0.0.1:5000/userdelete/${id}`)
  //   .then(()=>{
  //     this.componentDidMount();
  //   })
  // }
 

  render()

{
   
  return (
    <>
      <Container fluid>
        <Row>
          <Col md="8">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Edit Profile</Card.Title>
                <h3>Rheumatology  Investigation</h3><br></br>

              </Card.Header>
              <Card.Body>
                <Form onSubmit={this.submit}>


                <Row>
  <Col md="6">
  {/* <Form.Group>
          <Form.Label>pid</Form.Label><br></br>
           <Form.Control  
           type="number"  
           name="pid"
           readOnly
            placeholder="pid"
          value={this.state.pid}
          onChange={(e)=>this.setState({pid:e.target.value})}
          
           />
          </Form.Group> */}

         </Col> 
      
         <Col md="6">
     

          {/* <Form.Group>
          <Form.Label>user id</Form.Label><br></br>
           <Form.Control  
           type="number" 
           readOnly 
           name="user_id"
            placeholder="user Id"
          value={this.state.user_id}
          onChange={(e)=>this.setState({user_id:e.target.value})}
           />
          </Form.Group> */}

          </Col>
          </Row>

<Row>

<h5>hematological date: Information</h5> 

                <Table striped bordered hover>
  <thead>
    <tr>
    <th>DATE</th>
      <th>ESR</th>
      <th>HB</th>
      <th>TLC</th>
      <th>PLEB</th>
      <th>PLAT</th>
      <th>URINE</th>
      <th>OTHERS</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input name="hematological_date"                          
           placeholder="hematological_date" type="text" 	onChange={(e)=>this.setState({hematological_date:e.target.value})}
Value={this.state.hematological_date} maxlength="50" size="4" /></td>

      <td><input name="hem_esr"                          
           placeholder="hem_esr" type="text" 	onChange={(e)=>this.setState({hem_esr:e.target.value})}
Value={this.state.hem_esr} maxlength="50" size="4" /></td>
     
     <td><input name="hem_hb"                          
           placeholder="hem_hb" type="text" 						onChange={(e)=>this.setState({hem_hb:e.target.value})}
Value={this.state.hem_hb} maxlength="50" size="4" /></td>

      <td><input name="hem_tlc"                          
           placeholder="hem_tlc" type="text" 	onChange={(e)=>this.setState({hem_tlc:e.target.value})}
Value={this.state.hem_tlc} maxlength="50" size="4" /></td>


            <td><input name="hem_pleb"                          
           placeholder="hem_pleb" type="text" 						onChange={(e)=>this.setState({hem_pleb:e.target.value})}
Value={this.state.hem_pleb} maxlength="50" size="4" /></td>

            <td><input name="hem_plat"                          
           placeholder="hem_plat" type="text" 						onChange={(e)=>this.setState({hem_plat:e.target.value})}
Value={this.state.hem_plat} maxlength="50" size="4" /></td>


<td><input name="hem_urine"                          
           placeholder="hem_urine" type="text" 						onChange={(e)=>this.setState({hem_urine:e.target.value})}
Value={this.state.hem_urine} maxlength="50" size="4" /></td>


<td><input name="hem_others"                          
           placeholder="hem_others" type="text" 						onChange={(e)=>this.setState({hem_others:e.target.value})}
Value={this.state.hem_others} maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td><input name="hematological_date2"                          
           placeholder="hematological_date2" type="text" 	onChange={(e)=>this.setState({hematological_date2:e.target.value})}
Value={this.state.hematological_date2} maxlength="50" size="4" /></td>

      <td><input name="hem_esr2"                          
           placeholder="hem_esr2" type="text" 	onChange={(e)=>this.setState({hem_esr2:e.target.value})}
Value={this.state.hem_esr2} maxlength="50" size="4" /></td>
     
     <td><input name="hem_hb2"                          
           placeholder="hem_hb2" type="text" 						onChange={(e)=>this.setState({hem_hb2:e.target.value})}
Value={this.state.hem_hb2} maxlength="50" size="4" /></td>

      <td><input name="hem_tlc2"                          
           placeholder="hem_tlc2" type="text" 	onChange={(e)=>this.setState({hem_tlc2:e.target.value})}
Value={this.state.hem_tlc2} maxlength="50" size="4" /></td>
            <td><input name="hem_pleb2"                          
           placeholder="hem_pleb2" type="text" 						onChange={(e)=>this.setState({hem_pleb2:e.target.value})}
Value={this.state.hem_pleb2} maxlength="50" size="4" /></td>

            <td><input name="hem_plat2"                          
           placeholder="hem_plat2" type="text" 						onChange={(e)=>this.setState({hem_plat2:e.target.value})}
Value={this.state.hem_plat2} maxlength="50" size="4" /></td>

<td><input name="hem_urine2"                          
           placeholder="hem_urine2" type="text" 						onChange={(e)=>this.setState({hem_urine2:e.target.value})}
Value={this.state.hem_urine2} maxlength="50" size="4" /></td>

<td><input name="hem_others2"                          
           placeholder="hem_others2" type="text" 						onChange={(e)=>this.setState({hem_others2:e.target.value})}
Value={this.state.hem_others2} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td><input name="hematological_date3"                          
           placeholder="hematological_date3" type="text" 	onChange={(e)=>this.setState({hematological_date3:e.target.value})}
Value={this.state.hematological_date3} maxlength="50" size="4" /></td>

      <td><input name="hem_esr3"                          
           placeholder="hem_esr3" type="text" 	onChange={(e)=>this.setState({hem_esr3:e.target.value})}
Value={this.state.hem_esr3} maxlength="50" size="4" /></td>
     
     <td><input name="hem_hb3"                          
           placeholder="hem_hb3" type="text" 						onChange={(e)=>this.setState({hem_hb3:e.target.value})}
Value={this.state.hem_hb3} maxlength="50" size="4" /></td>

      <td><input name="hem_tlc3"                          
           placeholder="hem_tlc3" type="text" 	onChange={(e)=>this.setState({hem_tlc3:e.target.value})}
Value={this.state.hem_tlc3} maxlength="50" size="4" /></td>
            <td><input name="hem_pleb3"                          
           placeholder="hem_pleb3 id" type="text" 						onChange={(e)=>this.setState({hem_pleb3:e.target.value})}
Value={this.state.hem_pleb3} maxlength="50" size="4" /></td>

            <td><input name="hem_plat3"                          
           placeholder="hem_plat3" type="text" 						onChange={(e)=>this.setState({hem_plat3:e.target.value})}
Value={this.state.hem_plat3} maxlength="50" size="4" /></td>


<td><input name="hem_urine3"                          
           placeholder="hem_urine3" type="text" 						onChange={(e)=>this.setState({hem_urine3:e.target.value})}
Value={this.state.hem_urine3} maxlength="50" size="4" /></td>


<td><input name="hem_others3"                          
           placeholder="hem_others3" type="text" 						onChange={(e)=>this.setState({hem_others3:e.target.value})}
Value={this.state.hem_others3} maxlength="50" size="4" /></td>

    </tr>

    <tr>
      <td><input name="hematological_date4"                          
           placeholder="hematological_date4" type="text" 	onChange={(e)=>this.setState({hematological_date4:e.target.value})}
Value={this.state.hematological_date4} maxlength="50" size="4" /></td>

      <td><input name="hem_esr4"                          
           placeholder="hem_esr4" type="text" 	onChange={(e)=>this.setState({hem_esr4:e.target.value})}
Value={this.state.hem_esr4} maxlength="50" size="4" /></td>
     
     <td><input name="hem_hb4"                          
           placeholder="hem_hb4" type="text" 						onChange={(e)=>this.setState({hem_hb4:e.target.value})}
Value={this.state.hem_hb4} maxlength="50" size="4" /></td>

      <td><input name="hem_tlc4"                          
           placeholder="hem_tlc4" type="text" 	onChange={(e)=>this.setState({hem_tlc4:e.target.value})}
Value={this.state.hem_tlc4} maxlength="50" size="4" /></td>
            <td><input name="hem_pleb4"                          
           placeholder="hem_pleb4" type="text" 						onChange={(e)=>this.setState({hem_pleb4:e.target.value})}
Value={this.state.hem_pleb4} maxlength="50" size="4" /></td>

            <td><input name="hem_plat4"                          
           placeholder="hem_plat4" type="text" 						onChange={(e)=>this.setState({hem_plat4:e.target.value})}
Value={this.state.hem_plat4} maxlength="50" size="4" /></td>


<td><input name="hem_urine4"                          
           placeholder="hem_urine4" type="text" 						onChange={(e)=>this.setState({hem_urine4:e.target.value})}
Value={this.state.hem_urine4} maxlength="50" size="4" /></td>


<td><input name="hem_others4"                          
           placeholder="hem_others4" type="text" 						onChange={(e)=>this.setState({hem_others4:e.target.value})}
Value={this.state.hem_others4} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td><input name="hematological_date5"                          
           placeholder="hematological_date5" type="text" 	onChange={(e)=>this.setState({hematological_date5:e.target.value})}
Value={this.state.hematological_date5} maxlength="50" size="5" /></td>

      <td><input name="hem_esr5"                          
           placeholder="hem_esr5" type="text" 	onChange={(e)=>this.setState({hem_esr5:e.target.value})}
Value={this.state.hem_esr5} maxlength="50" size="5" /></td>
     
     <td><input name="hem_hb5"                          
           placeholder="hem_hb5" type="text" 						onChange={(e)=>this.setState({hem_hb5:e.target.value})}
Value={this.state.hem_hb5} maxlength="50" size="5" /></td>

      <td><input name="hem_tlc5"                          
           placeholder="hem_tlc5" type="text" 	onChange={(e)=>this.setState({hem_tlc5:e.target.value})}
Value={this.state.hem_tlc5} maxlength="50" size="5" /></td>
            <td><input name="hem_pleb5"                          
           placeholder="hem_pleb5" type="text" 						onChange={(e)=>this.setState({hem_pleb5:e.target.value})}
Value={this.state.hem_pleb5} maxlength="50" size="5" /></td>

            <td><input name="hem_plat5"                          
           placeholder="hem_plat5" type="text" 						onChange={(e)=>this.setState({hem_plat5:e.target.value})}
Value={this.state.hem_plat5} maxlength="50" size="5" /></td>


<td><input name="hem_urine5"                          
           placeholder="hem_urine5" type="text" 						onChange={(e)=>this.setState({hem_urine5:e.target.value})}
Value={this.state.hem_urine5} maxlength="50" size="5" /></td>


<td><input name="hem_others5"                          
           placeholder="hem_others5" type="text" 						onChange={(e)=>this.setState({hem_others5:e.target.value})}
Value={this.state.hem_others5} maxlength="50" size="5" /></td>

    </tr>
   
  </tbody>
</Table>

          </Row>


          <Row>
          <h5>Biochemical date: Information</h5> 

        <Table striped bordered hover>
  <thead>
    <tr>
    <th>DATE</th>
      <th>LFT (Bill(T),protein etc</th>
      <th>RFT BUN,CR,UREA</th>
      <th>CRP</th>
      <th>BSL</th>
      <th>UA</th>
      <th>OTHERS</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input name="biochemical_date"                          
           placeholder="biochemical_date" type="text" 	onChange={(e)=>this.setState({biochemical_date:e.target.value})}
Value={this.state.biochemical_date} maxlength="50" size="4" /></td>

      <td><input name="bio_lft"                          
           placeholder="bio_lft" type="text" 	onChange={(e)=>this.setState({bio_lft:e.target.value})}
Value={this.state.bio_lft} maxlength="50" size="4" /></td>
     
     <td><input name="bio_rft"                          
           placeholder="bio_rft" type="text" 						onChange={(e)=>this.setState({bio_rft:e.target.value})}
Value={this.state.bio_rft} maxlength="50" size="4" /></td>


<td><input name="bio_crp"                          
           placeholder="bio_crp" type="text" 	onChange={(e)=>this.setState({bio_crp:e.target.value})}
Value={this.state.bio_crp} maxlength="50" size="4" /></td>

      <td><input name="bio_bsl"                          
           placeholder="bio_bsl" type="text" 	onChange={(e)=>this.setState({bio_bsl:e.target.value})}
Value={this.state.bio_bsl} maxlength="50" size="4" /></td>


            <td><input name="bio_ua"                          
           placeholder="bio_ua" type="text" 						onChange={(e)=>this.setState({bio_ua:e.target.value})}
Value={this.state.bio_ua} maxlength="50" size="4" /></td>

            <td><input name="bio_others"                          
           placeholder="bio_others" type="text" 						onChange={(e)=>this.setState({bio_others:e.target.value})}
Value={this.state.bio_others} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td><input name="biochemical_date2"                          
           placeholder="biochemical_date2" type="text" 	onChange={(e)=>this.setState({biochemical_date2:e.target.value})}
Value={this.state.biochemical_date2} maxlength="50" size="4" /></td>

      <td><input name="bio_lft2"                          
           placeholder="bio_lft2" type="text" 	onChange={(e)=>this.setState({bio_lft2:e.target.value})}
Value={this.state.bio_lft2} maxlength="50" size="4" /></td>
     
     <td><input name="bio_rft2"                          
           placeholder="bio_rft2" type="text" 						onChange={(e)=>this.setState({bio_rft2:e.target.value})}
Value={this.state.bio_rft2} maxlength="50" size="4" /></td>


<td><input name="bio_crp2"                          
           placeholder="bio_crp2" type="text" 	onChange={(e)=>this.setState({bio_crp2:e.target.value})}
Value={this.state.bio_crp2} maxlength="50" size="4" /></td>

      <td><input name="bio_bsl2"                          
           placeholder="bio_bsl2" type="text" 	onChange={(e)=>this.setState({bio_bsl2:e.target.value})}
Value={this.state.bio_bsl2} maxlength="50" size="4" /></td>


            <td><input name="bio_ua2"                          
           placeholder="bio_ua2" type="text" 						onChange={(e)=>this.setState({bio_ua2:e.target.value})}
Value={this.state.bio_ua2} maxlength="50" size="4" /></td>

            <td><input name="bio_others2"                          
           placeholder="bio_others2" type="text" 						onChange={(e)=>this.setState({bio_others2:e.target.value})}
Value={this.state.bio_others2} maxlength="50" size="4" /></td>

    </tr>





    <tr>
      <td><input name="biochemical_date3"                          
           placeholder="biochemical_date3" type="text" 	onChange={(e)=>this.setState({biochemical_date3:e.target.value})}
Value={this.state.biochemical_date3} maxlength="50" size="4" /></td>

      <td><input name="bio_lft3"                          
           placeholder="bio_lft3" type="text" 	onChange={(e)=>this.setState({bio_lft3:e.target.value})}
Value={this.state.bio_lft3} maxlength="50" size="4" /></td>
     
     <td><input name="bio_rft3"                          
           placeholder="bio_rft3" type="text" 						onChange={(e)=>this.setState({bio_rft3:e.target.value})}
Value={this.state.bio_rft3} maxlength="50" size="4" /></td>


<td><input name="bio_crp3"                          
           placeholder="bio_crp3" type="text" 	onChange={(e)=>this.setState({bio_crp3:e.target.value})}
Value={this.state.bio_crp3} maxlength="50" size="4" /></td>

      <td><input name="bio_bsl3"                          
           placeholder="bio_bsl3" type="text" 	onChange={(e)=>this.setState({bio_bsl3:e.target.value})}
Value={this.state.bio_bsl3} maxlength="50" size="4" /></td>


            <td><input name="bio_ua3"                          
           placeholder="bio_ua3" type="text" 						onChange={(e)=>this.setState({bio_ua3:e.target.value})}
Value={this.state.bio_ua3} maxlength="50" size="4" /></td>

            <td><input name="bio_others3"                          
           placeholder="bio_others3" type="text" 						onChange={(e)=>this.setState({bio_others3:e.target.value})}
Value={this.state.bio_others3} maxlength="50" size="4" /></td>

    </tr>




    <tr>
      <td><input name="biochemical_date4"                          
           placeholder="biochemical_date4" type="text" 	onChange={(e)=>this.setState({biochemical_date4:e.target.value})}
Value={this.state.biochemical_date4} maxlength="50" size="4" /></td>

      <td><input name="bio_lft4"                          
           placeholder="bio_lft4" type="text" 	onChange={(e)=>this.setState({bio_lft4:e.target.value})}
Value={this.state.bio_lft4} maxlength="50" size="4" /></td>
     
     <td><input name="bio_rft4"                          
           placeholder="bio_rft4" type="text" 						onChange={(e)=>this.setState({bio_rft4:e.target.value})}
Value={this.state.bio_rft4} maxlength="50" size="4" /></td>


<td><input name="bio_crp4"                          
           placeholder="bio_crp4" type="text" 	onChange={(e)=>this.setState({bio_crp4:e.target.value})}
Value={this.state.bio_crp4} maxlength="50" size="4" /></td>

      <td><input name="bio_bsl4"                          
           placeholder="bio_bsl4" type="text" 	onChange={(e)=>this.setState({bio_bsl4:e.target.value})}
Value={this.state.bio_bsl4} maxlength="50" size="4" /></td>


            <td><input name="bio_ua4"                          
           placeholder="bio_ua4" type="text" 						onChange={(e)=>this.setState({bio_ua4:e.target.value})}
Value={this.state.bio_ua4} maxlength="50" size="4" /></td>

            <td><input name="bio_others4"                          
           placeholder="bio_others4" type="text" 						onChange={(e)=>this.setState({bio_others4:e.target.value})}
Value={this.state.bio_others4} maxlength="50" size="4" /></td>

    </tr>
  


    <tr>
      <td><input name="biochemical_date5"                          
           placeholder="biochemical_date5" type="text" 	onChange={(e)=>this.setState({biochemical_date5:e.target.value})}
Value={this.state.biochemical_date5} maxlength="50" size="4" /></td>

      <td><input name="bio_lft5"                          
           placeholder="bio_lft5" type="text" 	onChange={(e)=>this.setState({bio_lft5:e.target.value})}
Value={this.state.bio_lft5} maxlength="50" size="4" /></td>
     
     <td><input name="bio_rft5"                          
           placeholder="bio_rft5" type="text" 						onChange={(e)=>this.setState({bio_rft5:e.target.value})}
Value={this.state.bio_rft5} maxlength="50" size="4" /></td>


<td><input name="bio_crp5"                          
           placeholder="bio_crp5" type="text" 	onChange={(e)=>this.setState({bio_crp5:e.target.value})}
Value={this.state.bio_crp5} maxlength="50" size="4" /></td>

      <td><input name="bio_bsl5"                          
           placeholder="bio_bsl5" type="text" 	onChange={(e)=>this.setState({bio_bsl5:e.target.value})}
Value={this.state.bio_bsl5} maxlength="50" size="4" /></td>


            <td><input name="bio_ua5"                          
           placeholder="bio_ua5" type="text" 						onChange={(e)=>this.setState({bio_ua5:e.target.value})}
Value={this.state.bio_ua5} maxlength="50" size="4" /></td>

            <td><input name="bio_others5"                          
           placeholder="bio_others5" type="text" 						onChange={(e)=>this.setState({bio_others5:e.target.value})}
Value={this.state.bio_others5} maxlength="50" size="4" /></td>

    </tr>
  </tbody>
</Table>
</Row>



<Row>
<h5>Immonological date: Information</h5> 

<Table striped bordered hover>
  <thead>
    <tr>
    <th>DATE</th>
      <th>RF</th>
      <th>ACCP</th>
      <th>ANA</th>
      <th>ANA-WB</th>
      <th>ACE</th>
      <th>DsDNA</th>
      <th>HLA 827</th>
      <th>C3 </th>
      <th>C3</th>
      <th>OTHERS</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input name="immunological_date"                          
           placeholder="immunological_date" type="text" 	onChange={(e)=>this.setState({immunological_date:e.target.value})}
Value={this.state.immunological_date} maxlength="50" size="2" /></td>

      <td><input name="imm_rf"                          
           placeholder="imm_rf" type="text" 	onChange={(e)=>this.setState({imm_rf:e.target.value})}
Value={this.state.imm_rf} maxlength="50" size="2" /></td>
     
     <td><input name="imm_accp"                          
           placeholder="imm_accp" type="text" 						onChange={(e)=>this.setState({imm_accp:e.target.value})}
Value={this.state.imm_accp} maxlength="50" size="2" /></td>


<td><input name="imm_ana"                          
           placeholder="imm_ana" type="text" 	onChange={(e)=>this.setState({imm_ana:e.target.value})}
Value={this.state.imm_ana} maxlength="50" size="2" /></td>

      <td><input name="imm_anawb"                          
           placeholder="imm_anawb" type="text" 	onChange={(e)=>this.setState({imm_anawb:e.target.value})}
Value={this.state.imm_anawb} maxlength="50" size="2" /></td>


            <td><input name="imm_ace"                          
           placeholder="imm_ace" type="text" 						onChange={(e)=>this.setState({imm_ace:e.target.value})}
Value={this.state.imm_ace} maxlength="50" size="2" /></td>

            <td><input name="imm_dsdna"                          
           placeholder="imm_dsdna" type="text" 						onChange={(e)=>this.setState({imm_dsdna:e.target.value})}
Value={this.state.imm_dsdna} maxlength="50" size="2" /></td>


<td><input name="imm_hlab27"                          
           placeholder="imm_hlab27" type="text" 	onChange={(e)=>this.setState({imm_hlab27:e.target.value})}
Value={this.state.imm_hlab27} maxlength="50" size="2" /></td>


            <td><input name="imm_c3"                          
           placeholder="imm_c3" type="text" 						onChange={(e)=>this.setState({imm_c3:e.target.value})}
Value={this.state.imm_c3} maxlength="50" size="2" /></td>

            <td><input name="imm_c3"                          
           placeholder="imm_c3" type="text" 						onChange={(e)=>this.setState({imm_c3:e.target.value})}
Value={this.state.imm_c3} maxlength="50" size="2" /></td>



<td><input name="imm_others"                          
           placeholder="imm_others" type="text" 						onChange={(e)=>this.setState({imm_others:e.target.value})}
Value={this.state.imm_others} maxlength="50" size="2" /></td>

    </tr>



    <tr>
      <td><input name="immunological_date2"                          
           placeholder="immunological_date2" type="text" 	onChange={(e)=>this.setState({immunological_date2:e.target.value})}
Value={this.state.immunological_date2} maxlength="50" size="2" /></td>

      <td><input name="imm_rf2"                          
           placeholder="imm_rf2" type="text" 	onChange={(e)=>this.setState({imm_rf2:e.target.value})}
Value={this.state.imm_rf2} maxlength="50" size="2" /></td>
     
     <td><input name="imm_accp2"                          
           placeholder="imm_accp2" type="text" 						onChange={(e)=>this.setState({imm_accp2:e.target.value})}
Value={this.state.imm_accp2} maxlength="50" size="2" /></td>


<td><input name="imm_ana2"                          
           placeholder="imm_ana2" type="text" 	onChange={(e)=>this.setState({imm_ana2:e.target.value})}
Value={this.state.imm_ana2} maxlength="50" size="2" /></td>

      <td><input name="imm_anawb2"                          
           placeholder="imm_anawb2" type="text" 	onChange={(e)=>this.setState({imm_anawb2:e.target.value})}
Value={this.state.imm_anawb2} maxlength="50" size="2" /></td>


            <td><input name="imm_ace2"                          
           placeholder="imm_ace2" type="text" 						onChange={(e)=>this.setState({imm_ace2:e.target.value})}
Value={this.state.imm_ace2} maxlength="50" size="2" /></td>

            <td><input name="imm_dsdna2"                          
           placeholder="imm_dsdna2" type="text" 						onChange={(e)=>this.setState({imm_dsdna2:e.target.value})}
Value={this.state.imm_dsdna2} maxlength="50" size="2" /></td>


<td><input name="imm_hlab272"                          
           placeholder="imm_hlab272" type="text" 	onChange={(e)=>this.setState({imm_hlab272:e.target.value})}
Value={this.state.imm_hlab272} maxlength="50" size="2" /></td>


            <td><input name="imm_c32"                          
           placeholder="imm_c32" type="text" 						onChange={(e)=>this.setState({imm_c32:e.target.value})}
Value={this.state.imm_c32} maxlength="50" size="2" /></td>

            <td><input name="imm_c32"                          
           placeholder="imm_c32" type="text" 						onChange={(e)=>this.setState({imm_c32:e.target.value})}
Value={this.state.imm_c32} maxlength="50" size="2" /></td>



<td><input name="imm_others2"                          
           placeholder="imm_others2" type="text" 						onChange={(e)=>this.setState({imm_others2:e.target.value})}
Value={this.state.imm_others2} maxlength="50" size="2" /></td>

    </tr>


    <tr>
      <td><input name="immunological_date3"                          
           placeholder="immunological_date3" type="text" 	onChange={(e)=>this.setState({immunological_date3:e.target.value})}
Value={this.state.immunological_date3} maxlength="50" size="2" /></td>

      <td><input name="imm_rf3"                          
           placeholder="imm_rf3" type="text" 	onChange={(e)=>this.setState({imm_rf3:e.target.value})}
Value={this.state.imm_rf3} maxlength="50" size="2" /></td>
     
     <td><input name="imm_accp3"                          
           placeholder="imm_accp3" type="text" 						onChange={(e)=>this.setState({imm_accp3:e.target.value})}
Value={this.state.imm_accp3} maxlength="50" size="2" /></td>


<td><input name="imm_ana3"                          
           placeholder="imm_ana3" type="text" 	onChange={(e)=>this.setState({imm_ana3:e.target.value})}
Value={this.state.imm_ana3} maxlength="50" size="2" /></td>

      <td><input name="imm_anawb3"                          
           placeholder="imm_anawb3" type="text" 	onChange={(e)=>this.setState({imm_anawb3:e.target.value})}
Value={this.state.imm_anawb3} maxlength="50" size="2" /></td>


            <td><input name="imm_ace3"                          
           placeholder="imm_ace3" type="text" 						onChange={(e)=>this.setState({imm_ace3:e.target.value})}
Value={this.state.imm_ace3} maxlength="50" size="2" /></td>

            <td><input name="imm_dsdna3"                          
           placeholder="imm_dsdna3" type="text" 						onChange={(e)=>this.setState({imm_dsdna3:e.target.value})}
Value={this.state.imm_dsdna3} maxlength="50" size="2" /></td>


<td><input name="imm_hlab373"                          
           placeholder="imm_hlab373" type="text" 	onChange={(e)=>this.setState({imm_hlab373:e.target.value})}
Value={this.state.imm_hlab373} maxlength="50" size="2" /></td>


            <td><input name="imm_c33"                          
           placeholder="imm_c33" type="text" 						onChange={(e)=>this.setState({imm_c33:e.target.value})}
Value={this.state.imm_c33} maxlength="50" size="2" /></td>

            <td><input name="imm_c33"                          
           placeholder="imm_c33" type="text" 						onChange={(e)=>this.setState({imm_c33:e.target.value})}
Value={this.state.imm_c33} maxlength="50" size="2" /></td>



<td><input name="imm_others3"                          
           placeholder="imm_others3" type="text" 						onChange={(e)=>this.setState({imm_others3:e.target.value})}
Value={this.state.imm_others3} maxlength="50" size="2" /></td>

    </tr>


  </tbody>

  <h5>Patient Radiological  Information</h5>

</Table>

</Row> 

<Row>
    
<Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>USG</Form.Label><br></br>
          <Form.Control name="usg"                          
           placeholder="usg" type="text" onChange={(e)=>this.setState({usg:e.target.value})}
						Value={this.state.usg}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>X Ray Chest</Form.Label><br></br>
          <Form.Control name="xray_chest"                          
           placeholder="xray_chest" type="text"  onChange={(e)=>this.setState({xray_chest:e.target.value})} Value={this.state.xray_chest}  />
          </Form.Group>
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>X Ray Joints</Form.Label><br></br>
          <Form.Control name="xray_joints"                          
           placeholder="xray_joints" type="text"  onChange={(e)=>this.setState({xray_joints:e.target.value})}  Value={this.state.xray_joints}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>MRI</Form.Label><br></br>
          <Form.Control name="mri"                          
           placeholder="mri" type="text"  onChange={(e)=>this.setState({mri:e.target.value})} Value={this.state.mri}/>
          </Form.Group>
          </Col>
          </Row><br></br>


       
          <Col md="6">
          <Form.Group>
          <Form.Label>Radiological Remark</Form.Label><br></br>
          <Form.Control name="radio_remarks"                          
           placeholder="radio_remarks" type="text"  onChange={(e)=>this.setState({radio_remarks:e.target.value})} Value={this.state.radio_remarks}/>
          </Form.Group>

         
</Col>



<br></br>




<h5>2) Disease activity Indices</h5>
<Table striped bordered hover>
  <thead>
    <tr>
      <th>Disease activity Indices</th>
      <th>yes/no</th>
     
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>DAS 28</td>
      <td><input name="das28"                          
           placeholder="das28" type="text" 	onChange={(e)=>this.setState({das28:e.target.value})}
Value={this.state.das28} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>SLEDAI</td>
      <td><input name="sledai"                          
           placeholder="sledai" type="text" 	onChange={(e)=>this.setState({sledai:e.target.value})}
Value={this.state.sledai} maxlength="50" size="8" /></td>
     

    </tr>


    <tr>
      <td>BASDAI</td>
      <td><input name="basdai"                          
           placeholder="basdai" type="text" 	onChange={(e)=>this.setState({basdai:e.target.value})}
Value={this.state.basdai} maxlength="50" size="8" /></td>
     

    </tr>

    <tr>
      <td>PASI</td>
      <td><input name="pasi"                          
           placeholder="pasi" type="text" 	onChange={(e)=>this.setState({pasi:e.target.value})}
Value={this.state.pasi} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>BVAS</td>
      <td><input name="bvas"                          
           placeholder="bvas" type="text" 	onChange={(e)=>this.setState({bvas:e.target.value})}
Value={this.state.bvas} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>MRSS</td>
      <td><input name="mrss"                          
           placeholder="mrss" type="text" 	onChange={(e)=>this.setState({mrss:e.target.value})}
Value={this.state.mrss} maxlength="50" size="8" /></td>
     

    </tr>




    <tr>
      <td>SDAI  / CDAI</td>
      <td><input name="sdai_cdai"                          
           placeholder="sdai_cdai" type="text" 	onChange={(e)=>this.setState({sdai_cdai:e.target.value})}
Value={this.state.sdai_cdai} maxlength="50" size="8" /></td>
     

    </tr>
  


    <tr>
      <td>ASDAS</td>
      <td><input name="asdas"                          
           placeholder="asdas" type="text" 	onChange={(e)=>this.setState({asdas:e.target.value})}
Value={this.state.asdas} maxlength="50" size="8" /></td>
     

    </tr>

    <tr>
      <td>DAPSA</td>
      <td><input name="dapsa"                          
           placeholder="dapsa " type="text" 	onChange={(e)=>this.setState({dapsa:e.target.value})}
Value={this.state.dapsa} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>VDI</td>
      <td><input name="vdi"                          
           placeholder="vdi" type="text" 	onChange={(e)=>this.setState({vdi:e.target.value})}
Value={this.state.vdi} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>ESSDAI</td>
      <td><input name="essdai"                          
           placeholder="essdai id" type="text" 	onChange={(e)=>this.setState({essdai:e.target.value})}
Value={this.state.essdai} maxlength="50" size="8" /></td>
     

    </tr>



  </tbody>
</Table>
</Row>
          

<Table striped bordered hover>
  <thead>
    <tr>
      <th>are you able to </th>
      <th></th>
      <th></th>
      <th></th>
      
      <th>Score</th>
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>Dressing</td>
      <td>
          <label>Dress yourself</label><br></br>
          <input name="can_u_dress_urself" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_dress_urself:e.target.value})}
Value={this.state.can_u_dress_urself} maxlength="50" size="5" /></td>
     
     <td>          <label>Wash Your Hair</label><br></br>

         <input name="can_u_wash_ur_hair" type="text" placeholder="yes/no"						onChange={(e)=>this.setState({can_u_wash_ur_hair:e.target.value})}
Value={this.state.can_u_wash_ur_hair} maxlength="50" size="5" /></td>

   
            <td>          <label>Comb your Hair</label><br></br>

                
                <input name="can_u_comb_ur_hair" type="text" 		placeholder="yes/no"				onChange={(e)=>this.setState({can_u_comb_ur_hair:e.target.value})}
Value={this.state.can_u_comb_ur_hair} maxlength="50" size="5" /></td>

            <td><input name="dressing_score" type="number" 						onChange={(e)=>this.setState({dressing_score:e.target.value})}
Value={this.state.dressing_score} maxlength="50" size="5" /></td>

    </tr>


    <tr>
      <td>Arising</td>
      <td>      <label>Stand From Chair</label><br></br>
          <input name="can_u_stand_from_chair" type="text" placeholder="yes/no" 	onChange={(e)=>this.setState({can_u_stand_from_chair:e.target.value})}
Value={this.state.can_u_stand_from_chair} maxlength="50" size="5" /></td>
     
     <td><label>Get in out From Bed</label><br></br>
         <input name="can_u_get_inout_from_bed" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_get_inout_from_bed:e.target.value})}
Value={this.state.can_u_get_inout_from_bed} maxlength="50" size="5" /></td>

     
            <td><label>Sit Gross teg on Floor</label><br></br>
                <input name="can_u_sit_grossteg_onfloor" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_sit_grossteg_onfloor:e.target.value})}
Value={this.state.can_u_sit_grossteg_onfloor} maxlength="50" size="5" /></td>

            <td><input name="arising_score" type="number" 						onChange={(e)=>this.setState({arising_score:e.target.value})}
Value={this.state.arising_score} maxlength="50" size="5" /></td>

    </tr>


    <tr>
      <td>Eating</td>
      <td><label>Cut Vegetables</label><br></br>
          <input name="can_u_cut_vegetables" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_cut_vegetables:e.target.value})}
Value={this.state.can_u_cut_vegetables} maxlength="50" size="5" /></td>
     
     <td><label>Lift a glass to your mouth</label><br></br>
         <input name="can_u_lift_glass" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_lift_glass:e.target.value})}
Value={this.state.can_u_lift_glass} maxlength="50" size="5" /></td>

    
            <td><label>Break Roti from 1 hand</label><br></br>
                <input name="can_u_break_roti_from_1hand" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_break_roti_from_1hand:e.target.value})}
Value={this.state.can_u_break_roti_from_1hand} maxlength="50" size="5" /></td>

            <td>
                <input name="eating_score" type="number" 						onChange={(e)=>this.setState({eating_score:e.target.value})}
Value={this.state.eating_score} maxlength="50" size="5" /></td>

    </tr>

    <tr>
      <td>Walking</td>
      <td><label>Walking Outdoors</label><br></br>
          <input name="can_u_walk" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_walk:e.target.value})}
Value={this.state.can_u_walk} maxlength="50" size="5" /></td>
     
     <td><label>Climb up 5 Steps</label><br></br>
         <input name="can_u_climb_5steps" type="text" placeholder="yes/no"						onChange={(e)=>this.setState({can_u_climb_5steps:e.target.value})}
Value={this.state.can_u_climb_5steps} maxlength="50" size="5" /></td>

   
            <td></td>


<td>
    <input name="walking_score" type="number" 				onChange={(e)=>this.setState({walking_score:e.target.value})}
Value={this.state.walking_score} maxlength="50" size="5" /></td>

    </tr>
   


    <tr>
      <td>Hygiene</td>
      <td><label>Take a Bath</label><br></br>
          <input name="can_u_take_bath" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_take_bath:e.target.value})}
Value={this.state.can_u_take_bath} maxlength="50" size="5" /></td>
     
     <td><label>Was and dry your body </label><br></br>
         <input name="can_u_wash_dry_urbody" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_wash_dry_urbody:e.target.value})}
Value={this.state.can_u_wash_dry_urbody} maxlength="50" size="5" /></td>

    
            <td><label>Get on and off the toilet</label><br></br>
                <input name="can_u_get_onoff_toilet" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_get_onoff_toilet:e.target.value})}
Value={this.state.can_u_get_onoff_toilet} maxlength="50" size="5" /></td>

            <td>
                <input name="hygiene_score" type="number" 						onChange={(e)=>this.setState({hygiene_score:e.target.value})}
Value={this.state.hygiene_score} maxlength="50" size="5" /></td>

    </tr>
   


    <tr>
      <td>Reaching</td>
      <td><label>Reach and get down a 2 kg objects</label><br></br>
          <input name="can_u_weigh_2kg" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_weigh_2kg:e.target.value})}
Value={this.state.can_u_weigh_2kg} maxlength="50" size="5" /></td>
     
     <td><label>Bend down to pick up clothing from floor </label><br></br>
         <input name="can_u_bend_and_pickcloths" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_bend_and_pickcloths:e.target.value})}
Value={this.state.can_u_bend_and_pickcloths} maxlength="50" size="5" /></td>

    
            <td></td>

            <td>
                <input name="reaching_score" type="number" 						onChange={(e)=>this.setState({reaching_score:e.target.value})}
Value={this.state.reaching_score} maxlength="50" size="5" /></td>

    </tr>



    <tr>
      <td>Grip</td>
      <td><label>Open a bottle previously opened</label><br></br>
          <input name="can_u_open_bottle" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_open_bottle:e.target.value})}
Value={this.state.can_u_open_bottle} maxlength="50" size="5" /></td>
     
     <td><label>Turn taps on and off </label><br></br>
         <input name="can_u_turntaps_onoff" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_turntaps_onoff:e.target.value})}
Value={this.state.can_u_turntaps_onoff} maxlength="50" size="5" /></td>

    
            <td><label>Open done Latches</label><br></br>
                <input name="can_u_open_latches" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_open_latches:e.target.value})}
Value={this.state.can_u_open_latches} maxlength="50" size="5" /></td>

            <td>
                <input name="grip_score" type="number" 						onChange={(e)=>this.setState({grip_score:e.target.value})}
Value={this.state.grip_score} maxlength="50" size="5" /></td>

    </tr>



    <tr>
      <td>Activities</td>
      <td><label>Work in office/house</label><br></br>
          <input name="can_u_work_office_house" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_work_office_house:e.target.value})}
Value={this.state.can_u_work_office_house} maxlength="50" size="5" /></td>
     
     <td><label>Run errands and shop</label><br></br>
         <input name="can_u_run_errands" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_run_errands:e.target.value})}
Value={this.state.can_u_run_errands} maxlength="50" size="5" /></td>

    
            <td><label>Get in and out of bus</label><br></br>
                <input name="can_u_get_inout_of_bus" type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_get_inout_of_bus:e.target.value})}
Value={this.state.can_u_get_inout_of_bus} maxlength="50" size="5" /></td>

            <td>
                <input name="activities_score" type="number" 						onChange={(e)=>this.setState({activities_score:e.target.value})}
Value={this.state.activities_score} maxlength="50" size="5" /></td>

    </tr>
  </tbody>
</Table><br></br>



<h4>Modified CRD Pune Version HAQ Score</h4>

<Table striped bordered hover>
  <thead>
    <tr>
      <th>Modified CRD Pune Version HAQ Score</th>
      <th>/24</th>
      
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>Patient assessment pain(VSA)</td>
      <td><input name="patient_assessment_pain" type="number" 	onChange={(e)=>this.setState({patient_assessment_pain:e.target.value})}
Value={this.state.patient_assessment_pain} maxlength="50" size="10" />cm(on a scale of 10cm)</td>
     

    </tr>
    <tr>
      <td>Grip strength</td>
      <td><input name="grip_strength_rt" type="number" 	onChange={(e)=>this.setState({grip_strength_rt:e.target.value})}
Value={this.state.grip_strength_rt} maxlength="50" size="10" />MM HG of RT  <br></br><br></br>


<input name="grip_strength_hg" type="number" 	onChange={(e)=>this.setState({grip_strength_hg:e.target.value})}
Value={this.state.grip_strength_hg} maxlength="50" size="10" />HG  <br></br><br></br>


 <input name="grip_strength_lt" type="number" 	onChange={(e)=>this.setState({grip_strength_lt:e.target.value})}
Value={this.state.grip_strength_lt} maxlength="50" size="10" />MM HG of LT
</td>


 
    </tr>


    <tr>
      <td>Early morning stiffness</td>
      <td><input name="early_mrng_stiffness" type="number" 	onChange={(e)=>this.setState({early_mrng_stiffness:e.target.value})}
Value={this.state.early_mrng_stiffness} maxlength="50" size="10" />Mins</td>

 
    </tr>

    <tr>
      <td>Sleep</td>
      <td><input name="assess_sleep" type="text" 	onChange={(e)=>this.setState({assess_sleep:e.target.value})}
Value={this.state.assess_sleep} maxlength="50" size="20"/> (normal/distributed/excess)</td>


    </tr>
   
    <tr>
      <td>General health assessment</td>
      <td><input name="general_health_assessment" type="number" 	onChange={(e)=>this.setState({general_health_assessment:e.target.value})}
Value={this.state.general_health_assessment} maxlength="50" size="10" />mm (on a 100 cm scale)</td>


    </tr>
   
  </tbody>
</Table>
<br></br>



<h4>Summary</h4>
<p>please Confirm all detials</p>

<Table striped bordered hover>
  <thead>
    <tr>
      <th>Summary</th>
      <th></th>
      
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>classification_criteria_followig</td>
      <td><input name="classification_criteria_followig" type="text" placeholder="yes/no"	onChange={(e)=>this.setState({classification_criteria_followig:e.target.value})}
Value={this.state.classification_criteria_followig} maxlength="50" size="10" /></td>
     

    </tr>
 
    <tr>
      <td>Diagnosis</td>
      <td><input name="diagnosis" type="text" 	onChange={(e)=>this.setState({diagnosis:e.target.value})}
Value={this.state.diagnosis} maxlength="50" size="20"/> </td>


    </tr>
   
    <tr>
      <td>Treatment Plan</td>
      <td><textarea name="treatment_plan" type="text" 	onChange={(e)=>this.setState({treatment_plan:e.target.value})}
Value={this.state.treatment_plan} maxlength="50" size="10" /></td>


    </tr>
   
  </tbody>
</Table>


<Button
                    className="btn-fill pull-right"
              type="submit"
                    name=""
                    variant="info"
                  >
                    Add Examination
                  </Button>

        



                       </Form>
                       </Card.Body>
                       </Card>
                       </Col>
                       </Row>
                       
      </Container>
    </>
  );
}
}

export default Investigation_Main

// export default User;
// import React from "react";
// import axios from 'axios'
// // react-bootstrap components
// import {
//   Badge,
//   Button,
//   Card,
//   Form,
//   Navbar,
//   Nav,
//   Container,
//   Row,
//   Col,
// } from "react-bootstrap";


// class User extends React.Component {
//   constructor(props){
//     super(props);
//     this.state ={
//       users:[],
//       id:0,
//       pid:''
//       ,date:''
//       ,code:''
//       ,place:''
//       ,pname:''
//       ,fhname:''
//       ,age:''
//       ,sex:''
//       ,education:''
//       ,address:''
//       ,phone:''
//       ,district:''
//       ,state:''
//       ,area:''
//       ,referred_by:''
//       ,occupation:''
//       ,ethnic:''
//       ,marital_status:''
//       ,marital_status_years:''
//       ,menses_frequency:''
//       ,menses_loss:''
//       ,menarche_years:''
//       ,hysterectomy:''
//       ,hysterectomy_years:''
//       ,menopause:''
//       ,menopause_years:''
//       ,children:''
//       ,children_male:''
//       ,children_female:''
//       ,abortions:''
//       ,abortions_number:''
//       ,abortions_cause:''
//       ,current_lactation:''
//       ,contraception_methods:''
//       ,contraception_methods_type:''
//       ,hormone_treatment:''
//       ,addiction:''
//       ,tobacco:''
//       ,tobacco_years:''
//       ,smoking:''
//       ,smoking_years:''
//       ,alcohol:''
//       ,alcohol_years:''
//       ,family_history:''
//       ,health_condition:''
//       ,remarks:''
//       ,comorbidities:''
//       ,onset:''
//       ,onset_duration:''
//       ,onset_duration_type:''
//       ,first_symptom:''
//       ,initial_site_joint:''
//       ,soft_tissue:''
//       ,others:''
//       ,course1:''
//       ,course2:''
//       ,pattern1:''
//       ,pattern2:''
//       ,current_relapse:''
//       ,current_relapse_type:''
//       ,detailed_history:''
//       ,pasthistory:''
//       ,surgical_history:''
//       ,drug_history:''
//       ,drug_allergy:''
//       ,drug_allergy_type:''
//       ,bowelhabit:''
//       ,sleep:''
//       ,user_id:''

//     }

//   }


//   componentDidMount(){
//     axios.get(`http://127.0.0.1:5000/bpatientdetails/${this.props.match.params.id}`)
//     .then((res)=>{
//       console.log(res)
//       this.setState({
//         id:res.data.bpatient.id,

//         pid:res.data.bpatient.pid,
//         date:res.data.bpatient.date,
//         code:res.data.bpatient.code,
//         place:res.data.bpatient.place,
//         pname:res.data.bpatient.pname,
//         fhname:res.data.bpatient.fhname,
//         age:res.data.bpatient.age,
//         sex:res.data.bpatient.sex,
//         education:res.data.bpatient.education,
//         address:res.data.bpatient.address,
//         phone:res.data.bpatient.phone,
//         district:res.data.bpatient.district,
//         state:res.data.bpatient.state,
//         area:res.data.bpatient.area,
//         referred_by:res.data.bpatient.referred_by,
//         occupation:res.data.bpatient.occupation,
//         ethnic:res.data.bpatient.ethnic,
//         marital_status:res.data.bpatient.marital_status,
//         marital_status_years :res.data.bpatient.marital_status_years,
//         menses_frequency:res.data.bpatient.menses_frequency,
//         menses_loss:res.data.bpatient.menses_loss,
//         menarche_years :res.data.bpatient.menarche_years,
//         hystrectomy :res.data.bpatient.hystrectomy,
//         hystrectomy_years:res.data.bpatient.hystrectomy_years,
//         menopause:res.data.bpatient.menopause,
//         menopause_years:res.data.bpatient.menarche_years,
//         children:res.data.bpatient.children,
//         children_male:res.data.bpatient.children_male,
//         children_female:res.data.bpatient.children_female,
//         abortions:res.data.bpatient.abortions,
//         abortions_number:res.data.bpatient.abortions_number,
//         abortions_cause:res.data.bpatient.abortions_cause,
//         breastfed:res.data.bpatient.breastfed,
//         breastfed_years:res.data.bpatient.breastfed_years,
//         current_lactation:res.data.bpatient.current_lactation,
//         contraception_methods:res.data.bpatient.contraception_methods,
//         contraception_methods_type:res.data.bpatient.contraception_methods_type,
//         hormone_treatment:res.data.bpatient.hormone_treatment,
//         addiction:res.data.bpatient.addiction,
//         tobacco:res.data.bpatient.tobacco,
//         tobacco_years:res.data.bpatient.tobacco_years,
//         smoking:res.data.bpatient.smoking,
//         smoking_years:res.data.bpatient.smoking_years,
//         alcohol:res.data.bpatient.alcohol,
//         alcohol_years:res.data.bpatient.alcohol_years,
//         family_history:res.data.bpatient.family_history,
//         comorbidities:res.data.bpatient.comorbidities,
//         breastlump:res.data.bpatient.breastlump,
//         breastlump_location:res.data.bpatient.breastlump_location,
//         breastlump_size:res.data.bpatient.breastlump_size,
//         overlying_skin:res.data.bpatient.overlying_skin,
//         axillarylump:res.data.bpatient.axillarylump,
//         axillarylump_side:res.data.bpatient.axillarylump_side,
//         matted:res.data.bpatient.matted,
//         axillarylump_size:res.data.bpatient.axillarylump_size,
//         nipple_discharge:res.data.bpatient.nipple_discharge,
//         nipple_discharge_frequency:res.data.bpatient.nipple_discharge_frequency,
//         nipple_discharge_colour:res.data.bpatient.nipple_discharge_colour,
//         mastalgia:res.data.bpatient.mastalgia,
//         mastitis:res.data.bpatient.mastitis,
//         ulcer:res.data.bpatient.ulcer,
//         nipple_inversion:res.data.bpatient.nipple_inversion,
//         others:res.data.bpatient.others,
//         duration:res.data.bpatient.duration,
//         pasthistory:res.data.bpatient.pasthistory,
//         surgical_history:res.data.bpatient.surgical_history,
//         drug_history:res.data.bpatient.drug_history,
//         drug_allergy:res.data.bpatient.drug_allergy,
//         drug_allergy_type:res.data.bpatient.drug_allergy_type,
//         bowelhabit:res.data.bpatient.bowelhabit,
//         bladderhabit:res.data.bpatient.bladderhabit,
//         sleep:res.data.bpatient.sleep,
//         appetite:res.data.bpatient.appetite,
//         weight:res.data.bpatient.weight,
//         height:res.data.bpatient.height,
//         bmi:res.data.bpatient.bmi,
//         bp:res.data.bpatient.bp,
//         pulse:res.data.bpatient.pulse,
//         temp:res.data.bpatient.temp,
//         respiratory_rate:res.data.bpatient.respiratory_rate,
//         health_condition:res.data.bpatient.health_condition,
//         examination_remarks:res.data.bpatient.examination_remarks,
//         bra_size:res.data.bpatient.bra_size,
//         usg:res.data.bpatient.usg,
//         mmg:res.data.bpatient.mmg,
//         mri:res.data.bpatient.mri,
//         fnac:res.data.bpatient.fnac,
//         core_biopsy:res.data.bpatient.core_biopsy,
//         incision_biopsy:res.data.bpatient.incision_biopsy,
//         investigation_remarks:res.data.bpatient.investigation_remarks,
//         blood_investigation:res.data.bpatient.blood_investigation,
//         diagnosis:res.data.bpatient.diagnosis,
//         treatment_plan:res.data.bpatient.treatment_plan,
//         user_id:res.data.bpatient.user.id
      
        
//       })
//     })
//   }
//   submit(event,id){
//     event.preventDefault();
//     if(id === 0){
//       axios.post("http://127.0.0.1:5000/addbpatient",{
    
//       pid:this.state.pid
//       ,date:this.state.date
//       ,code:this.state.code
//       ,place:this.state.place
//       ,pname:this.state.pname
//       ,fhname:this.state.fhname
//       ,age:this.state.age
//       ,sex:this.state.sex
//       ,education:this.state.education
//       ,address:this.state.address
//       ,phone:this.state.phone
//       ,district:this.state.district
//       ,state:this.state.state
//       ,area:this.state.area
//       ,referred_by:this.state.referred_by
//       ,occupation:this.state.occupation
//       ,ethnic:this.state.ethnic
//       ,marital_status:this.state.marital_status
//       ,marital_status_years:this.state.marital_status_years
//       ,menses_frequency:this.state.menses_frequency
//       ,menses_loss:this.state.menses_loss
//       ,menarche_years:this.state.menarche_years
//       ,hysterectomy:this.state.hysterectomy
//       ,hysterectomy_years:this.state.hysterectomy_years
//       ,menopause:this.state.menopause
//       ,menopause_years:this.state.menopause_years
//       ,children:this.state.children
//       ,children_male:this.state.children_male
//       ,children_female:this.state.children_female
//       ,abortions:this.state.abortions
//       ,abortions_number:this.state.abortions_number
//       ,abortions_cause:this.state.abortions_cause
//       ,current_lactation:this.state.current_lactation
//       ,contraception_methods:this.state.contraception_methods
//       ,contraception_methods_type:this.state.contraception_methods_type
//       ,hormone_treatment:this.state.hormone_treatment
//       ,addiction:this.state.addiction
//       ,tobacco:this.state.tobacco
//       ,tobacco_years:this.state.tobacco_years
//       ,smoking:this.state.smoking
//       ,smoking_years:this.state.smoking_years
//       ,alcohol:this.state.alcohol
//       ,alcohol_years:this.state.alcohol_years
//       ,family_history:this.state.family_history
//       ,health_condition:this.state.health_condition
//       ,remarks:this.state.remarks
//       ,comorbidities:this.state.comorbidities
//       ,onset:this.state.onset
//       ,onset_duration:this.state.onset_duration
//       ,onset_duration_type:this.state.onset_duration_type
//       ,first_symptom:this.state.first_symptom
//       ,initial_site_joint:this.state.initial_site_joint
//       ,soft_tissue:this.state.soft_tissue
//       ,others:this.state.others
//       ,course1:this.state.course1
//       ,course2:this.state.course2
//       ,pattern1:this.state.pattern1
//       ,pattern2:this.state.pattern2
//       ,current_relapse:this.state.current_relapse
//       ,current_relapse_type:this.state.current_relapse_type
//       ,detailed_history:this.state.detailed_history
//       ,pasthistory:this.state.pasthistory
//       ,surgical_history:this.state.surgical_history
//       ,drug_history:this.state.drug_history
//       ,drug_allergy:this.state.drug_allergy
//       ,drug_allergy_type:this.state.drug_allergy_type
//       ,bowelhabit:this.state.bowelhabit
//       ,sleep:this.state.sleep
//       ,user_id:this.state.user_id
//           })
//       .then((res)=>{
//         this.componentDidMount();
//       })
//     }else{
//       axios.put(`http://127.0.0.1:5000/updatebpatient/${this.props.match.params.id}`,{
//         id:this.state.id,

      
//         pid:this.state.pid
//         ,date:this.state.date
//         ,code:this.state.code
//         ,place:this.state.place
//         ,pname:this.state.pname
//         ,fhname:this.state.fhname
//         ,age:this.state.age
//         ,sex:this.state.sex
//         ,education:this.state.education
//         ,address:this.state.address
//         ,phone:this.state.phone
//         ,district:this.state.district
//         ,state:this.state.state
//         ,area:this.state.area
//         ,referred_by:this.state.referred_by
//         ,occupation:this.state.occupation
//         ,ethnic:this.state.ethnic
//         ,marital_status:this.state.marital_status
//         ,marital_status_years:this.state.marital_status_years
//         ,menses_frequency:this.state.menses_frequency
//         ,menses_loss:this.state.menses_loss
//         ,menarche_years:this.state.menarche_years
//         ,hysterectomy:this.state.hysterectomy
//         ,hysterectomy_years:this.state.hysterectomy_years
//         ,menopause:this.state.menopause
//         ,menopause_years:this.state.menopause_years
//         ,children:this.state.children
//         ,children_male:this.state.children_male
//         ,children_female:this.state.children_female
//         ,abortions:this.state.abortions
//         ,abortions_number:this.state.abortions_number
//         ,abortions_cause:this.state.abortions_cause
//         ,current_lactation:this.state.current_lactation
//         ,contraception_methods:this.state.contraception_methods
//         ,contraception_methods_type:this.state.contraception_methods_type
//         ,hormone_treatment:this.state.hormone_treatment
//         ,addiction:this.state.addiction
//         ,tobacco:this.state.tobacco
//         ,tobacco_years:this.state.tobacco_years
//         ,smoking:this.state.smoking
//         ,smoking_years:this.state.smoking_years
//         ,alcohol:this.state.alcohol
//         ,alcohol_years:this.state.alcohol_years
//         ,family_history:this.state.family_history
//         ,health_condition:this.state.health_condition
//         ,remarks:this.state.remarks
//         ,comorbidities:this.state.comorbidities
//         ,onset:this.state.onset
//         ,onset_duration:this.state.onset_duration
//         ,onset_duration_type:this.state.onset_duration_type
//         ,first_symptom:this.state.first_symptom
//         ,initial_site_joint:this.state.initial_site_joint
//         ,soft_tissue:this.state.soft_tissue
//         ,others:this.state.others
//         ,course1:this.state.course1
//         ,course2:this.state.course2
//         ,pattern1:this.state.pattern1
//         ,pattern2:this.state.pattern2
//         ,current_relapse:this.state.current_relapse
//         ,current_relapse_type:this.state.current_relapse_type
//         ,detailed_history:this.state.detailed_history
//         ,pasthistory:this.state.pasthistory
//         ,surgical_history:this.state.surgical_history
//         ,drug_history:this.state.drug_history
//         ,drug_allergy:this.state.drug_allergy
//         ,drug_allergy_type:this.state.drug_allergy_type
//         ,bowelhabit:this.state.bowelhabit
//         ,sleep:this.state.sleep
//         ,user_id:this.state.user_id
//       }).then(()=>{
//         this.props.history.push(`/admin/dashboard`)
//       })

//     }

//   }
//   delete(id){
//     axios.delete(`http://127.0.0.1:5000/userdelete/${id}`)
//     .then(()=>{
//       this.componentDidMount();
//     })
//   }
//   // edit(id){
//   //   axios.get(`http://127.0.0.1:5000/userdetails/1`)
//   //   .then((res)=>{
//   //     console.log(res.data.user);
//   //     this.setState({
//   //       id:res.data.user.id,
//   //       username:res.data.user.username,
//   //       email:res.data.user.email,
//   //       phone:res.data.user.phone,
//   //       password:res.data.user.password,
//   //       role:res.data.user.role,
//   //       speciality:res.data.user.speciality,
//   //     })
//   //   })
//   // }

//   render()


// {
//   return (
//     <>
//       <Container fluid>
//         <Row>
//           <Col md="8">
//             <Card>
//               <Card.Header>
//                 <Card.Title as="h4">Edit Profile</Card.Title>
//               </Card.Header>
//               <Card.Body>
//                 <Form onSubmit={(e)=>this.submit(e,this.state.id)}>


//                 <Row>
//           <Col md="6">
         

//           <Form.Group>
//         <Form.Label>Patient Id</Form.Label><br></br>
//           <Form.Control
//            type="number"
           
//            name="pid"                          
//            placeholder="patient id"
//            value={this.state.pid}
//            onChange={(e)=>this.setState({pid:e.target.value})}></Form.Control>
//           </Form.Group>



//           <Form.Group>
//           <Form.Label>Date</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="date"
//           placeholder="Date"
//           value={this.state.date}
//           onChange={(e)=>this.setState({date:e.target.value})}  />
//           </Form.Group>
//         </Col>

        
//         <Col md="6">
//         <Form.Group>
//           <Form.Label>code</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="code"
//           placeholder="Code"
//           value={this.state.code}
//           onChange={(e)=>this.setState({code:e.target.value})}/>
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>place</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="place"
//           placeholder="place"
//           value={this.state.place}
//           onChange={(e)=>this.setState({place:e.target.value})}/>
//           </Form.Group>
//           </Col>
//           </Row>


//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>Patient Name</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="pname"
//           placeholder="pname"
//           value={this.state.pname}
//           onChange={(e)=>this.setState({pname:e.target.value})}/>
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Father or Husband name</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="fhname"
//           placeholder="fhname"
//           value={this.state.fhname}
//           onChange={(e)=>this.setState({fhname:e.target.value})}/>
//           </Form.Group>
// </Col>
// <Col md="6">

// <Form.Group>
//           <Form.Label>Age</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="age"  
//           placeholder="Age"
//           value={this.state.age}
//           onChange={(e)=>this.setState({age:e.target.value})}/>
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Gender</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="sex"
//           placeholder="Gender"
//           value={this.state.sex}
//           onChange={(e)=>this.setState({sex:e.target.value})}/>
//           </Form.Group>

// </Col>
// </Row>



// <Row>
//   <Col md="6">
//   <Form.Group>
//             <Form.Label>Education</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="education"
//           placeholder="Education"
//           value={this.state.education}
//           onChange={(e)=>this.setState({education:e.target.value})}
//           />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Address</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="address"
//           placeholder="Address"
//           value={this.state.address}
//           onChange={(e)=>this.setState({address:e.target.value})}
//           />
//           </Form.Group>

// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>Phone</Form.Label><br></br>
//           <Form.Control  
//           type="number"
//           name="phone"  
//           placeholder="Phone"
//           value={this.state.phone}
//           onChange={(e)=>this.setState({phone:e.target.value})}/>
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>District</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="district"
//           placeholder="District"
//           value={this.state.district}
//           onChange={(e)=>this.setState({district:e.target.value})}
//           />
//           </Form.Group>

// </Col>
// </Row> 

// <Row>
//   <Col md="6">
//   <Form.Group>
//           <Form.Label>State</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="state"
//           placeholder="State"
//           value={this.state.state}
//           onChange={(e)=>this.setState({state:e.target.value})}
//           />
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>Area</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="area"
//           placeholder="Area"
//           value={this.state.area}
//           onChange={(e)=>this.setState({area:e.target.value})}
//           />
//           </Form.Group>

//           </Col>
//           <Col md="6">
//           <Form.Group>

//           <Form.Label>Referred_by</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="referred_by"
//           placeholder="Referred by"
//           value={this.state.referred_by}
//           onChange={(e)=>this.setState({referred_by:e.target.value})}
//           />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Occupation</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="occupation"
//           placeholder="Occupation"
//           value={this.state.occupation}
//           onChange={(e)=>this.setState({occupation:e.target.value})}
//           />
//           </Form.Group>


// </Col>
// </Row>
          

//             <Row>
//   <Col md="6">
//   <Form.Group>
//           <Form.Label>Ethnic</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="ethnic"
//             placeholder="ethnic"
//           value={this.state.ethnic}
//           onChange={(e)=>this.setState({ethnic:e.target.value})}
          
//            />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Marital_status</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="marital_status"
//             placeholder="Marital status"
//           value={this.state.marital_status}
//           onChange={(e)=>this.setState({marital_status:e.target.value})}
          
//            />
//           </Form.Group>

//          </Col> 
      
//          <Col md="6">
//          <Form.Group>
//           <Form.Label>marital_status_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="marital_status_years"
//             placeholder="marital status years"
//           value={this.state.marital_status_years}
//           onChange={(e)=>this.setState({marital_status_years:e.target.value})}  
//          />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>menses_frequency</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="menses_frequency"
//             placeholder="menses frequency"
//           value={this.state.menses_frequency}
//           onChange={(e)=>this.setState({menses_frequency:e.target.value})}
//            />
//           </Form.Group>

//           </Col>
//           </Row>



//           <Row>

			  
//   <Col md="6">   
//   <Form.Group>
//           <Form.Label>menses_loss</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="menses_loss"
//             placeholder="menses loss"
//           value={this.state.menses_loss}
//           onChange={(e)=>this.setState({menses_loss:e.target.value})}
//     />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>menarche_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="menarche_years"
//             placeholder="menarche year"
//           value={this.state.menarche_years}
//           onChange={(e)=>this.setState({menarche_years:e.target.value})}  
//           />
//           </Form.Group>
//           </Col>
//           <Col md="6"> 
//           <Form.Group>  

//           <Form.Label>hystrectomy</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="hystrectomy"
//             placeholder="hystrectomy"
//           value={this.state.hystrectomy}
//           onChange={(e)=>this.setState({hystrectomy:e.target.value})}
//         />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>hystrectomy_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="hystrectomy_years"
//             placeholder="hystrectomy year"
//           value={this.state.hystrectomy_years}
//           onChange={(e)=>this.setState({hystrectomy_years:e.target.value})}  
//            />
//           </Form.Group>

// </Col>
// </Row>




// <Row>
//           <Col md="6">
       
//           <Form.Group>
//           <Form.Label>menopause</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="menopause"
//              placeholder="menopause"
//           value={this.state.menopause}
//           onChange={(e)=>this.setState({menopause:e.target.value})}  					
  
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>menopause_years</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="menopause_years"
//               placeholder="menopause year"
//           value={this.state.menopause_years}
//           onChange={(e)=>this.setState({menopause_years:e.target.value})}   						
// />
//            </Form.Group>
//           </Col>


//           <Col md="6">
//           <Form.Group>

//           <Form.Label>no of children</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="children"
//               placeholder="no of children"
//           value={this.state.children}
//           onChange={(e)=>this.setState({children:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>children_male</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="children_male"
//               placeholder="children_male"
//           value={this.state.children_male}
//           onChange={(e)=>this.setState({children_male:e.target.value})}   						
//      />
//            </Form.Group>

// </Col>
// </Row>

// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>children_female</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="children_female"
//               placeholder="children_female "
//           value={this.state.children_female}
//           onChange={(e)=>this.setState({children_female:e.target.value})}   						
//  />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>abortions</Form.Label><br></br>
//            <Form.Control 
//            type="text" 	
//            name="abortions"
//               placeholder="abortions"
//           value={this.state.abortions}
//           onChange={(e)=>this.setState({abortions:e.target.value})}  					
// />
//            </Form.Group>
// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>abortions_number</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="abortions_number"
//               placeholder="abortions_number"
//           value={this.state.abortions_number}
//           onChange={(e)=>this.setState({abortions_number:e.target.value})}   						
//     />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>abortions_cause</Form.Label><br></br>
//            <Form.Control 
//            type="text" 	
//            name="abortions_cause"
//               placeholder="abortions_cause"
//           value={this.state.abortions_cause}
//           onChange={(e)=>this.setState({abortions_cause:e.target.value})}  					
// />
//            </Form.Group>
//          </Col>
//          </Row>




//          <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>breastfed</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="breastfed"	
//                placeholder="breastfed"
//           value={this.state.breastfed}
//           onChange={(e)=>this.setState({breastfed:e.target.value})}  					
//  />
//           </Form.Group>

//           <Form.Group>
//            <Form.Label>breastfed_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="breastfed_years" 
//                placeholder="breastfed_years"
//           value={this.state.breastfed_years}
//           onChange={(e)=>this.setState({breastfed_years:e.target.value})}  							
//        />
//           </Form.Group>
// </Col>

// <Col md="6">

// <Form.Group>
//            <Form.Label>current_lactation</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="current_lactation"	
//                placeholder="current_lactation"
//           value={this.state.current_lactation}
//           onChange={(e)=>this.setState({current_lactation:e.target.value})}  					
//   />
//           </Form.Group>

//           <Form.Group>
//            <Form.Label>contraception_methods</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="contraception_methods"	
//                placeholder="contraception_methods"
//           value={this.state.contraception_methods}
//           onChange={(e)=>this.setState({contraception_methods:e.target.value})}  					
// />
//           </Form.Group>
//          </Col>
//          </Row>



//        <Row>
//           <Col md="6">
//           <Form.Group>
//          <Form.Label>contraception_methods_type</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="contraception_methods_type"	
//                placeholder="contraception_methods_type"
//           value={this.state.contraception_methods_type}
//           onChange={(e)=>this.setState({contraception_methods_type:e.target.value})}  					
// />
//           </Form.Group>
//           <Form.Group>
//            <Form.Label>hormone_treatment</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="hormone_treatment"	
//                placeholder="hormone_treatment"
//           value={this.state.hormone_treatment}
//           onChange={(e)=>this.setState({hormone_treatment:e.target.value})}  					
// />
//           </Form.Group>
//           </Col>

//           <Col md="6">
//           <Form.Group>
//            <Form.Label>addiction</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="addiction"	
//                placeholder="addiction"
//           value={this.state.addiction}
//           onChange={(e)=>this.setState({addiction:e.target.value})}  					
// />
//           </Form.Group>
//           <Form.Group>
//            <Form.Label>tobacco</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="tobacco"	
//                placeholder="tobacco"
//           value={this.state.tobacco}
//           onChange={(e)=>this.setState({tobacco:e.target.value})}  					
// />
//           </Form.Group>

//           </Col>
//           </Row>


//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>tobacco_years</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="tobacco_years" 
//              placeholder="tobacco_years"
//           value={this.state.tobacco_years}
//           onChange={(e)=>this.setState({tobacco_years:e.target.value})}  						
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>smoking</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="smoking"	
//              placeholder="smoking"
//           value={this.state.smoking}
//           onChange={(e)=>this.setState({smoking:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>smoking_years</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="smoking_years" 
//              placeholder="smoking_years"
//           value={this.state.smoking_years}
//           onChange={(e)=>this.setState({smoking_years:e.target.value})}  						
//  />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>alcohol</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="alcohol"	
//              placeholder="alcohol"
//           value={this.state.alcohol}
//           onChange={(e)=>this.setState({alcohol:e.target.value})}  				
//  />
//           </Form.Group>
//           </Col>

//           </Row>

//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>alcohol_years</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="alcohol_years" 
//              placeholder="alcohol_years"
//           value={this.state.alcohol_years}
//           onChange={(e)=>this.setState({alcohol_years:e.target.value})}  						
//    />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>family_history</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="family_history"	
//              placeholder="family_history"
//           value={this.state.family_history}
//           onChange={(e)=>this.setState({family_history:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>


//           <Col md="6">
//           <Form.Group>
//           <Form.Label>comorbidities</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="comorbidities"	
//              placeholder="comorbidities"
//           value={this.state.comorbidities}
//           onChange={(e)=>this.setState({comorbidities:e.target.value})}  				
// />
//           </Form.Group>

          
//           <Form.Group>
//           <Form.Label>breastlump</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="breastlump"	
//              placeholder="breastlump"
//           value={this.state.breastlump}
//           onChange={(e)=>this.setState({breastlump:e.target.value})}  				
// />




// </Form.Group>
//           </Col>
//           </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>breastlump_location</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="breastlump_location"        placeholder="breastlump_location"
//           value={this.state.breastlump_location}
//           onChange={(e)=>this.setState({breastlump_location:e.target.value})}   						
//      />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>breastlump_size</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="breastlump_size"        placeholder="breastlump_size"
//           value={this.state.breastlump_size}
//           onChange={(e)=>this.setState({breastlump_size:e.target.value})}   						
// />
//            </Form.Group>
// </Col>





// <Col md="6">
// <Form.Group>
//           <Form.Label>overlying_skin</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="overlying_skin"        placeholder="overlying_skin"
//           value={this.state.overlying_skin}
//           onChange={(e)=>this.setState({overlying_skin:e.target.value})}   						
//   />
//            </Form.Group>

//           <Form.Group>
//           <Form.Label>axillarylump</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="axillarylump"        placeholder="axillarylump"
//           value={this.state.axillarylump}
//           onChange={(e)=>this.setState({axillarylump:e.target.value})}   						
// />
//            </Form.Group>
// </Col>
// </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>

//           <Form.Label>axillarylump_side</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="axillarylump_side"        placeholder="axillarylump_side"
//           value={this.state.axillarylump_side}
//           onChange={(e)=>this.setState({axillarylump_side:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>matted</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="matted"        placeholder="matted"
//           value={this.state.matted}
//           onChange={(e)=>this.setState({matted:e.target.value})}   						
// />
//            </Form.Group>
// </Col>

// <Col md="6">
// <Form.Group>
//           <Form.Label>axillarylump_size</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="axillarylump_size"        placeholder="axillarylump_size"
//           value={this.state.axillarylump_size}
//           onChange={(e)=>this.setState({axillarylump_size:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>nipple_discharge</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="nipple_discharge"        placeholder="nipple_discharge"
//           value={this.state.nipple_discharge}
//           onChange={(e)=>this.setState({nipple_discharge:e.target.value})}   						
//  />
//            </Form.Group>

//           </Col>
//           </Row>
        
//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>nipple_discharge_frequency</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="nipple_discharge_frequency"        placeholder="nipple_discharge_frequency"
//           value={this.state.nipple_discharge_frequency}
//           onChange={(e)=>this.setState({nipple_discharge_frequency:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>nipple_discharge_colour</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="nipple_discharge_colour"        placeholder="nipple_discharge_colour"
//           value={this.state.nipple_discharge_colour}
//           onChange={(e)=>this.setState({nipple_discharge_colour:e.target.value})}   						
//     />
//            </Form.Group>
// </Col>

// <Col md="6">
// <Form.Group>
//           <Form.Label>mastalgia</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="mastalgia"        placeholder="mastalgia"
//           value={this.state.mastalgia}
//           onChange={(e)=>this.setState({mastalgia:e.target.value})}   						
// />
//            </Form.Group>

//           <Form.Group>
//           <Form.Label>mastitis</Form.Label><br></br>
//          <Form.Control 
//          type="text"
 
//  name="mastitis"        placeholder="mastitis"
//           value={this.state.mastitis}
//           onChange={(e)=>this.setState({mastitis:e.target.value})}   						
// />
//            </Form.Group>
//  </Col>
// </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>ulcer</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="ulcer"	
//            placeholder="ulcer"
//           value={this.state.ulcer}
//           onChange={(e)=>this.setState({ulcer:e.target.value})}  				
// />
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>nipple_inversion</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="nipple_inversion"	
//            placeholder="nipple_inversion"
//           value={this.state.nipple_inversion}
//           onChange={(e)=>this.setState({nipple_inversion:e.target.value})}  				
// />
//           </Form.Group>


// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>others</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="others"	
//            placeholder="others"
//           value={this.state.others}
//           onChange={(e)=>this.setState({others:e.target.value})}  				
// />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>duration</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="duration"	
//            placeholder="duration"
//           value={this.state.duration}
//           onChange={(e)=>this.setState({duration:e.target.value})}  				
//    />
//           </Form.Group>
// </Col>
// </Row>


// <Row>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>pasthistory</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="pasthistory"	
//            placeholder="pasthistory"
//           value={this.state.pasthistory}
//           onChange={(e)=>this.setState({pasthistory:e.target.value})}  				
// />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>surgical_history</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="surgical_history"	
//            placeholder="surgical_history"
//           value={this.state.surgical_history}
//           onChange={(e)=>this.setState({surgical_history:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>


// <Col md="6">
// <Form.Group>
//         <Form.Label>drug_history</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="drug_history"	
//            placeholder="drug_history"
//           value={this.state.drug_history}
//           onChange={(e)=>this.setState({drug_history:e.target.value})}  				
//  />
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>drug_allergy</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="drug_allergy"	
//            placeholder="drug_allergy"
//           value={this.state.drug_allergy}
//           onChange={(e)=>this.setState({drug_allergy:e.target.value})}  				
// />
//           </Form.Group>
// </Col>

// </Row>



// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>drug_allergy_type</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="drug_allergy_type"	
//           placeholder="drug_allergy_type"
//           value={this.state.drug_allergy_type}
//           onChange={(e)=>this.setState({drug_allergy_type:e.target.value})}  				
//     />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>bowelhabit</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="bowelhabit"	
//           placeholder="bowelhabit"
//           value={this.state.bowelhabit}
//           onChange={(e)=>this.setState({bowelhabit:e.target.value})}  				
// />
//           </Form.Group>

// </Col>

// <Col md="6">
// <Form.Group>
//           <Form.Label>bladderhabit</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="bladderhabit"	
//           placeholder="bladderhabit"
//           value={this.state.bladderhabit}
//           onChange={(e)=>this.setState({bladderhabit:e.target.value})}  				
//      />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>sleep</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="sleep"	
//           placeholder="sleep"
//           value={this.state.sleep}
//           onChange={(e)=>this.setState({sleep:e.target.value})}  				
// />
//           </Form.Group>

//    </Col>
//    </Row>

// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>appetite</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="appetite"	
//           placeholder="appetite"
//           value={this.state.appetite}
//           onChange={(e)=>this.setState({appetite:e.target.value})}  				
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>weight</Form.Label>
//           <Form.Control 
//           type="number"
//           name="weight" 
//           placeholder="weight"
//           value={this.state.weight}
//           onChange={(e)=>this.setState({weight:e.target.value})}  						
//   />
//           </Form.Group>
// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>height</Form.Label>
//           <Form.Control 
//           type="number"
//           name="height" 
//           placeholder="height"
//           value={this.state.height}
//           onChange={(e)=>this.setState({height:e.target.value})}  						
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>bmi</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="bmi"	
//           placeholder="bmi"
//           value={this.state.bmi}
//           onChange={(e)=>this.setState({bmi:e.target.value})}  				
// />
//           </Form.Group>
// </Col>
// </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>bp</Form.Label>
//          <Form.Control 
//          type="number"
//          name="bp" 
//          placeholder="bp"
//           value={this.state.bp}
//           onChange={(e)=>this.setState({bp:e.target.value})}  						
// />
//          </Form.Group>


//           <Form.Group>
//           <Form.Label>pulse</Form.Label>
//          <Form.Control 
//          type="number"
//          name="pulse" 
//          placeholder="pulse"
//           value={this.state.pulse}
//           onChange={(e)=>this.setState({pulse:e.target.value})}  						
// />
//          </Form.Group>
//        </Col>
//        <Col md="6">
//        <Form.Group>
//           <Form.Label>temp</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="temp"	
//          placeholder="temp"
//           value={this.state.temp}
//           onChange={(e)=>this.setState({temp:e.target.value})}  				
//   />
//          </Form.Group>
//           <Form.Group>
//           <Form.Label>respiratory_rate</Form.Label>
//          <Form.Control 
//          type="number"
//          name="respiratory_rate" 
//          placeholder="respiratory_rate"
//           value={this.state.respiratory_rate}
//           onChange={(e)=>this.setState({respiratory_rate:e.target.value})}  						
// />
//          </Form.Group>
          
//           </Col>

//           </Row>

//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>health_condition</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="health_condition"	
//          placeholder="health_condition"
//           value={this.state.health_condition}
//           onChange={(e)=>this.setState({health_condition:e.target.value})}  				
// />
//          </Form.Group>
//           <Form.Group>
//           <Form.Label>examination_remarks</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="examination_remarks"	
//          placeholder="examination_remarks"
//           value={this.state.examination_remarks}
//           onChange={(e)=>this.setState({examination_remarks:e.target.value})}  				
// />
//          </Form.Group>
//           </Col>
         
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>bra_size</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="bra_size"	
//          placeholder="bra_size"
//           value={this.state.bra_size}
//           onChange={(e)=>this.setState({bra_size:e.target.value})}  				
//  />
//          </Form.Group>
//           <Form.Group>
//           <Form.Label>usg</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="usg"	
//          placeholder="usg"
//           value={this.state.usg}
//           onChange={(e)=>this.setState({usg:e.target.value})}  				
// />
//          </Form.Group>
// </Col>
// </Row>




// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>mmg</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="mmg"	
//          placeholder="mmg"
//           value={this.state.mmg}
//           onChange={(e)=>this.setState({mmg:e.target.value})}  				
//  />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>mri</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="mri"	
//          placeholder="mri"
//           value={this.state.mri}
//           onChange={(e)=>this.setState({mri:e.target.value})}  				
//  />
//           </Form.Group>
//           </Col>
//           <Col md="6">

//           <Form.Group>
//           <Form.Label>fnac</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="fnac"	
//          placeholder="fnac"
//           value={this.state.fnac}
//           onChange={(e)=>this.setState({fnac:e.target.value})}  				
//   />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>core_biopsy</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="core_biopsy"	
//          placeholder="core_biopsy"
//           value={this.state.core_biopsy}
//           onChange={(e)=>this.setState({core_biopsy:e.target.value})}  				
// />
        
//           </Form.Group>
//         </Col>
//         </Row>

//         <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>incision_biopsy</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="incision_biopsy"	
//          placeholder="incision_biopsy"
//           value={this.state.incision_biopsy}
//           onChange={(e)=>this.setState({incision_biopsy:e.target.value})}  				
//  />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>investigation_remarks</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="investigation_remarks"	
//          placeholder="investigation_remarks"
//           value={this.state.investigation_remarks}
//           onChange={(e)=>this.setState({investigation_remarks:e.target.value})}  				
//  />
//           </Form.Group>
//           </Col>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>blood_investigation</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="blood_investigation"	
//          placeholder="blood_investigation"
//           value={this.state.blood_investigation}
//           onChange={(e)=>this.setState({blood_investigation:e.target.value})}  				
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>diagnosis</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="diagnosis"	
//          placeholder="diagnosis"
//           value={this.state.diagnosis}
//           onChange={(e)=>this.setState({diagnosis:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>
//           </Row>




//         <Row>
        
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>treatment_plan</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="treatment_plan"	
//          placeholder="treatment_plan"
//           value={this.state.treatment_plan}
//           onChange={(e)=>this.setState({treatment_plan:e.target.value})}  				
//   />
//           </Form.Group>

//           </Col>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>User Id</Form.Label>
//          <Form.Control 
//          type="number"
//          name="user_id" 
//          placeholder="User Id"
//           value={this.state.user_id}
//           onChange={(e)=>this.setState({user_id:e.target.value})}  	
//           disabled					
// />
//           </Form.Group>

// </Col>
// </Row>



// <Button
//                     className="btn-fill pull-right"
//                     type="submit"
//                     name=""
//                     variant="info"
//                   >
//                     Update User
//                   </Button>

        



//                        </Form>
//                        </Card.Body>
//                        </Card>
//                        </Col>
//                        </Row>
                       
//       </Container>
//     </>
//   );
// }
// }

// export default User;
