import React from "react";

import axios from "axios";
import { Link } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
// import Main from '../Patientform/Main'
// import Main1 from './Main1'

import step6 from '../Patientform/step6'
import UserForm  from '../UserForm'

// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  Dropdown,
} from "react-bootstrap";


class Main1 extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:[],
      id:0,
      username: "",
      email: "",  
      phone: "",
      password: "",
      role:"",
      speciality:"",
      showHide : false,
      open: false,
    


      

    }


  }
  // openModal (){
  //   this.setState({open: true})}


  closeModal (){
    this.setState({open: false}) }


  // handleModalChange1 () {
  //   this.setState({ ModalContent : '<h1>Modal1 Content</h1>' })
  //   }

  // handleModalChange2  (){
  //   this.setState({ ModalContent : '<h1>Modal2 Content</h1>' })
  // }


  componentDidMount(){
    axios.get("https://abcapi.vidaria.in/allbpatientdetails")
    .then((res)=>{
      console.log(res)
      this.setState({
        users:res.data.breast_patient,
        id:0,
        username:'',
        email:'',
        phone:'',
        password:'',
        role:'',
        speciality:'',
      
        
      })
    })
  }

  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide })
}


secondhandleModalShowHide() {
  this.setState({ open: !this.state.open })
}


thirdhandleModalShowHide() {
    this.setState({ open: !this.state.open })
  }



Updatebpatient = () => {
  axios.get(`https://abcapi.vidaria.in/bpatientdetails/1`)
  .then((res)=>{
    console.log(res)
    this.setState({
      // users:res.data.bpatient,
   
      // is this correct way?
    
      
    })
  })
}


submit(event,id){
  event.preventDefault();
  if(id === 0){
    axios.post("https://abcapi.vidaria.in/adduser",{
      username:this.state.username,
      email:this.state.email,
      phone:this.state.phone,
      password:this.state.password,
      role:this.state.role,
      speciality:this.state.speciality,
    })
    .then((res)=>{
      console.log(res)
    })
  }else{
    axios.put(`https://abcapi.vidaria.in/userupdate/${this.props.match.params.id}`,{
      id:this.state.id,
      username:this.state.username,
      email:this.state.email,
      phone:this.state.phone,
      password:this.state.password,
      role:this.state.role,
      speciality:this.state.speciality,
    }).then(()=>{
      this.componentDidMount();
    })

  }

}
delete(id){
  axios.delete(`https://abcapi.vidaria.in/bpatientdelete/${id}`)
  .then(()=>{
    this.componentDidMount();
  })
}



  render()

 {
  return (
    <>

<div>



<Modal 
                
                size="lg"
                
                show={this.state.showHide}>
                    <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
                 </Modal.Header>
                    <Modal.Body>


{/* <Main /> */}



                    </Modal.Body>
                 
                </Modal>









           <Modal 
                
                size="lg"
                
                show={this.state.showHide}>
                    <Modal.Header closeButton onClick={() => this.secondhandleModalShowHide()}>
                 </Modal.Header>
                    <Modal.Body>


{/* <Main /> */}



                    </Modal.Body>
                   
                </Modal>


{/* modal 2 */}
                <Modal 
                
                size="lg"
                
                show={this.state.open}>
                    <Modal.Header closeButton onClick={() => this.thirdhandleModalShowHide()}> 
                 </Modal.Header>
                    <Modal.Body>


<Main1 />

                    </Modal.Body>
                   
                </Modal>







            </div>


      <Container fluid>
        <div className="container">
          <div className="row" >
          <Dropdown>
  <Dropdown.Toggle variant="success" id="dropdown-basic">
  Add Patient
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item  onClick={() => this.handleModalShowHide()}>Basic patient details</Dropdown.Item>
    <Dropdown.Item  onClick={() => this.secondhandleModalShowHide()}>Add examination</Dropdown.Item>
    <Dropdown.Item  onClick={() => this.thirdModalShowHide()}> Add investigation</Dropdown.Item>

  </Dropdown.Menu>
</Dropdown>
</div>
</div>
</Container>
</>
  );
 }
}

export default Main1;