
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Register2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Complaint  Information</h2>

        {/* <Row> */}

        <Form>
 
       
<Row>
<Col md="6">
<Form.Group>
          <Form.Label>smoking_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('smoking_years')}
defaultValue={values.smoking_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>alcohol</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('alcohol')}
defaultValue={values.alcohol}/>
          </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>alcohol_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('alcohol_years')}
 defaultValue={values.alcohol_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>family_history</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('family_history')}
 defaultValue={values.family_history}/>
          </Form.Group>
          </Col>

          <Col md="12">
          <Form.Group>
          <Form.Label>Health condition</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('health_condition')}
 defaultValue={values.health_condition}/>
          </Form.Group>
</Col>

</Row>

<Row>

<Col md="6">
<Form.Group>
          <Form.Label>Remarks</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('remarks')}
defaultValue={values.remarks}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>comorbidities</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('comorbidities')}
defaultValue={values.comorbidities}/>
          </Form.Group>
   

</Col>

<Col md="6">

<Form.Group>
        <Form.Label>Onset</Form.Label><br></br>
          <Form.Control type="text" onChange={this.props.handleChange('onset')}
						defaultValue={values.onset}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>onset_duration</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('onset_duration')} defaultValue={values.onset_duration}  />
          </Form.Group>
</Col>


</Row>



<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Register2;







