
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Examination extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Examination Information</h2>

        <Row>

        <Form>
  
        <Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>general_condition</Form.Label><br></br>
          <Form.Control type="text" onChange={this.props.handleChange('general_condition')}
						defaultValue={values.general_condition}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>weight</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('weight')} defaultValue={values.weight}  />
          </Form.Group>
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>height</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('height')}  defaultValue={values.height}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>bmi</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('bmi')} defaultValue={values.bmi}/>
          </Form.Group>
          </Col>
          </Row>


          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>bp</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('bp')} defaultValue={values.bp}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>pulse</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('pulse')} defaultValue={values.pulse}/>
          </Form.Group>
</Col>
<Col md="6">

<Form.Group>
          <Form.Label>temp</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('temp')} defaultValue={values.temp}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>respiratory_rate</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('respiratory_rate')} defaultValue={values.respiratory_rate}/>
          </Form.Group>

</Col>
</Row>

<Row>
  <Col md="6">
  <Form.Group>

            <Form.Label>examination_health_condition</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('ex_health_condition')} defaultValue={values.ex_health_condition}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>examination_remarks</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('ex_remarks')} defaultValue={values.ex_remarks}/>
                    </Form.Group>


</Col>
<Col md="6">
<Form.Group>

          <Form.Label>eyes</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('eyes')} defaultValue={values.eyes}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>skin_morphology</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('skin_morphology')} defaultValue={values.skin_morphology}/>
                    </Form.Group>


</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>skin_pattern</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('skin_pattern')} defaultValue={values.skin_pattern}/>
                    </Form.Group>


          <Form.Group>


          <Form.Label>skin_color</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('skin_color')}  defaultValue={values.skin_color}/>
                    </Form.Group>


          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>distribution</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('distribution')}  defaultValue={values.distribution}/>
                    </Form.Group>

          <Form.Group>

          <Form.Label>other_affects</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('other_affects')}  defaultValue={values.other_affects}/>
                    </Form.Group>


</Col>
</Row>

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>mucosa</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('mucosa')}  defaultValue={values.mucosa}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>hair_loss</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('hair_loss')} defaultValue={values.hair_loss}/>
                    </Form.Group>


         </Col> 
         <Col md="6">
         <Form.Group>

          <Form.Label>dandruff</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('dandruff')}  defaultValue={values.dandruff}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>nail</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('nail')} defaultValue={values.nail}/>
                    </Form.Group>


          </Col>
          </Row>


          <Row>
  <Col md="6">   
  <Form.Group>

          <Form.Label>lymphnodes</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('lymphnodes')} defaultValue={values.lymphnodes}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>lymphnodes_number</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('lymphnodes_number')} defaultValue={values.lymphnodes_number}/>
                    </Form.Group>

          </Col>
          <Col md="6">   

          <Form.Group>

          <Form.Label>tender</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('tender')} defaultValue={values.tender}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>tender_type</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('tender_type')} defaultValue={values.tender_type}/>
                    </Form.Group>
<br></br>





          {/* <Button onClick={this.saveAndContinue}>Save And Continue </Button> */}

          {/* <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button> */}


       </Col>
       </Row>


       <Row>
          <Col md="6">
       
          <Form.Group>
          <Form.Label>lymphnodes_tender_others</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('lymphnodes_tender_others')}
defaultValue={values.lymphnodes_tender_others}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>vascular</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('vascular')}
 defaultValue={values.vascular}/>
           </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>

          <Form.Label>cns_examination</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('cns_examination')}
defaultValue={values.cns_examination}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>cvs_examination</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('cvs_examination')}
 defaultValue={values.cvs_examination}/>
           </Form.Group>

</Col>
</Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>chest_examination</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('chest_examination')}
defaultValue={values.chest_examination}/>
           </Form.Group>
           </Col>
           <Col md="6">
          <Form.Group>
          <Form.Label>abdomen_examination</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('abdomen_examination')}
 defaultValue={values.abdomen_examination}/>
           </Form.Group>
</Col>



</Row>
<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
















        </Form>
   
      </Row>
      </Container>
    )
  }
}

export default Examination;







