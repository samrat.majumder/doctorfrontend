
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Inves_Biochemical extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Investigation</h2>
         <h5>Biochemical date: Information</h5> 

        <Row>

        <Form>
  
  
        <Table striped bordered hover>
  <thead>
    <tr>
    <th>DATE</th>
      <th>LFT (Bill(T),protein etc</th>
      <th>RFT BUN,CR,UREA</th>
      <th>CRP</th>
      <th>BSL</th>
      <th>UA</th>
      <th>OTHERS</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('biochemical_date')}
defaultValue={values.biochemical_date} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_lft')}
defaultValue={values.bio_lft} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('bio_rft')}
defaultValue={values.bio_rft} maxlength="50" size="4" /></td>


<td><input type="text" 	onChange={this.props.handleChange('bio_crp')}
defaultValue={values.bio_crp} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_bsl')}
defaultValue={values.bio_bsl} maxlength="50" size="4" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('bio_ua')}
defaultValue={values.bio_ua} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('bio_others')}
defaultValue={values.bio_others} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('biochemical_date2')}
defaultValue={values.biochemical_date2} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_lft2')}
defaultValue={values.bio_lft2} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('bio_rft2')}
defaultValue={values.bio_rft2} maxlength="50" size="4" /></td>


<td><input type="text" 	onChange={this.props.handleChange('bio_crp2')}
defaultValue={values.bio_crp2} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_bsl2')}
defaultValue={values.bio_bsl2} maxlength="50" size="4" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('bio_ua2')}
defaultValue={values.bio_ua2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('bio_others2')}
defaultValue={values.bio_others2} maxlength="50" size="4" /></td>

    </tr>





    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('biochemical_date3')}
defaultValue={values.biochemical_date3} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_lft3')}
defaultValue={values.bio_lft3} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('bio_rft3')}
defaultValue={values.bio_rft3} maxlength="50" size="4" /></td>


<td><input type="text" 	onChange={this.props.handleChange('bio_crp3')}
defaultValue={values.bio_crp3} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_bsl3')}
defaultValue={values.bio_bsl3} maxlength="50" size="4" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('bio_ua3')}
defaultValue={values.bio_ua3} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('bio_others3')}
defaultValue={values.bio_others3} maxlength="50" size="4" /></td>

    </tr>




    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('biochemical_date4')}
defaultValue={values.biochemical_date4} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_lft4')}
defaultValue={values.bio_lft4} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('bio_rft4')}
defaultValue={values.bio_rft4} maxlength="50" size="4" /></td>


<td><input type="text" 	onChange={this.props.handleChange('bio_crp4')}
defaultValue={values.bio_crp4} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_bsl4')}
defaultValue={values.bio_bsl4} maxlength="50" size="4" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('bio_ua4')}
defaultValue={values.bio_ua4} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('bio_others4')}
defaultValue={values.bio_others4} maxlength="50" size="4" /></td>

    </tr>
  


    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('biochemical_date5')}
defaultValue={values.biochemical_date5} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_lft5')}
defaultValue={values.bio_lft5} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('bio_rft5')}
defaultValue={values.bio_rft5} maxlength="50" size="4" /></td>


<td><input type="text" 	onChange={this.props.handleChange('bio_crp5')}
defaultValue={values.bio_crp5} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_bsl5')}
defaultValue={values.bio_bsl5} maxlength="50" size="4" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('bio_ua5')}
defaultValue={values.bio_ua5} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('bio_others5')}
defaultValue={values.bio_others5} maxlength="50" size="4" /></td>

    </tr>
  </tbody>
</Table>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Inves_Biochemical;







