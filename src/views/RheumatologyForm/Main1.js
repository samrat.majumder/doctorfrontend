

import React, { Component } from 'react';
import BasicInfo from './Register';
import Complaints from './Complaints'
import Register2 from './Register2'
import Register3 from './Register3'
import Register4 from './Register4'
import Register5 from './Register5'
import Register6 from './Register6'
import Register7 from './Register7'
import Register8 from './Register8'


// import Examination from './Examination';
// import Mus_examination from './Mus_examination'
// import Rom_examination from './Rom_examination'
// import Soft_tis_examination from './Soft_tis_examination'
// import Deformities_examination from './Deformities_examination';
// import Hypermobility_examination from './Hypermobility_examination';
// import Health_ass_ques from './Health_ass_ques';
// import Inves_Hematological from './Inves_Hematological';

// import Inves_Biochemical from './Inves_Biochemical';
// import Inves_Immonological from './Inves_Immonological';
// import Inves_Radiological from './Inves_Radiological';
// import Inves_Radiological from './Inves_Radiological';
import Summary from './Basic_PatientSummary';
import userdetails from '../admin'
import PatientManagement from '../PatientManagement'
import axios from 'axios';

class Main1 extends Component {
    state = {
        step:1,        
        pid:''
        ,date:''
        ,code:''
        ,place:''
        ,pname:''
        ,fhname:''
        ,age:''
        ,sex:''
        ,education:''
        ,address:''
        ,phone:false
        ,district:''
        ,state:''
        ,area:''
        ,referred_by:''
        ,occupation:''
        ,ethnic:''
        ,marital_status:''
        ,marital_status_years:false
        ,menses_frequency:''
        ,menses_loss:''
        ,menarche_years:false
        ,hysterectomy:''
        ,hysterectomy_years:false
        ,menopause:''
        ,menopause_years:false
        ,children:false
        ,children_male:false
        ,children_female:false
        ,abortions:''
        ,abortions_number:false
        ,abortions_cause:''
        ,current_lactation:''
        ,contraception_methods:''
        ,contraception_methods_type:''
        ,hormone_treatment:''
        ,addiction:''
        ,tobacco:''
        ,tobacco_years:false
        ,smoking:''
        ,smoking_years:false
        ,alcohol:''
        ,alcohol_years:false
        ,family_history:''
        ,health_condition:''
        ,remarks:''
        ,comorbidities:''
        ,onset:''
        ,onset_duration:''
        ,onset_duration_type:''
        ,first_symptom:''
        ,initial_site_joint:''
        ,soft_tissue:''
        ,others:''
        ,course1:''
        ,course2:''
        ,pattern1:''
        ,pattern2:''
        ,current_relapse:''
        ,current_relapse_type:''
        ,detailed_history:''
        ,pasthistory:''
        ,surgical_history:''
        ,drug_history:''
        ,drug_allergy:''
        ,drug_allergy_type:''
        ,bowelhabit:''
        ,sleep:''
        ,user_id:userdetails()

      }
      nextStep = () => {
        const { step } = this.state
        this.setState({
          step: step + 1
        })
      }

  
      prevStep = () => {
        const { step } = this.state
        this.setState({
          step: step - 1
        })
      }
    

      handleChange = input => event => {
        this.setState({ [input]: event.target.value })
      }




      opensweetalert()
      {
        Swal.fire({
          title: 'Patient record added',
          text: "sucess",
          type: 'success',
          
        }).then(function() {
          window.location = "/admin/dashboard";
      });
      }
    

      sameemailsweetalert()
  {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'patient Id  exist please change your patient Id',
      
    })
  }

      saveAll = () => {

        let Obj={
        pid:this.state.pid,
        date:this.state.date,
        code:this.state.code,
        place:this.state.place,
        pname:this.state.pname,
        fhname:this.state.fhname,
        age:this.state.age,
        sex:this.state.sex,
        education:this.state.education,
        address:this.state.address,
        phone:this.state.phone,
        district:this.state.district,
        state:this.state.state,
        area:this.state.area,
        referred_by:this.state.referred_by,
        occupation:this.state.occupation,
        ethnic:this.state.ethnic,
        marital_status:this.state.marital_status,
        marital_status_years :this.state.marital_status_years,
        menses_frequency:this.state.menses_frequency,
        menses_loss:this.state.menses_loss,
        menarche_years :this.state.menarche_years,
        hystrectomy :this.state.hystrectomy,
        hystrectomy_years:this.state.hystrectomy_years,
        menopause:this.state.menopause,
        menopause_years:this.state.menarche_years,
        children:this.state.children,
        children_male:this.state.children_male,
        children_female:this.state.children_female,
        abortions:this.state.abortions,
        abortions_number:this.state.abortions_number,
        abortions_cause:this.state.abortions_cause,
      
        current_lactation:this.state.current_lactation,
        contraception_methods:this.state.contraception_methods,
        contraception_methods_type:this.state.contraception_methods_type,
        hormone_treatment:this.state.hormone_treatment,
        addiction:this.state.addiction,
        tobacco:this.state.tobacco,
        tobacco_years:this.state.tobacco_years,
        smoking:this.state.smoking,
        smoking_years:this.state.smoking_years,
        alcohol:this.state.alcohol,
        alcohol_years:this.state.alcohol_years,
        family_history:this.state.family_history,
        health_condition:this.state.health_condition,
        remarks:this.state.remarks,
        comorbidities:this.state.comorbidities,

        onset:this.state.onset,
        onset_duration:this.state.onset_duration,
        onset_duration_type:this.state.onset_duration_type,



        first_symptom:this.state.first_symptom,
        initial_site_joint:this.state.initial_site_joint,
        soft_tissue:this.state.soft_tissue,
        others:this.state.others,

        course1:this.state.course1,
        course2:this.state.course2,
        pattern1:this.state.pattern1,


        pattern2:this.state.pattern2,
        current_relapse:this.state.current_relapse,
        current_relapse_type:this.state.current_relapse_type,
        detailed_history:this.state.detailed_history,

        pasthistory:this.state.pasthistory,
        surgical_history:this.state.surgical_history,
        
        drug_history:this.state.drug_history,
        drug_allergy:this.state.drug_allergy,
        drug_allergy_type:this.state.drug_allergy_type,
        bowelhabit:this.state.bowelhabit,
        bladderhabit:this.state.bladderhabit,
        sleep:this.state.sleep,
       

        user_id:this.state.user_id,



          
        }

        axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`,{

          activity :"Rheumatology Patient Added",
          user_id:this.state.user_id,
    
        })
            axios.post(`https://abcapi.vidaria.in/addrheumapatient?X-AUTH=abc123`,Obj)
          
            .then(res => {

              if (res.data.success == "false"){
               this.sameemailsweetalert()
                
              }
              if(res.data.success == "true"){
                this.opensweetalert()
                this.componentDidMount();
                this.props.history.push(`/admin/umanagement`)
                console.log('yes aya mai')
        
              }



            // this.opensweetalert()
            // this.componentDidMount();

            // this.props.push(`/admin/dashboard`)
            console.log(res);
            console.log(res.data);
      }).catch(function (error) {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          // console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          // console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          // console.log('Error', error.message);
        }
        // console.log(error.config);
      });
      
      }

  
  
	render() {
		const { step } = this.state;
		const {
            pid,date,code,place,pname,fhname,age,sex,education,address,phone,district,state,area,referred_by,occupation,ethnic,marital_status,marital_status_years,menses_frequency,menses_loss,menarche_years,hysterectomy,hysterectomy_years,menopause,menopause_years,children,children_male,children_female,abortions,abortions_number,abortions_cause,current_lactation,contraception_methods,contraception_methods_type,hormone_treatment,addiction,tobacco,tobacco_years,smoking,smoking_years,alcohol,alcohol_years,family_history,health_condition,remarks,comorbidities,onset,onset_duration,onset_duration_type,first_symptom,initial_site_joint,soft_tissue,others,course1,course2,pattern1,pattern2,current_relapse,current_relapse_type,detailed_history,pasthistory,surgical_history,drug_history,drug_allergy,drug_allergy_type,bowelhabit,sleep,user_id

      
      } = this.state;



		const values = { 
          pid,date,code,place,pname,fhname,age,sex,education,address,phone,district,state,area,referred_by,occupation,ethnic,marital_status,marital_status_years,menses_frequency,menses_loss,menarche_years,hysterectomy,hysterectomy_years,menopause,menopause_years,children,children_male,children_female,abortions,abortions_number,abortions_cause,current_lactation,contraception_methods,contraception_methods_type,hormone_treatment,addiction,tobacco,tobacco_years,smoking,smoking_years,alcohol,alcohol_years,family_history,health_condition,remarks,comorbidities,onset,onset_duration,onset_duration_type,first_symptom,initial_site_joint,soft_tissue,others,course1,course2,pattern1,pattern2,current_relapse,current_relapse_type,detailed_history,pasthistory,surgical_history,drug_history,drug_allergy,drug_allergy_type,bowelhabit,sleep,user_id

    
    };
		switch (step) {
			case 1:
				return <BasicInfo
					nextStep={this.nextStep}
					handleChange={this.handleChange}
					values={values}
				/>
      case 2:
        return <Register2
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
              />


      // case 3:
      //   return <Register2
      //           nextStep={this.nextStep}
      //           prevStep={this.prevStep}

      //           handleChange={this.handleChange}
      //           values={values}
      //         />


      case 3:
        return <Register3
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
      />


      case 4:
        return <Register4
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
      />


      case 5:
        return <Register5
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
      />


      case 6:
        return <Register6
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
      />



      case 7:
        return <Register7
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
      />



      case 8:
        return <Register8
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
      />


      case 9:
        return <Complaints
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            saveAll={this.saveAll.bind(this)} 
            handleChange={this.handleChange}
            values={values}
          />



      case 11:
        return <Summary 
                 nextStep={this.nextStep}
                 prevStep={this.prevStep}
                 values={values}
                 saveAll={this.saveAll.bind(this)} 
                 />
      // case 3:
      //   return <Examination
      //           nextStep={this.nextStep}
      //           prevStep={this.prevStep}

      //           handleChange={this.handleChange}
      //           values={values}
      //         />


			// case 4:
			// 	return <Mus_examination
			// 		nextStep={this.nextStep}
			// 		prevStep={this.prevStep}
			// 		handleChange={this.handleChange}
			// 		values={values}
			// 	/>


      // case 5:
      //   return <Rom_examination
      //       nextStep={this.nextStep}
      //       prevStep={this.prevStep}
      //       handleChange={this.handleChange}
      //       values={values}
      //     />
          
          

      // case 6:
      //   return <Soft_tis_examination
      //           nextStep={this.nextStep}
      //           prevStep={this.prevStep}
      //           handleChange={this.handleChange}
      //           values={values}
      //         /> 


      // case 7:
      //   return <Deformities_examination
      //               nextStep={this.nextStep}
      //               prevStep={this.prevStep}
      //               handleChange={this.handleChange}
      //               values={values}
      //             />         
			// case 8:
			// 	return <Hypermobility_examination
      //          nextStep={this.nextStep}
      //          prevStep={this.prevStep}
      //          handleChange={this.handleChange}
      //          values={values}
      //   />




      // case 9:
      //   return <Health_ass_ques
      //            nextStep={this.nextStep}
      //            prevStep={this.prevStep}
      //            handleChange={this.handleChange}
      //            values={values}
      //     />



      // case 10:
			// 	return <Inves_Hematological
      //          nextStep={this.nextStep}
      //          prevStep={this.prevStep}
      //          handleChange={this.handleChange}
      //          values={values}
      //   />



      // case 11:
			// 	return <Inves_Biochemical
      //          nextStep={this.nextStep}
      //          prevStep={this.prevStep}
      //          handleChange={this.handleChange}
      //          values={values}
      //   />


      // case 12:
			// 	return <Inves_Immonological
      //          nextStep={this.nextStep}
      //          prevStep={this.prevStep}
      //          handleChange={this.handleChange}
      //          values={values}
      //   />

      // case 13:
      //   return <Inves_Radiological
      //            nextStep={this.nextStep}
      //            prevStep={this.prevStep}
      //            handleChange={this.handleChange}
      //            values={values}
      //     />



     
	
		}
	}
}

export default Main1;