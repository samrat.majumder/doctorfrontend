
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Inves_Immonological extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Investigation</h2>
         <h5>Immonological date: Information</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
    <th>DATE</th>
      <th>RF</th>
      <th>ACCP</th>
      <th>ANA</th>
      <th>ANA-WB</th>
      <th>ACE</th>
      <th>DsDNA</th>
      <th>HLA 827</th>
      <th>C3 </th>
      <th>C3</th>
      <th>OTHERS</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('immunological_date')}
defaultValue={values.immunological_date} maxlength="50" size="2" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('imm_rf')}
defaultValue={values.imm_rf} maxlength="50" size="2" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('imm_accp')}
defaultValue={values.imm_accp} maxlength="50" size="2" /></td>


<td><input type="text" 	onChange={this.props.handleChange('imm_ana')}
defaultValue={values.imm_ana} maxlength="50" size="2" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('imm_anawb')}
defaultValue={values.imm_anawb} maxlength="50" size="2" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('imm_ace')}
defaultValue={values.imm_ace} maxlength="50" size="2" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('imm_dsdna')}
defaultValue={values.imm_dsdna} maxlength="50" size="2" /></td>


<td><input type="text" 	onChange={this.props.handleChange('imm_hlab27')}
defaultValue={values.imm_hlab27} maxlength="50" size="2" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('imm_c3')}
defaultValue={values.imm_c3} maxlength="50" size="2" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('imm_c3')}
defaultValue={values.imm_c3} maxlength="50" size="2" /></td>



<td><input type="text" 						onChange={this.props.handleChange('imm_others')}
defaultValue={values.imm_others} maxlength="50" size="2" /></td>

    </tr>



    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('immunological_date2')}
defaultValue={values.immunological_date2} maxlength="50" size="2" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('imm_rf2')}
defaultValue={values.imm_rf2} maxlength="50" size="2" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('imm_accp2')}
defaultValue={values.imm_accp2} maxlength="50" size="2" /></td>


<td><input type="text" 	onChange={this.props.handleChange('imm_ana2')}
defaultValue={values.imm_ana2} maxlength="50" size="2" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('imm_anawb2')}
defaultValue={values.imm_anawb2} maxlength="50" size="2" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('imm_ace2')}
defaultValue={values.imm_ace2} maxlength="50" size="2" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('imm_dsdna2')}
defaultValue={values.imm_dsdna2} maxlength="50" size="2" /></td>


<td><input type="text" 	onChange={this.props.handleChange('imm_hlab272')}
defaultValue={values.imm_hlab272} maxlength="50" size="2" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('imm_c32')}
defaultValue={values.imm_c32} maxlength="50" size="2" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('imm_c32')}
defaultValue={values.imm_c32} maxlength="50" size="2" /></td>



<td><input type="text" 						onChange={this.props.handleChange('imm_others2')}
defaultValue={values.imm_others2} maxlength="50" size="2" /></td>

    </tr>


    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('immunological_date3')}
defaultValue={values.immunological_date3} maxlength="50" size="2" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('imm_rf3')}
defaultValue={values.imm_rf3} maxlength="50" size="2" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('imm_accp3')}
defaultValue={values.imm_accp3} maxlength="50" size="2" /></td>


<td><input type="text" 	onChange={this.props.handleChange('imm_ana3')}
defaultValue={values.imm_ana3} maxlength="50" size="2" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('imm_anawb3')}
defaultValue={values.imm_anawb3} maxlength="50" size="2" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('imm_ace3')}
defaultValue={values.imm_ace3} maxlength="50" size="2" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('imm_dsdna3')}
defaultValue={values.imm_dsdna3} maxlength="50" size="2" /></td>


<td><input type="text" 	onChange={this.props.handleChange('imm_hlab373')}
defaultValue={values.imm_hlab373} maxlength="50" size="2" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('imm_c33')}
defaultValue={values.imm_c33} maxlength="50" size="2" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('imm_c33')}
defaultValue={values.imm_c33} maxlength="50" size="2" /></td>



<td><input type="text" 						onChange={this.props.handleChange('imm_others3')}
defaultValue={values.imm_others3} maxlength="50" size="2" /></td>

    </tr>


  </tbody>
</Table>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Inves_Immonological;







