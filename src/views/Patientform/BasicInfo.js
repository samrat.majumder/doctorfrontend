


// import React, { Component } from 'react';
// import { Form, Button } from 'semantic-ui-react';

// class Step1 extends Component {

// 	saveAndContinue = (e) => {
// 		e.preventDefault()
// 		this.props.nextStep()
// 	}

// 	render() {
// 		const { values } = this.props;
// 		return (
// 			<Form >
// 				<h1 className="ui centered">Enter User Details</h1>
// 				<Form.Field>
// 					<label>First Name</label><br></br>
// 					<input
// 						placeholder='First Name'
// 						onChange={this.props.handleChange('username')}
// 						defaultValue={values.username}
// 					/>
// 				</Form.Field>
				
// 				<Form.Field>
// 					<label>Email Address</label><br></br>
// 					<input
// 						type='email'
// 						placeholder='Email Address'
// 						onChange={this.props.handleChange('email')}
// 						defaultValue={values.email}
// 					/>
// 				</Form.Field>

//                 <Form.Field>
// 					<label>Phone</label><br></br>
// 					<input
// 						type='number'
// 						placeholder='phone'
// 						onChange={this.props.handleChange('phone')}
// 						defaultValue={values.phone}
// 					/>
// 				</Form.Field>
// 				<Button onClick={this.saveAndContinue}>Save And Continue </Button>
// 			</Form>
// 		)
// 	}
// }

// export default Step1;




import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class BasicInfo extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>
      <h4 style={{"color":"red"}}>please fill all input in this page</h4>

        <Form>
  
        <Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>Patient Id</Form.Label><br></br>
          <Form.Control type="number" onChange={this.props.handleChange('pid')}
						defaultValue={values.pid}/>
          </Form.Group>

          {/* <Form.Group>
          <Form.Label>Date</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('date')} defaultValue={values.date}  />
          </Form.Group> */}
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('code')}  defaultValue={values.code}/>
          </Form.Group>
          </Col>


          </Row>

          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>place</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('place')} defaultValue={values.place}/>
          </Form.Group>
          </Col>
         
          <Col md="6">
          <Form.Group>
          <Form.Label>Patient Name</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pname')} defaultValue={values.pname}/>
          </Form.Group>
          </Col>

</Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>Father or Husband name</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('fhname')} defaultValue={values.fhname}/>
          </Form.Group>
</Col>
<Col md="6">

<Form.Group>
          <Form.Label>Age</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('age')} defaultValue={values.age}/>
          </Form.Group>

          </Col>
</Row>
<Row>
<Col md="12">

          <Form.Group>
          <Form.Label>Gender</Form.Label><br></br>
          <Form.Control type="text"   onChange={this.props.handleChange('sex')} defaultValue={values.sex}/>
          </Form.Group>

</Col>
</Row>
{/* 
<Row>
  <Col md="6">
            <label>Education</label><br></br>
          <input type="text"  onChange={this.props.handleChange('education')} defaultValue={values.education}/>
          <br/>

          <label>Address</label><br></br>
          <input type="text"  onChange={this.props.handleChange('address')} defaultValue={values.address}/>
          <br/>

</Col>
<Col md="6">
          <label>Phone</label><br></br>
          <input type="number"  onChange={this.props.handleChange('phone')} defaultValue={values.phone}/>
          <br/>


          <label>District</label><br></br>
          <input type="text"  onChange={this.props.handleChange('district')} defaultValue={values.district}/>
          <br/>

</Col>
</Row> */}
{/* 
<Row>
  <Col md="6">
          <label>State</label><br></br>
          <input type="text"  onChange={this.props.handleChange('state')} defaultValue={values.state}/>
          <br/>


          <label>Area</label><br></br>
          <input type="text"  onChange={this.props.handleChange('area')}  defaultValue={values.area}/>
          <br/>

          </Col>
          <Col md="6">

          <label>Referred_by</label><br></br>
          <input type="text"  onChange={this.props.handleChange('referred_by')}  defaultValue={values.referred_by}/>
          <br/>

          <label>Occupation</label><br></br>
          <input type="text"  onChange={this.props.handleChange('occupation')}  defaultValue={values.occupation}/>
          <br/>

</Col>
</Row>

<Row>
  <Col md="6">
          <label>Ethnic</label><br></br>
          <input type="text"  onChange={this.props.handleChange('ethnic')}  defaultValue={values.ethnic}/>
          <br/>


          <label>Marital_status</label><br></br>
          <input type="text"  onChange={this.props.handleChange('marital_status')} defaultValue={values.marital_status}/>
          <br/>

         </Col> 
         <Col md="6">
          <label>marital_status_years</label><br></br>
          <input type="number"  onChange={this.props.handleChange('marital_status_years')}  defaultValue={values.marital_status_years}/>
          <br/>
          <label>menses_frequency</label><br></br>
          <input type="text"  onChange={this.props.handleChange('menses_frequency')} defaultValue={values.menses_frequency}/>
          <br/>

          </Col>
          </Row>


          <Row>
  <Col md="6">   
          <label>menses_loss</label><br></br>
          <input type="text"  onChange={this.props.handleChange('menses_loss')} defaultValue={values.menses_loss}/>
          <br/>
          <label>menarche_years</label><br></br>
          <input type="number"  onChange={this.props.handleChange('menarche_years')} defaultValue={values.menarche_years}/>
          <br/>
          </Col>
          <Col md="6">   

          <label>hystrectomy</label><br></br>
          <input type="text"  onChange={this.props.handleChange('hystrectomy')} defaultValue={values.hystrectomy}/>
          <br/>
          <label>hystrectomy_years</label><br></br>
          <input type="number"  onChange={this.props.handleChange('hystrectomy_years')} defaultValue={values.hystrectomy_years}/>
          <br/><br></br>

          <Button onClick={this.saveAndContinue}>Save And Continue </Button>



       </Col>
       </Row> */}

<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

        </Form>
   
      
      </Container>
    )
  }
}

export default BasicInfo;










  
//   nextStep(e) {
//     e.preventDefault()
//     var data = {
//       pid: this.refs.pid.value,
//       date: this.refs.date.value,
//       code: this.refs.code.value,
//       place: this.refs.place.value,
//       pname: this.refs.pname.value,
//       fhname: this.refs.fhname.value,
//       age: this.refs.age.value,
//       sex: this.refs.sex.value,
//       education: this.refs.education.value,
//       address: this.refs.address.value,
//       phone: this.refs.phone.value,
//       district: this.refs.district.value,
//       state: this.refs.state.value,
//       area: this.refs.area.value,
//       referred_by: this.refs.referred_by.value,
//       occupation: this.refs.occupation.value,
//       ethinic: this.refs.ethinic.value,
//       marital_status: this.refs.marital_status.value,
//       marital_status_years: this.refs.marital_status_years.value,
//       menses_frequency: this.refs.menses_frequency.value,
//       menses_loss: this.refs.menses_loss.value,
//       menarche_years: this.refs.menarche_years.value,
//       hystrectomy: this.refs.hystrectomy.value,
//       hystrectomy_years: this.refs.hystrectomy_years.value,

//     }
//     this.props.saveValues(data);
//     this.props.nextStep();
//   }
// }

// export default BasicInfo;