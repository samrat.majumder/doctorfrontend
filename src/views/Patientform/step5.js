
// import React, { Component } from 'react';
// import { Button, List } from 'semantic-ui-react';

// class Step3 extends Component {
// 	saveAndContinue = (e) => {
// 		e.preventDefault();
// 		this.props.nextStep();
// 	}

// 	back = (e) => {
// 		e.preventDefault();
// 		this.props.prevStep();
// 	}

// 	render() {
// 		const { values: { username,email,phone,password,role,speciality} } = this.props;

// 		return (
// 			<div>
// 				<h1 className="ui centered">Confirm your Details</h1>
// 				<p>Click Confirm if the following details have been correctly entered</p>
// 				<List>
// 					<List.Item>
// 						<List.Icon name='users' />
// 						<List.Content>Name: {username}</List.Content>
// 					</List.Item>
					
// 					<List.Item>
// 						<List.Icon name='mail' />
// 						<List.Content>
// 							<a href='mailto:jack@semantic-ui.com'>{email}</a>
// 						</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='phone' />
// 						<List.Content>{phone}phone</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='role' />
// 						<List.Content> {role}</List.Content>
// 					</List.Item>
//                     <List.Item>
// 						<List.Icon name='speciality' />
// 						<List.Content> {speciality}</List.Content>
// 					</List.Item>
// 				</List>

// 				<Button onClick={this.back}>Back</Button>
//                 <button onClick={this.props.saveAll}>Submit</button>
// 			</div>
// 		)
// 	}
// }

// export default Step3;



import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Step5 extends Component {


  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render() {
		const { values } = this.props
    return (
      <Container fluid>
      <h2>Patient Information</h2>

        {/* <Row> */}

        <Form>





    <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>breastlump_location</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('breastlump_location')}
 defaultValue={values.breastlump_location}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>breastlump_size</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('breastlump_size')}
defaultValue={values.breastlump_size}/>
           </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>overlying_skin</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('overlying_skin')}
defaultValue={values.overlying_skin}/>
           </Form.Group>

          <Form.Group>
          <Form.Label>axillarylump</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('axillarylump')}
defaultValue={values.axillarylump}/>
           </Form.Group>
</Col>
</Row>


<Row>
          <Col md="6">
          <Form.Group>

          <Form.Label>axillarylump_side</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('axillarylump_side')}
 defaultValue={values.axillarylump_side}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>matted</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('matted')}
defaultValue={values.matted}/>
           </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>axillarylump_size</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('axillarylump_size')}
defaultValue={values.axillarylump_size}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>nipple_discharge</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('nipple_discharge')}
 defaultValue={values.nipple_discharge}/>
           </Form.Group>

          </Col>
          </Row>
        
          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>nipple_discharge_frequency</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('nipple_discharge_frequency')}
 defaultValue={values.nipple_discharge_frequency}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>nipple_discharge_colour</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('nipple_discharge_colour')}
defaultValue={values.nipple_discharge_colour}/>
           </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>mastalgia</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('mastalgia')}
defaultValue={values.mastalgia}/>
           </Form.Group>

          <Form.Group>
          <Form.Label>mastitis</Form.Label><br></br>
         <Form.Control type="text" 						onChange={this.props.handleChange('mastitis')}
 defaultValue={values.mastitis}/>
           </Form.Group>
 </Col>
</Row>
<Row>
	   <Col md="6">
       <Button className="btn-fill"  onClick={this.back}>Back</Button>


			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning" onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>
			</Form>
			{/* </Row> */}
			</Container>
	)
	}
}
export default Step5;