


import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Step7 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render() 
  {
		const { values } = this.props
    return (
      <Container fluid>
      <h2>Patient Information</h2>

        {/* <Row> */}

        <Form>


<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>bp</Form.Label>
         <Form.Control type="number" 						onChange={this.props.handleChange('bp')}
defaultValue={values.bp}/>
         </Form.Group>


          <Form.Group>
          <Form.Label>pulse</Form.Label>
         <Form.Control type="number" 						onChange={this.props.handleChange('pulse')}
defaultValue={values.pulse}/>
         </Form.Group>
       </Col>
       <Col md="6">
       <Form.Group>
          <Form.Label>temp</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('temp')}
defaultValue={values.temp}/>
         </Form.Group>
          <Form.Group>
          <Form.Label>respiratory_rate</Form.Label>
         <Form.Control type="number" 						onChange={this.props.handleChange('respiratory_rate')}
 defaultValue={values.respiratory_rate}/>
         </Form.Group>
          
          </Col>

          </Row>

          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>health_condition</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('health_condition')}
defaultValue={values.health_condition}/>
         </Form.Group>
          <Form.Group>
          <Form.Label>examination_remarks</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('examination_remarks')}
defaultValue={values.examination_remarks}/>
         </Form.Group>
          </Col>
         
          <Col md="6">
          <Form.Group>
          <Form.Label>bra_size</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('bra_size')}
 defaultValue={values.bra_size}/>
         </Form.Group>
          <Form.Group>
          <Form.Label>usg</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('usg')}
defaultValue={values.usg}/>
         </Form.Group>
</Col>
</Row>




{/* 
<Row>
          <Col md="6">

          <label>mmg</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mmg')}
defaultValue={values.mmg}/>
          <br/>

          <label>mri</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mri')}
 defaultValue={values.mri}/>
          <br/>
          </Col>
          <Col md="6">


          <label>fnac</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('fnac')}
 defaultValue={values.fnac}/>
          <br/>

          <label>core_biopsy</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('core_biopsy')}
defaultValue={values.core_biopsy}/>
        
          <br/>
        </Col>
        </Row>

        <Row>
          <Col md="6">

          <label>incision_biopsy</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('incision_biopsy')}
defaultValue={values.incision_biopsy}/>
          <br/>
          <label>investigation_remarks</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('investigation_remarks')}
           defaultValue={values.investigation_remarks}/>
          <br/>
          </Col>

          <Col md="6">
          <label>blood_investigation</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('blood_investigation')}
defaultValue={values.blood_investigation}/>
          <br/>

          <label>diagnosis</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('diagnosis')}
defaultValue={values.diagnosis}/>
          <br/>
          </Col>
          </Row>




        <Row>
          <Col md="6">
          <label>treatment_plan</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('treatment_plan')}
defaultValue={values.treatment_plan}/>
          <br/><br></br>
          <Button onClick={this.back}>Back</Button>

          </Col>
          <Col md="6">
          <label>User Id</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('user_id')}
defaultValue={values.user_id}/>
          <br/><br></br>
          <Button onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row> */}
<Row>
	   <Col md="6">
	   <Button className="btn-fill"  onClick={this.back}>Back</Button>
	 

			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning" onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>
          
        </Form>
        
        
      

        {/* </Row> */}
      </Container>
    )

  
  }

}

export default Step7;
