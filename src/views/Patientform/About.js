
 
// import React, { Component } from 'react';
// import { Form, Button } from 'semantic-ui-react';

// class Step2 extends Component {
// 	saveAndContinue = (e) => {
// 		e.preventDefault();
// 		this.props.nextStep();
// 	}

// 	back = (e) => {
// 		e.preventDefault();
// 		this.props.prevStep();
// 	}

// 	render() {
// 		const { values } = this.props
// 		return (
// 			<Form color='blue' >
// 				<h1 className="ui centered">Enter Personal Details</h1>
// 				<Form.Field>
// 					<label>password</label><br></br>
// 					<input placeholder='password'
// 						onChange={this.props.handleChange('password')}
// 						defaultValue={values.password}
// 					/>
// 				</Form.Field>
// 				<Form.Field>
// 					<label>role</label><br></br>
// 					<input placeholder='role'
// 						onChange={this.props.handleChange('role')}
// 						defaultValue={values.role}
// 					/>
// 				</Form.Field>
// 				<Form.Field>
// 					<label>speciality</label><br></br>
// 					<input placeholder='speciality'
// 						onChange={this.props.handleChange('speciality')}
// 						defaultValue={values.speciality}
// 					/>
// 				</Form.Field>
// 				<Button onClick={this.back}>Back</Button>
// 				<Button onClick={this.saveAndContinue}>Save And Continue </Button>
// 			</Form>
// 		)
// 	}
// }

// export default Step2;


import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class About extends Component {


  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render() {
		const { values } = this.props
    return (
      <Container fluid>
      <h2>Patient Information</h2>

        {/* <Row> */}

        <Form>
  
        <Row>
          <Col md="6">
       
          <Form.Group>
          <Form.Label>menopause</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('menopause')}
defaultValue={values.menopause}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>menopause_years</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('menopause_years')}
 defaultValue={values.menopause_years}/>
           </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>

          <Form.Label>no of children</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children')}
defaultValue={values.children}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>children_male</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children_male')}
 defaultValue={values.children_male}/>
           </Form.Group>

</Col>
</Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>children_female</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children_female')}
defaultValue={values.children_female}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('abortions')}
 defaultValue={values.abortions}/>
           </Form.Group>
</Col>
<Col md="6">
<Form.Group>
          <Form.Label>abortions_number</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('abortions_number')}
 defaultValue={values.abortions_number}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions_cause</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('abortions_cause')}
 defaultValue={values.abortions_cause}/>
           </Form.Group>
         </Col>
         </Row>


{/* 
<Row>
          <Col md="6">

         <label>breastfed</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('breastfed')}
 defaultValue={values.breastfed}/>
          <br/>


          <label>breastfed_years</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('breastfed_years')}
defaultValue={values.breastfed_years}/>
          <br/>
</Col>
<Col md="6">


          <label>current_lactation</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('current_lactation')}
defaultValue={values.current_lactation}/>
          <br/>


          <label>contraception_methods</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('contraception_methods')}
defaultValue={values.contraception_methods}/>
          <br/>
         </Col>
         </Row>



       <Row>
          <Col md="6">
        <label>contraception_methods_type</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('contraception_methods_type')}
defaultValue={values.contraception_methods_type}/>
          <br/>
          <label>hormone_treatment</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('hormone_treatment')}
defaultValue={values.hormone_treatment}/>
          <br/>
          </Col>

          <Col md="6">
          <label>addiction</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('addiction')}
defaultValue={values.addiction}/>
          <br/>
          <label>tobacco</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('tobacco')}
defaultValue={values.tobacco}/>
          <br/>

          </Col>
          </Row>

          <Row>
          <Col md="6">
          <label>tobacco_years</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('tobacco_years')}
defaultValue={values.tobacco_years}/>
          <br/>
          <label>smoking</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('smoking')}
 defaultValue={values.smoking}/>
          <br/>
          </Col>

          <Col md="6">
          <label>smoking_years</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('smoking_years')}
defaultValue={values.smoking_years}/>
          <br/>
          <label>alcohol</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('alcohol')}
defaultValue={values.alcohol}/>
          <br/>
          </Col>

          </Row>

          <Row>
          <Col md="6">

          <label>alcohol_years</label><br></br>
          <input type="number" 						onChange={this.props.handleChange('alcohol_years')}
 defaultValue={values.alcohol_years}/>
          <br/>
          <label>family_history</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('family_history')}
 defaultValue={values.family_history}/>
          <br/>
          </Col>
          <Col md="6">
          <label>comorbidities</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('comorbidities')}
defaultValue={values.comorbidities}/>
          <br/>
          <label>breastlump</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('breastlump')}
defaultValue={values.breastlump}/>
          <br/>
          </Col>
          </Row>


        


    <Row>
          <Col md="6">
          <label>breastlump_location</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('breastlump_location')}
 defaultValue={values.breastlump_location}/>
          <br/>
          <label>breastlump_size</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('breastlump_size')}
defaultValue={values.breastlump_size}/>
          <br/>
</Col>

<Col md="6">

          <label>overlying_skin</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('overlying_skin')}
defaultValue={values.overlying_skin}/>
          <br/>
          <label>axillarylump</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('axillarylump')}
defaultValue={values.axillarylump}/>
          <br/>
</Col>
</Row>


<Row>
          <Col md="6">

          <label>axillarylump_side</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('axillarylump_side')}
 defaultValue={values.axillarylump_side}/>
          <br/>
          <label>matted</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('matted')}
defaultValue={values.matted}/>
          <br/>
</Col>

<Col md="6">
          <label>axillarylump_size</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('axillarylump_size')}
defaultValue={values.axillarylump_size}/>
          <br/>
          <label>nipple_discharge</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('nipple_discharge')}
 defaultValue={values.nipple_discharge}/>
          <br/>

          </Col>
          </Row>
        
          <Row>
          <Col md="6">
          <label>nipple_discharge_frequency</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('nipple_discharge_frequency')}
 defaultValue={values.nipple_discharge_frequency}/>
          <br/>
          <label>nipple_discharge_colour</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('nipple_discharge_colour')}
defaultValue={values.nipple_discharge_colour}/>
          <br/><br></br>
          <Button onClick={this.back}>Back</Button>
</Col>

<Col md="6">
          <label>mastalgia</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mastalgia')}
defaultValue={values.mastalgia}/>
          <br/>
          <label>mastitis</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mastitis')}
 defaultValue={values.mastitis}/>
          <br/><br></br>


          
				<Button onClick={this.saveAndContinue}>Save And Continue </Button>
</Col>
</Row> */}

<Row>
	   <Col md="6">
	   <Button className="btn-fill"  onClick={this.back}>Back</Button>
	 

			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning" onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>
        </Form>
        
        
      

        {/* </Row> */}
      </Container>
          )
  }
}


export default About;
  // nextStep(e) {
  //   e.preventDefault()
  //   var data = {
    
  //     menopause: this.refs.menopause.value,
  //     menopause_years: this.refs.menopause_years.value,
  //     children: this.refs.children.value,
  //     children_male: this.refs.children_male.value,
  //     children_female: this.refs.children_female.value,
  //     abortions: this.refs.abortions.value,
  //     abortions_number: this.refs.abortions_number.value,
  //     abortions_cause: this.refs.abortions_cause.value,
  //     breastfed: this.refs.breastfed.value,
  //     breastfed_years: this.refs.breastfed_years.value,
  //     current_lactation: this.refs.current_lactation.value,
  //     contraception_methods: this.refs.contraception_methods.value,
  //     contraception_methods_type: this.refs.contraception_methods_type.value,
  //     hormone_treatment: this.refs.hormone_treatment.value,
  //     addiction: this.refs.addiction.value,
  //     tobacco: this.refs.tobacco.value,
  //     tobacco_years: this.refs.tobacco_years.value,
  //     smoking: this.refs.smoking.value,
  //     smoking_years: this.refs.smoking_years.value,
  //     alcohol: this.refs.alcohol.value,
  //     alcohol_years: this.refs.alcohol_years.value,
  //     family_history: this.refs.family_history.value,
  //     comorbidities: this.refs.comorbidities.value,
  //     breastlump: this.refs.breastlump.value,
  //     breastlump_location: this.refs.breastlump_location.value,
  //     breastlump_size: this.refs.breastlump_size.value,
  //     overlying_skin: this.refs.overlying_skin.value,
  //     axillarylump: this.refs.axillarylump.value,
  //     axillarylump_side: this.refs.axillarylump_side.value,
  //     matted: this.refs.matted.value,
  //     axillarylump_size: this.refs.axillarylump_size.value,
  //     nipple_discharge: this.refs.nipple_discharge.value,
  //     nipple_discharge_frequency: this.refs.nipple_discharge_frequency.value,
  //     nipple_discharge_colour: this.refs.nipple_discharge_colour.value,
  //     mastalgia: this.refs.mastalgia.value,
  //     mastitis: this.refs.mastitis.value,
  //   }
  //   this.props.saveValues(data);
  //   this.props.nextStep();
 