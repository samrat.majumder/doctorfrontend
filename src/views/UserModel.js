
import React, { Component } from 'react';
import {
    Button,
    Col,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    FormGroup,
    Label,
    Input,
} from 'reactstrap';

class UsersModal extends Component {
    render() {
        return (
            <Row>
                <Col md="6" sm="6" xs="6">

                    <Modal isOpen={true}
                    >
                        <ModalHeader >Create User</ModalHeader>
                        <ModalBody>
                            <FormGroup>
                                <Label for="exampleName">Name</Label>
                                <Input
                                    type='text'
                                    name='name'
                                    placeholder='Enter Your name'
                                />

                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input
                                    type='email'
                                    name='email'
                                    placeholder="Enter Your email"
                                />
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="secondary">
                                Cancel
                </Button>
                            <Button type="submit" color="primary">
                                Submit
                </Button>
                        </ModalFooter>
                    </Modal>
                </Col>
            </Row>
        )
    }
}

export default UsersModal