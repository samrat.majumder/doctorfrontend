

import React, { Component } from 'react';
import BasicInfo from './Details';
import Toxicity from './Toxicity'
import Drug_on from './Drugs_bg';
import Mus_examination from './Mus_examination'
import Rom_examination from './Rom_examination'
import Soft_tis_examination from './Soft_tis_examination'
import Deformities_examination from './Deformities_examination';
import Hypermobility_examination from './Hypermobility_examination';
import Health_ass_ques from './Health_ass_ques';
// import Inves_Hematological from './Inves_Hematological';

// import Inves_Biochemical from './Inves_Biochemical';
// import Inves_Immonological from './Inves_Immonological';
// import Inves_Radiological from './Inves_Radiological';
import axios from 'axios';

class Main extends Component {
    state = {
        step:1,  
        id:'',
        pid:'',
        code :'',
        date:'',
        pname :'',
        age:'',
        sex :'',
        address :'',
        phone :'',
        firstvisit :'',
        lastvisit :'',
        timelapsed :'',
        bp:'',
        wt:'',
        followup :'',
        course_result :'',
        improved_percent:'',
        deteriorate_percent:'',
        history :'',
        diagnosis :'',
        addnewdiagnosis :'',
      
    
        anorexia :'',
        anorexia_duration :'',
        anorexia_severity :'',
        anorexia_cause :'',
        nausea :'',
        nausea_duration :'',
        nausea_severity :'',
        nausea_cause :'',
        vomiting :'',
        vomiting_duration :'',
        vomiting_severity :'',
        vomiting_cause :'',
        pain_abdomen :'',
        pain_duration :'',
        pain_severity :'',
        pain_cause :'',
        diarrhoea :'',
        diarrhoea_duration :'',
        diarrhoea_severity :'',
        diarrhoea_cause :'',
        oral_ulcers :'',
        ulcers_duration :'',
        ulcers_severity :'',
        ulcers_cause :'',
        constipation :'',
        constipation_duration :'',
        constipation_severity :'',
        constipation_cause :'',
        skinrash :'',
        skinrash_duration :'',
        skinrash_severity :'',
        skinrash_cause :'',
        hairfall :'',
        hairfall_duration :'',
        hairfall_severity :'',
        hairfall_cause :'',
        othertoxics :'',
        othertoxics_duration :'',
        othertoxics_severity :'',
        othertoxics_cause :'',
    
        hcq :'',
        mtx :'',
        ssz :'',
        lef :'',
        azt :'',
        mmf :'',
        pred :'',
        nsaid :'',
    
 
        esr :'',
        hb :'',
        tlc :'',
        dlc :'',
        platelet :'',
        urine :'',
        bun :'',
        cr :'',
        sgot :'',
        sgpt :'',
        alb :'',
        glob :'',
        ua :'',
        bsl :'',
        rf :'',
        crp :'',
        bili :'',
        other :'',
        xray :'',
        treatment_plan :'',
        

    
        tm_t1 :'',
        tm_s1 :'',
        tm_t2 :'',
        tm_s2 :'',
        scl_t1 :'',
        scl_s1 :'',
        scl_t2 :'',
        scl_s2 :'',
        acl_t1 :'',
        acl_s1 :'',
        acl_t2 :'',
        acl_s2 :'',
        sh_t1 :'',
        sh_s1 :'',
        sh_t2 :'',
        sh_s2 :'',
        elbow_t1 :'',
        elbow_s1 :'',
        elbow_t2 :'',
        elbow_s2 :'',
        infru_t1 :'',
        infru_s1 :'',
        infru_t2 :'',
        infru_s2 :'',
        cmc1_t1 :'',
        cmc1_s1 :'',
        cmc1_t2 :'',
        cmc1_s2 :'',
        wrist_t1 :'',
        wrist_s1 :'',
        wrist_t2 :'',
        wrist_s2 :'',
        dip2_t1 :'',
        dip2_s1 :'',
        dip2_t2 :'',
        dip2_s2 :'',
        dip3_t1 :'',
        dip3_s1 :'',
        dip3_t2 :'',
        dip3_s2 :'',
        dip4_t1 :'',
        dip4_s1 :'',
        dip4_t2 :'',
        dip4_s2 :'',
        dip5_t1 :'',
        dip5_s1 :'',
        dip5_t2 :'',
        dip5_s2 :'',
        r1ip1_t1 :'',
        r1ip1_s1 :'',
        r1ip1_t2 :'',
        r1ip1_s2 :'',
        r1ip2_t1 :'',
        r1ip2_s1 :'',
        r1ip2_t2 :'',
        r1ip2_s2 :'',
        pip3_t1 :'',
        pip3_s1 :'',
        pip3_t2 :'',
        pip3_s2 :'',
        pip4_t1 :'',
        pip4_s1 :'',
        pip4_t2 :'',
        pip4_s2 :'',
        pip5_t1 :'',
        pip5_s1 :'',
        pip5_t2 :'',
        pip5_s2 :'',
        mcp1_t1 :'',
        mcp1_s1 :'',
        mcp1_t2 :'',
        mcp1_s2 :'',
        mcp2_t1 :'',
        mcp2_s1 :'',
        mcp2_t2 :'',
        mcp2_s2 :'',
    

    
        mcp3_t1 :'',
        mcp3_s1 :'',
        mcp3_t2 :'',
        mcp3_s2 :'',
        mcp4_t1 :'',
        mcp4_s1 :'',
        mcp4_t2 :'',
        mcp4_s2 :'',
        mcp5_t1 :'',
        mcp5_s1 :'',
        mcp5_t2 :'',
        mcp5_s2 :'',
        hip_t1 :'',
        hip_s1 :'',
        hip_t2 :'',
        hip_s2 :'',
        knee_t1 :'',
        knee_s1 :'',
        knee_t2 :'',
        knee_s2 :'',
        a_tt_t1 :'',
        a_tt_s1 :'',
        a_tt_t2 :'',
        a_tt_s2 :'',
        a_tc_t1 :'',
        a_tc_s1 :'',
        a_tc_t2 :'',
        a_tc_s2 :'',
        mtl_t1 :'',
        mtl_s1 :'',
        mtl_t2 :'',
        mtl_s2 :'',
        mtp1_t1 :'',
        mtp1_s1 :'',
        mtp1_t2 :'',
        mtp1_s2 :'',
        mtp2_t1 :'',
        mtp2_s1 :'',
        mtp2_t2 :'',
        mtp2_s2 :'',
        mtp3_t1 :'',
        mtp3_s1 :'',
        mtp3_t2 :'',
        mtp3_s2 :'',
        mtp4_t1 :'',
        mtp4_s1 :'',
        mtp4_t2 :'',
        mtp4_s2 :'',
        mtp5_t1 :'',
        mtp5_s1 :'',
        mtp5_t2 :'',
        mtp5_s2 :'',
        r2ip1_t1 :'',
        r2ip1_s1 :'',
        r2ip1_t2 :'',
        r2ip1_s2 :'',
        r2ip2_t1 :'',
        r2ip2_s1 :'',
        r2ip2_t2 :'',
        r2ip2_s2 :'',
        ip3_t1 :'',
        ip3_s1 :'',
        ip3_t2 :'',
        ip3_s2 :'',
        ip4_t1 :'',
        ip4_s1 :'',
        ip4_t2 :'',
        ip4_s2 :'',
        ip5_t1 :'',
        ip5_s1 :'',
        ip5_t2 :'',
        ip5_s2 :'',
        s1_t1 :'',
        s1_s1 :'',
        s1_t2 :'',
        s1_s2 :'',
    
     
    
        cervical_anky :'',
        cervical_flex :'',
        cervical_ext :'',
        cervical_rtrot :'',
        cervical_ltrot :'',
        cervical_rtfl :'',
        cervical_ltfl :'',
        cervical_pt :'',
        thorasic_anky :'',
        thorasic_flex :'',
        thorasic_ext :'',
        thorasic_rtrot :'',
        thorasic_ltrot :'',
        thorasic_rtfl :'',
        thorasic_ltfl :'',
        thorasic_pt :'',
        lumbar_anky :'',
        lumbar_flex :'',
        lumbar_ext :'',
        lumbar_rtrot :'',
        lumbar_ltrot :'',
        lumbar_rtfl :'',
        lumbar_ltfl :'',
        lumbar_pt :'',
    
        opt_rt :'',
        opt_lt :'',
        lcer_rt :'',
        lcer_lt :'',
        trpz_rt :'',
        trpz_lt :'',
        scap_rt :'',
        scap_lt :'',
        zcst_rt :'',
        zcst_lt :'',
        epdl_rt :'',
        epdl_lt :'',
        glut_rt :'',
        glut_lt :'',
        trcr_rt :'',
        trcr_lt :'',
        knee_rt :'',
        knee_lt :'',
        ta_rt :'',
        ta_lt :'',
        calf_rt :'',
        calf_lt :'',
        sole_rt :'',
        sole_lt :'',
       
    
        rhand_fl :'',
        rhand_ex :'',
        rhand_ab :'',
        rhand_add :'',
        rhand_slx :'',
        lhand_fl :'',
        lhand_ex :'',
        lhand_ab :'',
        lhand_add :'',
        lhand_slx :'',
        rwrist_fl :'',
        rwrist_ex :'',
        rwrist_ab :'',
        rwrist_add :'',
        rwrist_slx :'',
        lwrist_fl :'',
        lwrist_ex :'',
        lwrist_ab :'',
        lwrist_add :'',
        lwrist_slx :'',

        relb_fl :'',
        relb_ex :'',
        relb_ab :'',
        relb_add :'',
        relb_slx :'',

        lelb_fl :'',
        lelb_ex :'',
        lelb_ab :'',
        lelb_add :'',
        lelb_slx :'',


        shrt_fl :'',
        shrt_ex :'',
        shrt_ab :'',
        shrt_add :'',
        shrt_slx :'',
        shlt_fl :'',
        shlt_ex :'',
        shlt_ab :'',
        shlt_add :'',
        shlt_slx :'',
        kneert_fl :'',
        kneert_ex :'',
        kneert_ab :'',
        kneert_add :'',
        kneert_slx :'',
        kneelt_fl :'',
        kneelt_ex :'',
        kneelt_ab :'',
        kneelt_add :'',
        kneelt_slx :'',
        footrt_fl :'',
        footrt_ex :'',
        footrt_ab :'',
        footrt_add :'',
        footrt_slx :'',
        footlt_fl :'',
        footlt_ex :'',
        footlt_ab :'',
        footlt_add :'',
        footlt_slx :'',
    
     
        thumb_rt :'',
        thumb_lt :'',
        finger_rt :'',
        finger_lt :'',
        palm_rt :'',
        palm_lt :'',
        elbow_rt :'',
        elbow_lt :'',
        hy_knee_rt :'',
        hy_knee_lt :'',
        ankle_rt :'',
        ankle_lt :'',
        other_rt :'',
        other_lt :'',
        spine_rt :'',
        spine_lt :'',
    
    
        can_u_dress_urself :'',
        can_u_wash_ur_hair :'',
        can_u_comb_ur_hair :'',
        dressing_score:'',
    
        can_u_stand_from_chair :'',
        can_u_get_inout_from_bed :'',
        can_u_sit_grossteg_onfloor :'',
        arising_score:'',
   
        can_u_cut_vegetables :'',
        can_u_lift_glass :'',
        can_u_break_roti_from_1hand :'',
        eating_score:'',
        can_u_walk :'',
        can_u_climb_5steps :'',
        walking_score:'',
      
        can_u_take_bath :'',
        can_u_wash_dry_urbody :'',
        can_u_get_onoff_toilet :'',
        hygiene_score:'',
      
        can_u_weigh_2kg :'',
        can_u_bend_and_pickcloths :'',
        reaching_score:'',
     
        can_u_open_bottle :'',
        can_u_turntaps_onoff :'',
        can_u_open_latches :'',
        grip_score:'',
      
        can_u_work_office_house :'',
        can_u_run_errands :'',
        can_u_get_inout_of_bus :'',
        activities_score:'',
        
        patient_assessment_pain:'',
        grip_strength_rt:'',
        grip_strength_hg:'',
        grip_strength_lt:'',
        early_mrng_stiffness :'',
        sleep :'',
        general_health_assessment :'',
    
        user_id:'',
    
        rheuma_id:'',
      }
      nextStep = () => {
        const { step } = this.state
        this.setState({
          step: step + 1
        })
      }

  
      prevStep = () => {
        const { step } = this.state
        this.setState({
          step: step - 1
        })
      }
    

      handleChange = input => event => {
        this.setState({ [input]: event.target.value })
      }


      saveAll = () => {

        let Obj={
            id:this.state.id,
            pid:this.state.pid,
            code :this.state.code,
            date:this.state.date,
            pname :this.state.pname,
            age:this.state.age,
            sex :this.state.sex,
            address :this.state.address,
            phone :this.state.phone,
            firstvisit :this.state.firstvisit,
            lastvisit :this.state.lastvisit,
            timelapsed :this.state.timelapsed,
            bp:this.state.bp,
            wt:this.state.wt,
            followup :this.state.followup,
            course_result :this.state.course_result,
            improved_percent:this.state.improved_percent,
            deteriorate_percent:this.state.deteriorate_percent,
            history :this.state.history,
            diagnosis :this.state.diagnosis,
            addnewdiagnosis :this.state.addnewdiagnosis,
          
        
            anorexia :this.state.anorexia,
            anorexia_duration :this.state.anorexia_duration,
            anorexia_severity :this.state.anorexia_severity,
            anorexia_cause :this.state.anorexia_cause,
            nausea :this.state.nausea,
            nausea_duration :this.state.nausea_duration,
            nausea_severity :this.state.nausea_severity,
            nausea_cause :this.state.nausea_cause,
            vomiting :this.state.vomiting,
            vomiting_duration :this.state.vomiting_duration,
            vomiting_severity :this.state.vomiting_severity,
            vomiting_cause :this.state.vomiting_cause,
            pain_abdomen :this.state.pain_abdomen,
            pain_duration :this.state.pain_duration,
            pain_severity :this.state.pain_severity,
            pain_cause :this.state.pain_cause,
            diarrhoea :this.state.diarrhoea,
            diarrhoea_duration :this.state.diarrhoea_duration,
            diarrhoea_severity :this.state.diarrhoea_severity,
            diarrhoea_cause :this.state.diarrhoea_cause,
            oral_ulcers :this.state.oral_ulcers,
            ulcers_duration :this.state.ulcers_duration,
            ulcers_severity :this.state.ulcers_severity,
            ulcers_cause :this.state.ulcers_cause,
            constipation :this.state.constipation,
            constipation_duration :this.state.constipation_duration,
            constipation_severity :this.state.constipation_severity,
            constipation_cause :this.state.constipation_cause,
            skinrash :this.state.skinrash,
            skinrash_duration :this.state.skinrash_duration,
            skinrash_severity :this.state.skinrash_severity,
            skinrash_cause :this.state.skinrash_cause,
            hairfall :this.state.hairfall,
            hairfall_duration :this.state.hairfall_duration,
            hairfall_severity :this.state.hairfall_severity,
            hairfall_cause :this.state.hairfall_cause,
            othertoxics :this.state.othertoxics,
            othertoxics_duration :this.state.othertoxics_duration,
            othertoxics_severity :this.state.othertoxics_severity,
            othertoxics_cause :this.state.othertoxics_cause,
        
            hcq :this.state.hcq,
            mtx :this.state.mtx,
            ssz :this.state.ssz,
            lef :this.state.lef,
            azt :this.state.azt,
            mmf :this.state.mmf,
            pred :this.state.pred,
            nsaid :this.state.nsaid,
        
     
            esr :this.state.esr,
            hb :this.state.hb,
            tlc :this.state.tlc,
            dlc :this.state.dlc,
            platelet :this.state.platelet,
            urine :this.state.urine,
            bun :this.state.bun,
            cr :this.state.cr,
            sgot :this.state.sgot,
            sgpt :this.state.sgpt,
            alb :this.state.alb,
            glob :this.state.glob,
            ua :this.state.ua,
            bsl :this.state.bsl,
            rf :this.state.rf,
            crp :this.state.crp,
            bili :this.state.bili,
            other :this.state.other,
            xray :this.state.xray,
            treatment_plan :this.state.treatment_plan,
        

        


        tm_t1:this.state.tm_t1,
        tm_s1:this.state.tm_s1,
        tm_t2:this.state.tm_t2,
        tm_s2:this.state.tm_s2,

        scl_t1:this.state.scl_t1,
        scl_s1:this.state.scl_s1,
        scl_t2:this.state.scl_t2,
        scl_s2:this.state.scl_s2,



        acl_t1:this.state.acl_t1,
        acl_s1:this.state.acl_s1,
        acl_t2:this.state.acl_t2,
        acl_s2:this.state.acl_s2,


        sh_t1:this.state.sh_t1,
        sh_s1:this.state.sh_s1,
        sh_t2:this.state.sh_t2,
        sh_s2:this.state.sh_s2,

        elbow_t1:this.state.elbow_t1,
        elbow_s1:this.state.elbow_s1,
        elbow_t2:this.state.elbow_t2,
        elbow_s2:this.state.elbow_s2,


        infru_t1:this.state.infru_t1,
        infru_s1:this.state.infru_s1,
        infru_t2:this.state.infru_t2,
        infru_s2:this.state.infru_s2,


        cmc1_t1:this.state.cmc1_t1,
        cmc1_s1:this.state.cmc1_s1,
        cmc1_t2:this.state.cmc1_t2,
        cmc1_s2:this.state.cmc1_s2,



        wrist_t1:this.state.wrist_t1,
        wrist_s1:this.state.wrist_s1,
        wrist_t2:this.state.wrist_t2,
        wrist_s2:this.state.wrist_s2,


        dip2_t1:this.state.dip2_t1,
        dip2_s1:this.state.dip2_s1,
        dip2_t2:this.state.dip2_t2,
        dip2_s2:this.state.dip2_s2,


        dip3_t1:this.state.dip3_t1,
        dip3_s1:this.state.dip3_s1,
        dip3_t2:this.state.dip3_t2,
        dip3_s2:this.state.dip3_s2,


        dip4_t1:this.state.dip4_t1,
        dip4_s1:this.state.dip4_s1,
        dip4_t2:this.state.dip4_t2,
        dip4_s2:this.state.dip4_s2,



        dip5_t1:this.state.dip5_t1,
        dip5_s1:this.state.dip5_s1,
        dip5_t2:this.state.dip5_t2,
        dip5_s2:this.state.dip5_s2,


        ip1_t1:this.state.ip1_t1,
        ip1_s1:this.state.ip1_s1,
        ip1_t2:this.state.ip1_t2,
        ip1_s1:this.state.ip1_s2,


        ip2_t1:this.state.ip2_t1,
        ip2_s1:this.state.ip2_s1,
        ip2_t2:this.state.ip2_t2,
        ip2_s1:this.state.ip2_s2,


        pip3_t1:this.state.pip3_t1,
        pip3_s1:this.state.pip3_s1,
        pip3_t2:this.state.pip3_t2,
        pip3_s1:this.state.pip3_s2,


        pip4_t1:this.state.pip4_t1,
        pip4_s1:this.state.pip4_s1,
        pip4_t2:this.state.pip4_t2,
        pip4_s1:this.state.pip4_s2,

        pip5_t1:this.state.pip5_t1,
        pip5_s1:this.state.pip5_s1,
        pip5_t2:this.state.pip5_t2,
        pip5_s1:this.state.pip5_s2,

        mcp1_t1:this.state.mcp1_t1,
        mcp1_s1:this.state.mcp1_s1,
        mcp1_t2:this.state.mcp1_t2,
        mcp1_s1:this.state.mcp1_s2,


        mcp2_t1:this.state.mcp2_t1,
        mcp2_s1:this.state.mcp2_s1,
        mcp2_t2:this.state.mcp2_t2,
        mcp2_s1:this.state.mcp2_s2,

        mcp3_t1:this.state.mcp3_t1,
        mcp3_s1:this.state.mcp3_s1,
        mcp3_t2:this.state.mcp3_t2,
        mcp3_s1:this.state.mcp3_s2,


        mcp4_t1:this.state.mcp4_t1,
        mcp4_s1:this.state.mcp4_s1,
        mcp4_t2:this.state.mcp4_t2,
        mcp4_s1:this.state.mcp4_s2,

        mcp5_t1:this.state.mcp5_t1,
        mcp5_s1:this.state.mcp5_s1,
        mcp5_t2:this.state.mcp5_t2,
        mcp5_s1:this.state.mcp5_s2,

        hip_t1:this.state.hip_t1,
        hip_s1:this.state.hip_s1,
        hip_t2:this.state.hip_t2,
        hip_s1:this.state.hip_s2,


        knee_t1:this.state.knee_t1,
        knee_s1:this.state.knee_s1,
        knee_t2:this.state.knee_t2,
        knee_s1:this.state.knee_s2,


        a_tt_t1:this.state.a_tt_t1,
        a_tt_s1:this.state.a_tt_s1,
        a_tt_t2:this.state.a_tt_t2,
        a_tt_s1:this.state.a_tt_s2,

        a_tc_t1:this.state.a_tc_t1,
        a_tc_s1:this.state.a_tc_s1,
        a_tc_t2:this.state.a_tc_t2,
        a_tc_s1:this.state.a_tc_s2,

        mtl_t1:this.state.mtl_t1,
        mtl_s1:this.state.mtl_s1,
        mtl_t2:this.state.mtl_t2,
        mtl_s1:this.state.mtl_s2,


        mtp1_t1:this.state.mtp1_t1,
        mtp1_s1:this.state.mtp1_s1,
        mtp1_t2:this.state.mtp1_t2,
        mtp1_s1:this.state.mtp1_s2,


        mtp2_t1:this.state.mtp2_t1,
        mtp2_s1:this.state.mtp2_s1,
        mtp2_t2:this.state.mtp2_t2,
        mtp2_s1:this.state.mtp2_s2,


        mtp3_t1:this.state.mtp3_t1,
        mtp3_s1:this.state.mtp3_s1,
        mtp3_t2:this.state.mtp3_t2,
        mtp3_s1:this.state.mtp3_s2,


        mtp4_t1:this.state.mtp4_t1,
        mtp4_s1:this.state.mtp4_s1,
        mtp4_t2:this.state.mtp4_t2,
        mtp4_s1:this.state.mtp4_s2,


        mtp5_t1:this.state.mtp5_t1,
        mtp5_s1:this.state.mtp5_s1,
        mtp5_t2:this.state.mtp5_t2,
        mtp5_s1:this.state.mtp5_s2,


        ip1b_t1:this.state.ip1b_t1,
        ip1b_s1:this.state.ip1b_s1,
        ip1b_t2:this.state.ip1b_t2,
        ip1b_s1:this.state.ip1b_s2,


        ip2b_t1:this.state.ip2b_t1,
        ip2b_s1:this.state.ip2b_s1,
        ip2b_t2:this.state.ip2b_t2,
        ip2b_s1:this.state.ip2b_s2,


        ip3b_t1:this.state.ip3b_t1,
        ip3b_s1:this.state.ip3b_s1,
        ip3b_t2:this.state.ip3b_t2,
        ip3b_s1:this.state.ip3b_s2,

        ip4b_t1:this.state.ip4b_t1,
        ip4b_s1:this.state.ip4b_s1,
        ip4b_t2:this.state.ip4b_t2,
        ip4b_s1:this.state.ip4b_s2,


        ip5b_t1:this.state.ip5b_t1,
        ip5b_s1:this.state.ip5b_s1,
        ip5b_t2:this.state.ip5b_t2,
        ip5b_s1:this.state.ip5b_s2,


        s1_t1:this.state.s1_t1,
        s1_s1:this.state.s1_s1,
        s1_t2:this.state.s1_t2,
        s1_s1:this.state.s1_s2,


        cervical_anky:this.state.cervical_anky,
        cervical_flex:this.state.cervical_flex,
        cervical_ext:this.state.cervical_ext,
        cervical_rtrot:this.state.cervical_rtrot,

        cervical_ltrot:this.state.cervical_ltrot,
        cervical_rtfl:this.state.cervical_rtfl,
        cervical_ltfl:this.state.cervical_ltfl,
        cervical_pt:this.state.cervical_pt,


        thorasic_anky:this.state.thorasic_anky,
        thorasic_flex:this.state.thorasic_flex,
        thorasic_ext:this.state.thorasic_ext,
        thorasic_rtrot:this.state.thorasic_rtrot,

        thorasic_ltrot:this.state.thorasic_ltrot,
        thorasic_rtfl:this.state.thorasic_rtfl,
        thorasic_ltfl:this.state.thorasic_ltfl,
        thorasic_pt:this.state.thorasic_pt,





        lumbar_anky:this.state.lumbar_anky,
        lumbar_flex:this.state.lumbar_flex,
        lumbar_ext:this.state.lumbar_ext,
        lumbar_rtrot:this.state.lumbar_rtrot,
        lumbar_ltrot:this.state.lumbar_ltrot,



        lumbar_rtfl:this.state.lumbar_rtfl,
        lumbar_ltfl:this.state.lumbar_ltfl,
        lumbar_pt:this.state.lumbar_pt,


        opt_rt:this.state.opt_rt,
        opt_lt:this.state.opt_lt,

        lcer_rt:this.state.lcer_rt,
        lcer_lt:this.state.lcer_lt,
        trpz_rt:this.state.trpz_rt,


        trpz_lt:this.state.trpz_lt,
        scap_rt:this.state.scap_rt,
        scap_lt:this.state.scap_lt,
 
        zcst_rt:this.state.zcst_rt,
        zcst_lt:this.state.zcst_lt,
        epdl_rt:this.state.epdl_rt,


        epdl_lt:this.state.epdl_lt,
        glut_rt:this.state.glut_rt,
        glut_lt:this.state.glut_lt,


        trcr_rt:this.state.trcr_rt,
        trcr_lt:this.state.trcr_lt,
        knee_rt:this.state.knee_rt,




        knee_lt:this.state.knee_lt,
        ta_rt:this.state.ta_rt,
        ta_lt:this.state.ta_lt,


        calf_rt:this.state.calf_rt,
        calf_lt:this.state.calf_lt,
        sole_rt:this.state.sole_rt,


        sole_lt:this.state.sole_lt,
        rhand_fl:this.state.rhand_fl,
        rhand_ex:this.state.rhand_ex,




        rhand_ab:this.state.rhand_ab,
        rhand_add:this.state.rhand_add,
        lhand_fl:this.state.lhand_fl,


        lhand_ex:this.state.lhand_ex,
        lhand_ab:this.state.lhand_ab,
        lhand_add:this.state.lhand_add,



        rwrist_fl:this.state.rwrist_fl,
        rwrist_ex:this.state.rwrist_ex,
        rwrist_ab:this.state.rwrist_ab,
        rwrist_add:this.state.rwrist_add,
        lwrist_fl:this.state.lwrist_fl,


        lwrist_ex:this.state.lwrist_ex,
        lwrist_ab:this.state.lwrist_ab,
        lwrist_add:this.state.lwrist_add,


        relb_fl:this.state.relb_fl,
        relb_ex:this.state.relb_ex,
        relb_ab:this.state.relb_ab,
        relb_add:this.state.relb_add,
        relb_slx:this.state.relb_slx,

        lelb_fl :this.state.lelb_fl,
        lelb_ex :this.state.lhand_ex,
        lelb_ab :this.state.lwrist_ab,
        lelb_add :this.state.lwrist_add,
        lelb_slx :this.state.lwrist_slx,


        shrt_fl:this.state.shrt_fl,
        shrt_ex:this.state.shrt_ex,
        shrt_ab:this.state.shrt_ab,
        shrt_add:this.state.shrt_add,


        shlt_fl:this.state.shlt_fl,
        shlt_ex:this.state.shlt_ex,
        shlt_ab:this.state.shlt_ab,
        shlt_add:this.state.shlt_add,


        kneert_fl:this.state.kneert_fl,
        kneert_ex:this.state.kneert_ex,
        kneert_ab:this.state.kneert_ab,
        kneert_add:this.state.kneert_add,


        kneelt_fl:this.state.kneelt_fl,
        kneelt_ex:this.state.kneelt_ex,
        kneelt_ab:this.state.kneelt_ab,
        kneelt_add:this.state.kneelt_add,


        footrt_fl:this.state.footrt_fl,
        footrt_ex:this.state.footrt_ex,
        footrt_ab:this.state.footrt_ab,
        footrt_add:this.state.footrt_add,
        footlt_fl:this.state.footlt_fl,


        footlt_ex:this.state.footlt_ex,
        footlt_ab:this.state.footlt_ab,
        footlt_add:this.state.footlt_add,



        thumb_rt:this.state.thumb_rt,
        thumb_lt:this.state.thumb_lt,
        finger_rt:this.state.finger_rt,


        finger_lt:this.state.finger_lt,
        palm_rt:this.state.palm_rt,
        palm_lt:this.state.palm_lt,




        elbow_rt:this.state.elbow_rt,
        elbow_lt:this.state.elbow_lt,
        hy_knee_rt:this.state.hy_knee_rt,



        hy_knee_lt:this.state.hy_knee_lt,
        ankle_rt:this.state.ankle_rt,
        ankle_lt:this.state.ankle_lt,


        other_rt:this.state.other_rt,
        other_lt:this.state.other_lt,
        spine_rt:this.state.spine_rt,







        spine_lt:this.state.spine_lt,

        can_u_dress_urself:this.state.can_u_dress_urself,
        can_u_wash_ur_hair:this.state.can_u_wash_ur_hair,
        can_u_comb_ur_hair:this.state.can_u_comb_ur_hair,
        dressing_score:this.state.dressing_score,


        can_u_stand_from_chair:this.state.can_u_stand_from_chair,
        can_u_get_inout_from_bed:this.state.can_u_get_inout_from_bed,
        can_u_sit_grossteg_onfloor:this.state.can_u_sit_grossteg_onfloor,
        arising_score:this.state.arising_score,



        can_u_cut_vegetables:this.state.can_u_cut_vegetables,
        can_u_lift_glass:this.state.can_u_lift_glass,
        can_u_break_roti_from_1hand:this.state.can_u_break_roti_from_1hand,
        eating_score:this.state.eating_score,



        can_u_walk:this.state.can_u_walk,
        can_u_climb_5steps:this.state.can_u_climb_5steps,
        walking_score:this.state.walking_score,



        can_u_take_bath:this.state.can_u_take_bath,
        can_u_wash_dry_urbody:this.state.can_u_wash_dry_urbody,
        can_u_get_onoff_toilet:this.state.can_u_get_onoff_toilet,
        hygiene_score:this.state.hygiene_score,


        can_u_weigh_2kg:this.state.can_u_weigh_2kg,
        can_u_bend_and_pickcloths:this.state.can_u_bend_and_pickcloths,
        reaching_score:this.state.reaching_score,


        can_u_open_bottle:this.state.can_u_open_bottle,
        can_u_turntaps_onoff:this.state.can_u_turntaps_onoff,  
        can_u_open_latches:this.state.can_u_open_latches,
        grip_score:this.state.grip_score,


        can_u_work_office_house:this.state.can_u_work_office_house,
        can_u_run_errands:this.state.can_u_run_errands,
        can_u_get_inout_of_bus:this.state.can_u_get_inout_of_bus,
        activities_score:this.state.activities_score,


        patient_assessment_pain:this.state.patient_assessment_pain,


        grip_strength_rt:this.state.grip_strength_rt,
        grip_strength_hg:this.state.grip_strength_hg,
        grip_strength_lt:this.state.grip_strength_lt,


        early_mrng_stiffness:this.state.early_mrng_stiffness,
        assess_sleep:this.state.assess_sleep,
        general_health_assessment:this.state.general_health_assessment,



        user_id:this.state.user_id,
        rheuma_id:this.state.rheuma_id,




          
        }
            axios.post(`https://abcapi.vidaria.in/addbpatient`,Obj)
            .then(res => {
            console.log(res);
            console.log(res.data);
      })
      
      }

  
  
	render() {
		const { step } = this.state;
		const {
            id,
            pid,
            code ,
            date,
            pname ,
            age,
            sex ,
            address ,
            phone ,
            firstvisit ,
            lastvisit ,
            timelapsed ,
            bp,
            wt,
            followup ,
            course_result ,
            improved_percent,
            deteriorate_percent,
            history ,
            diagnosis ,
            addnewdiagnosis ,
          
        
            anorexia ,
            anorexia_duration ,
            anorexia_severity ,
            anorexia_cause ,
            nausea ,
            nausea_duration ,
            nausea_severity ,
            nausea_cause ,
            vomiting ,
            vomiting_duration ,
            vomiting_severity ,
            vomiting_cause ,
            pain_abdomen ,
            pain_duration ,
            pain_severity ,
            pain_cause ,
            diarrhoea ,
            diarrhoea_duration ,
            diarrhoea_severity ,
            diarrhoea_cause ,
            oral_ulcers ,
            ulcers_duration ,
            ulcers_severity ,
            ulcers_cause ,
            constipation ,
            constipation_duration ,
            constipation_severity ,
            constipation_cause ,
            skinrash ,
            skinrash_duration ,
            skinrash_severity ,
            skinrash_cause ,
            hairfall ,
            hairfall_duration ,
            hairfall_severity ,
            hairfall_cause ,
            othertoxics ,
            othertoxics_duration ,
            othertoxics_severity ,
            othertoxics_cause ,
        
            hcq ,
            mtx ,
            ssz ,
            lef ,
            azt ,
            mmf ,
            pred ,
            nsaid ,
        
     
            esr ,
            hb ,
            tlc ,
            dlc ,
            platelet ,
            urine ,
            bun ,
            cr ,
            sgot ,
            sgpt ,
            alb ,
            glob ,
            ua ,
            bsl ,
            rf ,
            crp ,
            bili ,
            other ,
            xray ,
            treatment_plan ,
            
    
        
            tm_t1 ,
            tm_s1 ,
            tm_t2 ,
            tm_s2 ,
            scl_t1 ,
            scl_s1 ,
            scl_t2 ,
            scl_s2 ,
            acl_t1 ,
            acl_s1 ,
            acl_t2 ,
            acl_s2 ,
            sh_t1 ,
            sh_s1 ,
            sh_t2 ,
            sh_s2 ,
            elbow_t1 ,
            elbow_s1 ,
            elbow_t2 ,
            elbow_s2 ,
            infru_t1 ,
            infru_s1 ,
            infru_t2 ,
            infru_s2 ,
            cmc1_t1 ,
            cmc1_s1 ,
            cmc1_t2 ,
            cmc1_s2 ,
            wrist_t1 ,
            wrist_s1 ,
            wrist_t2 ,
            wrist_s2 ,
            dip2_t1 ,
            dip2_s1 ,
            dip2_t2 ,
            dip2_s2 ,
            dip3_t1 ,
            dip3_s1 ,
            dip3_t2 ,
            dip3_s2 ,
            dip4_t1 ,
            dip4_s1 ,
            dip4_t2 ,
            dip4_s2 ,
            dip5_t1 ,
            dip5_s1 ,
            dip5_t2 ,
            dip5_s2 ,
            r1ip1_t1 ,
            r1ip1_s1 ,
            r1ip1_t2 ,
            r1ip1_s2 ,
            r1ip2_t1 ,
            r1ip2_s1 ,
            r1ip2_t2 ,
            r1ip2_s2 ,
            pip3_t1 ,
            pip3_s1 ,
            pip3_t2 ,
            pip3_s2 ,
            pip4_t1 ,
            pip4_s1 ,
            pip4_t2 ,
            pip4_s2 ,
            pip5_t1 ,
            pip5_s1 ,
            pip5_t2 ,
            pip5_s2 ,
            mcp1_t1 ,
            mcp1_s1 ,
            mcp1_t2 ,
            mcp1_s2 ,
            mcp2_t1 ,
            mcp2_s1 ,
            mcp2_t2 ,
            mcp2_s2 ,
        
    
        
            mcp3_t1 ,
            mcp3_s1 ,
            mcp3_t2 ,
            mcp3_s2 ,
            mcp4_t1 ,
            mcp4_s1 ,
            mcp4_t2 ,
            mcp4_s2 ,
            mcp5_t1 ,
            mcp5_s1 ,
            mcp5_t2 ,
            mcp5_s2 ,
            hip_t1 ,
            hip_s1 ,
            hip_t2 ,
            hip_s2 ,
            knee_t1 ,
            knee_s1 ,
            knee_t2 ,
            knee_s2 ,
            a_tt_t1 ,
            a_tt_s1 ,
            a_tt_t2 ,
            a_tt_s2 ,
            a_tc_t1 ,
            a_tc_s1 ,
            a_tc_t2 ,
            a_tc_s2 ,
            mtl_t1 ,
            mtl_s1 ,
            mtl_t2 ,
            mtl_s2 ,
            mtp1_t1 ,
            mtp1_s1 ,
            mtp1_t2 ,
            mtp1_s2 ,
            mtp2_t1 ,
            mtp2_s1 ,
            mtp2_t2 ,
            mtp2_s2 ,
            mtp3_t1 ,
            mtp3_s1 ,
            mtp3_t2 ,
            mtp3_s2 ,
            mtp4_t1 ,
            mtp4_s1 ,
            mtp4_t2 ,
            mtp4_s2 ,
            mtp5_t1 ,
            mtp5_s1 ,
            mtp5_t2 ,
            mtp5_s2 ,
            r2ip1_t1 ,
            r2ip1_s1 ,
            r2ip1_t2 ,
            r2ip1_s2 ,
            r2ip2_t1 ,
            r2ip2_s1 ,
            r2ip2_t2 ,
            r2ip2_s2 ,
            ip3_t1 ,
            ip3_s1 ,
            ip3_t2 ,
            ip3_s2 ,
            ip4_t1 ,
            ip4_s1 ,
            ip4_t2 ,
            ip4_s2 ,
            ip5_t1 ,
            ip5_s1 ,
            ip5_t2 ,
            ip5_s2 ,
            s1_t1 ,
            s1_s1 ,
            s1_t2 ,
            s1_s2 ,
        
         
        
            cervical_anky ,
            cervical_flex ,
            cervical_ext ,
            cervical_rtrot ,
            cervical_ltrot ,
            cervical_rtfl ,
            cervical_ltfl ,
            cervical_pt ,
            thorasic_anky ,
            thorasic_flex ,
            thorasic_ext ,
            thorasic_rtrot ,
            thorasic_ltrot ,
            thorasic_rtfl ,
            thorasic_ltfl ,
            thorasic_pt ,
            lumbar_anky ,
            lumbar_flex ,
            lumbar_ext ,
            lumbar_rtrot ,
            lumbar_ltrot ,
            lumbar_rtfl ,
            lumbar_ltfl ,
            lumbar_pt ,
        
            opt_rt ,
            opt_lt ,
            lcer_rt ,
            lcer_lt ,
            trpz_rt ,
            trpz_lt ,
            scap_rt ,
            scap_lt ,
            zcst_rt ,
            zcst_lt ,
            epdl_rt ,
            epdl_lt ,
            glut_rt ,
            glut_lt ,
            trcr_rt ,
            trcr_lt ,
            knee_rt ,
            knee_lt ,
            ta_rt ,
            ta_lt ,
            calf_rt ,
            calf_lt ,
            sole_rt ,
            sole_lt ,
           
        
            rhand_fl ,
            rhand_ex ,
            rhand_ab ,
            rhand_add ,
            rhand_slx ,
            lhand_fl ,
            lhand_ex ,
            lhand_ab ,
            lhand_add ,
            lhand_slx ,
            rwrist_fl ,
            rwrist_ex ,
            rwrist_ab ,
            rwrist_add ,
            rwrist_slx ,
            lwrist_fl ,
            lwrist_ex ,
            lwrist_ab ,
            lwrist_add ,
            lwrist_slx ,
    
            relb_fl ,
            relb_ex ,
            relb_ab ,
            relb_add ,
            relb_slx ,
    
            lelb_fl ,
            lelb_ex ,
            lelb_ab ,
            lelb_add ,
            lelb_slx ,
    
    
            shrt_fl ,
            shrt_ex ,
            shrt_ab ,
            shrt_add ,
            shrt_slx ,
            shlt_fl ,
            shlt_ex ,
            shlt_ab ,
            shlt_add ,
            shlt_slx ,
            kneert_fl ,
            kneert_ex ,
            kneert_ab ,
            kneert_add ,
            kneert_slx ,
            kneelt_fl ,
            kneelt_ex ,
            kneelt_ab ,
            kneelt_add ,
            kneelt_slx ,
            footrt_fl ,
            footrt_ex ,
            footrt_ab ,
            footrt_add ,
            footrt_slx ,
            footlt_fl ,
            footlt_ex ,
            footlt_ab ,
            footlt_add ,
            footlt_slx ,
        
         
            thumb_rt ,
            thumb_lt ,
            finger_rt ,
            finger_lt ,
            palm_rt ,
            palm_lt ,
            elbow_rt ,
            elbow_lt ,
            hy_knee_rt ,
            hy_knee_lt ,
            ankle_rt ,
            ankle_lt ,
            other_rt ,
            other_lt ,
            spine_rt ,
            spine_lt ,
        
        
            can_u_dress_urself ,
            can_u_wash_ur_hair ,
            can_u_comb_ur_hair ,
            dressing_score,
        
            can_u_stand_from_chair ,
            can_u_get_inout_from_bed ,
            can_u_sit_grossteg_onfloor ,
            arising_score,
       
            can_u_cut_vegetables ,
            can_u_lift_glass ,
            can_u_break_roti_from_1hand ,
            eating_score,
            can_u_walk ,
            can_u_climb_5steps ,
            walking_score,
          
            can_u_take_bath ,
            can_u_wash_dry_urbody ,
            can_u_get_onoff_toilet ,
            hygiene_score,
          
            can_u_weigh_2kg ,
            can_u_bend_and_pickcloths ,
            reaching_score,
         
            can_u_open_bottle ,
            can_u_turntaps_onoff ,
            can_u_open_latches ,
            grip_score,
          
            can_u_work_office_house ,
            can_u_run_errands ,
            can_u_get_inout_of_bus ,
            activities_score,
            
            patient_assessment_pain,
            grip_strength_rt,
            grip_strength_hg,
            grip_strength_lt,
            early_mrng_stiffness ,
            sleep ,
            general_health_assessment ,
        
            user_id,
        
            rheuma_id,
      
      } = this.state;

		const values = { 
            id,
            pid,
            code ,
            date,
            pname ,
            age,
            sex ,
            address ,
            phone ,
            firstvisit ,
            lastvisit ,
            timelapsed ,
            bp,
            wt,
            followup ,
            course_result ,
            improved_percent,
            deteriorate_percent,
            history ,
            diagnosis ,
            addnewdiagnosis ,
          
        
            anorexia ,
            anorexia_duration ,
            anorexia_severity ,
            anorexia_cause ,
            nausea ,
            nausea_duration ,
            nausea_severity ,
            nausea_cause ,
            vomiting ,
            vomiting_duration ,
            vomiting_severity ,
            vomiting_cause ,
            pain_abdomen ,
            pain_duration ,
            pain_severity ,
            pain_cause ,
            diarrhoea ,
            diarrhoea_duration ,
            diarrhoea_severity ,
            diarrhoea_cause ,
            oral_ulcers ,
            ulcers_duration ,
            ulcers_severity ,
            ulcers_cause ,
            constipation ,
            constipation_duration ,
            constipation_severity ,
            constipation_cause ,
            skinrash ,
            skinrash_duration ,
            skinrash_severity ,
            skinrash_cause ,
            hairfall ,
            hairfall_duration ,
            hairfall_severity ,
            hairfall_cause ,
            othertoxics ,
            othertoxics_duration ,
            othertoxics_severity ,
            othertoxics_cause ,
        
            hcq ,
            mtx ,
            ssz ,
            lef ,
            azt ,
            mmf ,
            pred ,
            nsaid ,
        
     
            esr ,
            hb ,
            tlc ,
            dlc ,
            platelet ,
            urine ,
            bun ,
            cr ,
            sgot ,
            sgpt ,
            alb ,
            glob ,
            ua ,
            bsl ,
            rf ,
            crp ,
            bili ,
            other ,
            xray ,
            treatment_plan ,
            
    
        
            tm_t1 ,
            tm_s1 ,
            tm_t2 ,
            tm_s2 ,
            scl_t1 ,
            scl_s1 ,
            scl_t2 ,
            scl_s2 ,
            acl_t1 ,
            acl_s1 ,
            acl_t2 ,
            acl_s2 ,
            sh_t1 ,
            sh_s1 ,
            sh_t2 ,
            sh_s2 ,
            elbow_t1 ,
            elbow_s1 ,
            elbow_t2 ,
            elbow_s2 ,
            infru_t1 ,
            infru_s1 ,
            infru_t2 ,
            infru_s2 ,
            cmc1_t1 ,
            cmc1_s1 ,
            cmc1_t2 ,
            cmc1_s2 ,
            wrist_t1 ,
            wrist_s1 ,
            wrist_t2 ,
            wrist_s2 ,
            dip2_t1 ,
            dip2_s1 ,
            dip2_t2 ,
            dip2_s2 ,
            dip3_t1 ,
            dip3_s1 ,
            dip3_t2 ,
            dip3_s2 ,
            dip4_t1 ,
            dip4_s1 ,
            dip4_t2 ,
            dip4_s2 ,
            dip5_t1 ,
            dip5_s1 ,
            dip5_t2 ,
            dip5_s2 ,
            r1ip1_t1 ,
            r1ip1_s1 ,
            r1ip1_t2 ,
            r1ip1_s2 ,
            r1ip2_t1 ,
            r1ip2_s1 ,
            r1ip2_t2 ,
            r1ip2_s2 ,
            pip3_t1 ,
            pip3_s1 ,
            pip3_t2 ,
            pip3_s2 ,
            pip4_t1 ,
            pip4_s1 ,
            pip4_t2 ,
            pip4_s2 ,
            pip5_t1 ,
            pip5_s1 ,
            pip5_t2 ,
            pip5_s2 ,
            mcp1_t1 ,
            mcp1_s1 ,
            mcp1_t2 ,
            mcp1_s2 ,
            mcp2_t1 ,
            mcp2_s1 ,
            mcp2_t2 ,
            mcp2_s2 ,
        
    
        
            mcp3_t1 ,
            mcp3_s1 ,
            mcp3_t2 ,
            mcp3_s2 ,
            mcp4_t1 ,
            mcp4_s1 ,
            mcp4_t2 ,
            mcp4_s2 ,
            mcp5_t1 ,
            mcp5_s1 ,
            mcp5_t2 ,
            mcp5_s2 ,
            hip_t1 ,
            hip_s1 ,
            hip_t2 ,
            hip_s2 ,
            knee_t1 ,
            knee_s1 ,
            knee_t2 ,
            knee_s2 ,
            a_tt_t1 ,
            a_tt_s1 ,
            a_tt_t2 ,
            a_tt_s2 ,
            a_tc_t1 ,
            a_tc_s1 ,
            a_tc_t2 ,
            a_tc_s2 ,
            mtl_t1 ,
            mtl_s1 ,
            mtl_t2 ,
            mtl_s2 ,
            mtp1_t1 ,
            mtp1_s1 ,
            mtp1_t2 ,
            mtp1_s2 ,
            mtp2_t1 ,
            mtp2_s1 ,
            mtp2_t2 ,
            mtp2_s2 ,
            mtp3_t1 ,
            mtp3_s1 ,
            mtp3_t2 ,
            mtp3_s2 ,
            mtp4_t1 ,
            mtp4_s1 ,
            mtp4_t2 ,
            mtp4_s2 ,
            mtp5_t1 ,
            mtp5_s1 ,
            mtp5_t2 ,
            mtp5_s2 ,
            r2ip1_t1 ,
            r2ip1_s1 ,
            r2ip1_t2 ,
            r2ip1_s2 ,
            r2ip2_t1 ,
            r2ip2_s1 ,
            r2ip2_t2 ,
            r2ip2_s2 ,
            ip3_t1 ,
            ip3_s1 ,
            ip3_t2 ,
            ip3_s2 ,
            ip4_t1 ,
            ip4_s1 ,
            ip4_t2 ,
            ip4_s2 ,
            ip5_t1 ,
            ip5_s1 ,
            ip5_t2 ,
            ip5_s2 ,
            s1_t1 ,
            s1_s1 ,
            s1_t2 ,
            s1_s2 ,
        
         
        
            cervical_anky ,
            cervical_flex ,
            cervical_ext ,
            cervical_rtrot ,
            cervical_ltrot ,
            cervical_rtfl ,
            cervical_ltfl ,
            cervical_pt ,
            thorasic_anky ,
            thorasic_flex ,
            thorasic_ext ,
            thorasic_rtrot ,
            thorasic_ltrot ,
            thorasic_rtfl ,
            thorasic_ltfl ,
            thorasic_pt ,
            lumbar_anky ,
            lumbar_flex ,
            lumbar_ext ,
            lumbar_rtrot ,
            lumbar_ltrot ,
            lumbar_rtfl ,
            lumbar_ltfl ,
            lumbar_pt ,
        
            opt_rt ,
            opt_lt ,
            lcer_rt ,
            lcer_lt ,
            trpz_rt ,
            trpz_lt ,
            scap_rt ,
            scap_lt ,
            zcst_rt ,
            zcst_lt ,
            epdl_rt ,
            epdl_lt ,
            glut_rt ,
            glut_lt ,
            trcr_rt ,
            trcr_lt ,
            knee_rt ,
            knee_lt ,
            ta_rt ,
            ta_lt ,
            calf_rt ,
            calf_lt ,
            sole_rt ,
            sole_lt ,
           
        
            rhand_fl ,
            rhand_ex ,
            rhand_ab ,
            rhand_add ,
            rhand_slx ,
            lhand_fl ,
            lhand_ex ,
            lhand_ab ,
            lhand_add ,
            lhand_slx ,
            rwrist_fl ,
            rwrist_ex ,
            rwrist_ab ,
            rwrist_add ,
            rwrist_slx ,
            lwrist_fl ,
            lwrist_ex ,
            lwrist_ab ,
            lwrist_add ,
            lwrist_slx ,
    
            relb_fl ,
            relb_ex ,
            relb_ab ,
            relb_add ,
            relb_slx ,
    
            lelb_fl ,
            lelb_ex ,
            lelb_ab ,
            lelb_add ,
            lelb_slx ,
    
    
            shrt_fl ,
            shrt_ex ,
            shrt_ab ,
            shrt_add ,
            shrt_slx ,
            shlt_fl ,
            shlt_ex ,
            shlt_ab ,
            shlt_add ,
            shlt_slx ,
            kneert_fl ,
            kneert_ex ,
            kneert_ab ,
            kneert_add ,
            kneert_slx ,
            kneelt_fl ,
            kneelt_ex ,
            kneelt_ab ,
            kneelt_add ,
            kneelt_slx ,
            footrt_fl ,
            footrt_ex ,
            footrt_ab ,
            footrt_add ,
            footrt_slx ,
            footlt_fl ,
            footlt_ex ,
            footlt_ab ,
            footlt_add ,
            footlt_slx ,
        
         
            thumb_rt ,
            thumb_lt ,
            finger_rt ,
            finger_lt ,
            palm_rt ,
            palm_lt ,
            elbow_rt ,
            elbow_lt ,
            hy_knee_rt ,
            hy_knee_lt ,
            ankle_rt ,
            ankle_lt ,
            other_rt ,
            other_lt ,
            spine_rt ,
            spine_lt ,
        
        
            can_u_dress_urself ,
            can_u_wash_ur_hair ,
            can_u_comb_ur_hair ,
            dressing_score,
        
            can_u_stand_from_chair ,
            can_u_get_inout_from_bed ,
            can_u_sit_grossteg_onfloor ,
            arising_score,
       
            can_u_cut_vegetables ,
            can_u_lift_glass ,
            can_u_break_roti_from_1hand ,
            eating_score,
            can_u_walk ,
            can_u_climb_5steps ,
            walking_score,
          
            can_u_take_bath ,
            can_u_wash_dry_urbody ,
            can_u_get_onoff_toilet ,
            hygiene_score,
          
            can_u_weigh_2kg ,
            can_u_bend_and_pickcloths ,
            reaching_score,
         
            can_u_open_bottle ,
            can_u_turntaps_onoff ,
            can_u_open_latches ,
            grip_score,
          
            can_u_work_office_house ,
            can_u_run_errands ,
            can_u_get_inout_of_bus ,
            activities_score,
            
            patient_assessment_pain,
            grip_strength_rt,
            grip_strength_hg,
            grip_strength_lt,
            early_mrng_stiffness ,
            sleep ,
            general_health_assessment ,
        
            user_id,
        
            rheuma_id,

    
    };
		switch (step) {
			case 1:
				return <BasicInfo
					nextStep={this.nextStep}
					handleChange={this.handleChange}
					values={values}
				/>


      case 2:
        return <Toxicity
            nextStep={this.nextStep}
            prevStep={this.prevStep}

            handleChange={this.handleChange}
            values={values}
          />


      case 3:
        return <Drug_on
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
              />


			case 4:
				return <Mus_examination
					nextStep={this.nextStep}
					prevStep={this.prevStep}
					handleChange={this.handleChange}
					values={values}
				/>


      case 5:
        return <Rom_examination
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
          
          

      case 6:
        return <Soft_tis_examination
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                handleChange={this.handleChange}
                values={values}
              /> 


      case 7:
        return <Deformities_examination
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    handleChange={this.handleChange}
                    values={values}
                  />         
			case 8:
				return <Hypermobility_examination
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
        />




      case 9:
        return <Health_ass_ques
                 nextStep={this.nextStep}
                 prevStep={this.prevStep}
                 handleChange={this.handleChange}
                 values={values}
                 saveAll={this.saveAll.bind(this)} 

          />



      case 10:
				return <Inves_Hematological
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
        />



      case 11:
				return <Inves_Biochemical
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
        />


      case 12:
				return <Inves_Immonological
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
        />

      case 13:
        return <Inves_Radiological
                 nextStep={this.nextStep}
                 prevStep={this.prevStep}
                 handleChange={this.handleChange}
                 values={values}
          />



      // case 12:
			// 	return <Summary 
			// 		   nextStep={this.nextStep}
			// 		   prevStep={this.prevStep}
			// 		   values={values}
      //        saveAll={this.saveAll.bind(this)} 
      //        />
	
		}
	}
}

export default Main;