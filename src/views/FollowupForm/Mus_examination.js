
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Mus_examination extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
         <h5>joint_evaluation: Information</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>T</th>
      <th>S</th>
      <th>RT LT</th>
      <th>T</th>
      <th>S</th>
      <th>T</th>
      <th>S</th>
      <th>RT  LT</th>
      <th>T</th>
      <th>S</th>

    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input type="text" 						onChange={this.props.handleChange('tm_t1')}
defaultValue={values.tm_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('tm_s1')}
defaultValue={values.tm_s1} maxlength="50" size="4" /></td>
      <td>TM</td>
      <td><input type="text" 						onChange={this.props.handleChange('tm_t2')}
defaultValue={values.tm_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('tm_s2')}
defaultValue={values.tm_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mcp3_t1')}
defaultValue={values.mcp3_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mcp3_s1')}
defaultValue={values.mcp3_s1} maxlength="50" size="4" /></td>

      <td>MCP3</td>
            <td><input type="text" 						onChange={this.props.handleChange('mcp3_t2')}
defaultValue={values.mcp3_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mcp3_s2')}
defaultValue={values.mcp3_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('scl_t1')}
defaultValue={values.scl_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('scl_s1')}
defaultValue={values.scl_s1} maxlength="50" size="4" /></td>
      <td>SCL</td>
      <td><input type="text" 						onChange={this.props.handleChange('scl_t2')}
defaultValue={values.scl_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('scl_s2')}
defaultValue={values.scl_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mcp4_t1')}
defaultValue={values.mcp4_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mcp4_s1')}
defaultValue={values.mcp4_s1} maxlength="50" size="4" /></td>

      <td>MCP 4</td>
            <td><input type="text" 						onChange={this.props.handleChange('mcp4_t2')}
defaultValue={values.mcp4_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mcp4_s2')}
defaultValue={values.mcp4_s2} maxlength="50" size="4" /></td>


    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('acl_t1')}
defaultValue={values.acl_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('acl_s1')}
defaultValue={values.acl_s1} maxlength="50" size="4" /></td>
      <td>ACL</td>
      <td><input type="text" 						onChange={this.props.handleChange('acl_t2')}
defaultValue={values.acl_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('acl_s2')}
defaultValue={values.acl_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mcp5_t1')}
defaultValue={values.mcp5_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mcp5_s1')}
defaultValue={values.mcp5_s1} maxlength="50" size="4" /></td>

      <td>MCP 5</td>
      <td><input type="text" 						onChange={this.props.handleChange('mcp5_t2')}
defaultValue={values.mcp5_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mcp5_s2')}
defaultValue={values.mcp5_s2} maxlength="50" size="4" /></td>

    </tr>

    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('sh_t1')}
defaultValue={values.sh_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('sh_s1')}
defaultValue={values.sh_s1} maxlength="50" size="4" /></td>
      <td>SH</td>
      <td><input type="text" 						onChange={this.props.handleChange('sh_t2')}
defaultValue={values.sh_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('sh_s2')}
defaultValue={values.sh_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hip_t1')}
defaultValue={values.hip_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('hip_s1')}
defaultValue={values.hip_s1} maxlength="50" size="4" /></td>

      <td>HIP</td>
            <td><input type="text" 						onChange={this.props.handleChange('hip_t2')}
defaultValue={values.hip_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hip_s2')}
defaultValue={values.hip_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('elbow_t1')}
defaultValue={values.elbow_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('elbow_s1')}
defaultValue={values.elbow_s1} maxlength="50" size="4" /></td>
      <td>ELBOW</td>
      <td><input type="text" 						onChange={this.props.handleChange('elbow_t2')}
defaultValue={values.elbow_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('elbow_s2')}
defaultValue={values.elbow_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('knee_t1')}
defaultValue={values.knee_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('knee_s1')}
defaultValue={values.knee_s1} maxlength="50" size="4" /></td>

      <td>KNEE</td>
            <td><input type="text" 						onChange={this.props.handleChange('knee_t2')}
defaultValue={values.knee_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('knee_s2')}
defaultValue={values.knee_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('infru_t1')}
defaultValue={values.infru_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('infru_s1')}
defaultValue={values.infru_s1} maxlength="50" size="4" /></td>
      <td>INF RU</td>
      <td><input type="text" 						onChange={this.props.handleChange('infru_t2')}
defaultValue={values.infru_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('infru_s2')}
defaultValue={values.infru_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('a_tt_t1')}
defaultValue={values.a_tt_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('a_tt_s1')}
defaultValue={values.a_tt_s1} maxlength="50" size="4" /></td>

      <td>A(TT)</td>
            <td><input type="text" 						onChange={this.props.handleChange('a_tt_t2')}
defaultValue={values.a_tt_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('a_tt_s2')}
defaultValue={values.a_tt_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('cmc1_t1')}
defaultValue={values.cmc1_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('cmc1_s1')}
defaultValue={values.cmc1_s1} maxlength="50" size="4" /></td>
      <td>CMC 1</td>
      <td><input type="text" 						onChange={this.props.handleChange('cmc1_t2')}
defaultValue={values.cmc1_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('cmc1_s2')}
defaultValue={values.cmc1_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('a_tc_t1')}
defaultValue={values.a_tc_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('a_tc_s1')}
defaultValue={values.a_tc_s1} maxlength="50" size="4" /></td>

      <td>A(TC)</td>
            <td><input type="text" 						onChange={this.props.handleChange('a_tc_t2')}
defaultValue={values.a_tc_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('a_tc_s2')}
defaultValue={values.a_tc_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('wrist_t1')}
defaultValue={values.wrist_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('wrist_s1')}
defaultValue={values.wrist_s1} maxlength="50" size="4" /></td>
      <td>WRIST</td>
      <td><input type="text" 						onChange={this.props.handleChange('wrist_t2')}
defaultValue={values.wrist_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('wrist_s2')}
defaultValue={values.wrist_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtl_t1')}
defaultValue={values.mtl_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mtl_s1')}
defaultValue={values.mtl_s1} maxlength="50" size="4" /></td>

      <td>MTL</td>
            <td><input type="text" 						onChange={this.props.handleChange('mtl_t2')}
defaultValue={values.mtl_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtl_s2')}
defaultValue={values.mtl_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('dip2_t1')}
defaultValue={values.dip2_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('dip2_s1')}
defaultValue={values.dip2_s1} maxlength="50" size="4" /></td>
      <td>DIP2</td>
      <td><input type="text" 						onChange={this.props.handleChange('dip2_t2')}
defaultValue={values.dip2_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('dip2_s2')}
defaultValue={values.dip2_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp1_t1')}
defaultValue={values.mtp1_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mtp1_s1')}
defaultValue={values.mtp1_s1} maxlength="50" size="4" /></td>

      <td>MTP 1</td>
            <td><input type="text" 						onChange={this.props.handleChange('mtp1_t2')}
defaultValue={values.mtp1_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp1_s2')}
defaultValue={values.mtp1_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('dip3_t1')}
defaultValue={values.dip3_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('dip3_s1')}
defaultValue={values.dip3_s1} maxlength="50" size="4" /></td>
      <td>DIP3</td>
      <td><input type="text" 						onChange={this.props.handleChange('dip3_t2')}
defaultValue={values.dip3_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('dip3_s2')}
defaultValue={values.dip3_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp2_t1')}
defaultValue={values.mtp2_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mtp2_s1')}
defaultValue={values.mtp2_s1} maxlength="50" size="4" /></td>

      <td>MTP 2</td>
            <td><input type="text" 						onChange={this.props.handleChange('mtp2_t2')}
defaultValue={values.mtp2_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp2_s2')}
defaultValue={values.mtp2_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('dip4_t1')}
defaultValue={values.dip4_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('dip4_s1')}
defaultValue={values.dip4_s1} maxlength="50" size="4" /></td>
      <td>DIP4</td>
      <td><input type="text" 						onChange={this.props.handleChange('dip4_t2')}
defaultValue={values.dip4_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('dip4_s2')}
defaultValue={values.dip4_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp3_t1')}
defaultValue={values.mtp3_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mtp3_s1')}
defaultValue={values.mtp3_s1} maxlength="50" size="4" /></td>

      <td>MTP 3</td>
            <td><input type="text" 						onChange={this.props.handleChange('mtp3_t2')}
defaultValue={values.mtp3_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp3_s2')}
defaultValue={values.mtp3_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('dip5_t1')}
defaultValue={values.dip5_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('dip5_s1')}
defaultValue={values.dip5_s1} maxlength="50" size="4" /></td>
      <td>DIP5</td>
      <td><input type="text" 						onChange={this.props.handleChange('dip5_t2')}
defaultValue={values.dip5_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('dip5_s2')}
defaultValue={values.dip5_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp4_t1')}
defaultValue={values.mtp4_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mtp4_s1')}
defaultValue={values.mtp4_s1} maxlength="50" size="4" /></td>

      <td>MTP 4</td>
            <td><input type="text" 						onChange={this.props.handleChange('mtp4_t2')}
defaultValue={values.mtp4_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp4_s2')}
defaultValue={values.mtp4_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('ip1b_t1')}
defaultValue={values.ip1b_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('ip1b_s1')}
defaultValue={values.ip1b_s1} maxlength="50" size="4" /></td>
      <td>IP 1</td>
      <td><input type="text" 						onChange={this.props.handleChange('ip1b_t2')}
defaultValue={values.ip1b_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('ip1b_s2')}
defaultValue={values.ip1b_s2} maxlength="50" size="4" /></td>



            <td><input type="text" 						onChange={this.props.handleChange('mtp5_t1')}
defaultValue={values.mtp5_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('mtp5_s1')}
defaultValue={values.mtp5_s1} maxlength="50" size="4" /></td>

      <td>MTP 5</td>
            <td><input type="text" 						onChange={this.props.handleChange('mtp5_t2')}
defaultValue={values.mtp5_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('mtp5_s2')}
defaultValue={values.mtp5_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('ip2_t1')}
defaultValue={values.ip2_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('ip2_s1')}
defaultValue={values.ip2_s1} maxlength="50" size="4" /></td>
      <td>IP 2</td>
      <td><input type="text" 						onChange={this.props.handleChange('ip2_t2')}
defaultValue={values.ip2_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('ip2_s2')}
defaultValue={values.ip2_s2} maxlength="50" size="4" /></td>



            <td><input type="text" 						onChange={this.props.handleChange('ip1b_t1')}
defaultValue={values.ip1b_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('ip1b_s1')}
defaultValue={values.ip1b_s1} maxlength="50" size="4" /></td>

      <td>IP 1</td>
            <td><input type="text" 						onChange={this.props.handleChange('ip1b_t2')}
defaultValue={values.ip1b_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip1b_s1')}
defaultValue={values.ip1b_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('pip3_t1')}
defaultValue={values.pip3_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('pip3_s1')}
defaultValue={values.pip3_s1} maxlength="50" size="4" /></td>
      <td>PIP 3</td>
      <td><input type="text" 						onChange={this.props.handleChange('pip3_t2')}
defaultValue={values.pip3_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('pip3_s2')}
defaultValue={values.pip3_s2} maxlength="50" size="4" /></td>



            <td><input type="text" 						onChange={this.props.handleChange('ip2b_t1')}
defaultValue={values.ip2b_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('ip2b_s1')}
defaultValue={values.ip2b_s1} maxlength="50" size="4" /></td>

      <td>IP 2</td>
            <td><input type="text" 						onChange={this.props.handleChange('ip2b_t2')}
defaultValue={values.ip2b_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip2b_s2')}
defaultValue={values.ip2b_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('pip4_t1')}
defaultValue={values.pip4_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('pip4_s1')}
defaultValue={values.pip4_s1} maxlength="50" size="4" /></td>
      <td>PIP 4</td>
      <td><input type="text" 						onChange={this.props.handleChange('pip4_t2')}
defaultValue={values.pip4_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('pip4_s2')}
defaultValue={values.pip4_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip3b_t1')}
defaultValue={values.ip3b_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('ip3b_s1')}
defaultValue={values.ip3b_s1} maxlength="50" size="4" /></td>

      <td>IP 3</td>
            <td><input type="text" 						onChange={this.props.handleChange('ip3b_t2')}
defaultValue={values.ip3b_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip3b_s2')}
defaultValue={values.ip3b_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('pip5_t1')}
defaultValue={values.pip5_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('pip5_s1')}
defaultValue={values.pip5_s1} maxlength="50" size="4" /></td>
      <td>PIP 5</td>
      <td><input type="text" 						onChange={this.props.handleChange('pip5_t2')}
defaultValue={values.pip5_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('pip5_s2')}
defaultValue={values.pip5_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip4b_t1')}
defaultValue={values.ip4b_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('ip4b_s1')}
defaultValue={values.ip4b_s1} maxlength="50" size="4" /></td>

      <td>IP 4</td>
            <td><input type="text" 						onChange={this.props.handleChange('ip4b_t2')}
defaultValue={values.ip4b_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip4b_s2')}
defaultValue={values.ip4b_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('mcp1_t1')}
defaultValue={values.mcp1_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('mcp1_s1')}
defaultValue={values.mcp1_s1} maxlength="50" size="4" /></td>
      <td>MCP 1</td>
      <td><input type="text" 						onChange={this.props.handleChange('mcp1_t2')}
defaultValue={values.mcp1_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('mcp1_s2')}
defaultValue={values.mcp1_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip5b_t1')}
defaultValue={values.ip5b_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('ip5b_s1')}
defaultValue={values.ip5b_s1} maxlength="50" size="4" /></td>

      <td>IP 5</td>
            <td><input type="text" 						onChange={this.props.handleChange('ip5b_t2')}
defaultValue={values.ip5b_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ip5b_s2')}
defaultValue={values.ip5b_s2} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text" 						onChange={this.props.handleChange('mcp2_t1')}
defaultValue={values.mcp2_t1} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('mcp2_s1')}
defaultValue={values.mcp2_s1} maxlength="50" size="4" /></td>
      <td>MCP 2</td>
      <td><input type="text" 						onChange={this.props.handleChange('mcp2_t2')}
defaultValue={values.mcp2_t2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('mcp2_s2')}
defaultValue={values.mcp2_s2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('s1_t1')}
defaultValue={values.s1_t1} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('s1_s1')}
defaultValue={values.s1_s1} maxlength="50" size="4" /></td>

      <td>S1</td>
            <td><input type="text" 						onChange={this.props.handleChange('s1_t2')}
defaultValue={values.s1_t2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('s1_s2')}
defaultValue={values.s1_s2} maxlength="50" size="4" /></td>

    </tr>
   
  </tbody>
</Table>
<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>

        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Mus_examination;







