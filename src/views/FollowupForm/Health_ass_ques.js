
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Health_ass_ques extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
         <h5>Health_assessment_questionaire:(Use 0,1,2 and 3)</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>are you able to </th>
      <th></th>
      <th></th>
      <th></th>
      
      <th>Score</th>
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>Dressing</td>
      <td>
          <label>Dress yourself</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={this.props.handleChange('can_u_dress_urself')}
defaultValue={values.can_u_dress_urself} maxlength="50" size="5" /></td>
     
     <td>          <label>Wash Your Hair</label><br></br>

         <input type="text" placeholder="yes/no"						onChange={this.props.handleChange('can_u_wash_ur_hair')}
defaultValue={values.can_u_wash_ur_hair} maxlength="50" size="5" /></td>

   
            <td>          <label>Comb your Hair</label><br></br>

                
                <input type="text" 		placeholder="yes/no"				onChange={this.props.handleChange('can_u_comb_ur_hair')}
defaultValue={values.can_u_comb_ur_hair} maxlength="50" size="5" /></td>

            <td><input type="number" 						onChange={this.props.handleChange('dressing_score')}
defaultValue={values.dressing_score} maxlength="50" size="5" /></td>

    </tr>


    <tr>
      <td>Arising</td>
      <td>      <label>Stand From Chair</label><br></br>
          <input type="text" placeholder="yes/no" 	onChange={this.props.handleChange('can_u_stand_from_chair')}
defaultValue={values.can_u_stand_from_chair} maxlength="50" size="5" /></td>
     
     <td><label>Get in out From Bed</label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_get_inout_from_bed')}
defaultValue={values.can_u_get_inout_from_bed} maxlength="50" size="5" /></td>

     
            <td><label>Sit Gross teg on Floor</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_sit_grossteg_onfloor')}
defaultValue={values.can_u_sit_grossteg_onfloor} maxlength="50" size="5" /></td>

            <td><input type="number" 						onChange={this.props.handleChange('arising_score')}
defaultValue={values.arising_score} maxlength="50" size="5" /></td>

    </tr>


    <tr>
      <td>Eating</td>
      <td><label>Cut Vegetables</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={this.props.handleChange('can_u_cut_vegetables')}
defaultValue={values.can_u_cut_vegetables} maxlength="50" size="5" /></td>
     
     <td><label>Lift a glass to your mouth</label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_lift_glass')}
defaultValue={values.can_u_lift_glass} maxlength="50" size="5" /></td>

    
            <td><label>Break Roti from 1 hand</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_break_roti_from_1hand')}
defaultValue={values.can_u_break_roti_from_1hand} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={this.props.handleChange('eating_score')}
defaultValue={values.eating_score} maxlength="50" size="5" /></td>

    </tr>

    <tr>
      <td>Walking</td>
      <td><label>Walking Outdoors</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={this.props.handleChange('can_u_walk')}
defaultValue={values.can_u_walk} maxlength="50" size="5" /></td>
     
     <td><label>Climb up 5 Steps</label><br></br>
         <input type="text" placeholder="yes/no"						onChange={this.props.handleChange('can_u_climb_5steps')}
defaultValue={values.can_u_climb_5steps} maxlength="50" size="5" /></td>

   
            <td></td>


<td>
    <input type="number" 				onChange={this.props.handleChange('walking_score')}
defaultValue={values.walking_score} maxlength="50" size="5" /></td>

    </tr>
   


    <tr>
      <td>Hygiene</td>
      <td><label>Take a Bath</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={this.props.handleChange('can_u_take_bath')}
defaultValue={values.can_u_take_bath} maxlength="50" size="5" /></td>
     
     <td><label>Was and dry your body </label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_wash_dry_urbody')}
defaultValue={values.can_u_wash_dry_urbody} maxlength="50" size="5" /></td>

    
            <td><label>Get on and off the toilet</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_get_onoff_toilet')}
defaultValue={values.can_u_get_onoff_toilet} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={this.props.handleChange('hygiene_score')}
defaultValue={values.hygiene_score} maxlength="50" size="5" /></td>

    </tr>
   


    <tr>
      <td>Reaching</td>
      <td><label>Reach and get down a 2 kg objects</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={this.props.handleChange('can_u_weigh_2kg')}
defaultValue={values.can_u_weigh_2kg} maxlength="50" size="5" /></td>
     
     <td><label>Bend down to pick up clothing from floor </label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_bend_and_pickcloths')}
defaultValue={values.can_u_bend_and_pickcloths} maxlength="50" size="5" /></td>

    
            <td></td>

            <td>
                <input type="number" 						onChange={this.props.handleChange('reaching_score')}
defaultValue={values.reaching_score} maxlength="50" size="5" /></td>

    </tr>



    <tr>
      <td>Grip</td>
      <td><label>Open a bottle previously opened</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={this.props.handleChange('can_u_open_bottle')}
defaultValue={values.can_u_open_bottle} maxlength="50" size="5" /></td>
     
     <td><label>Turn taps on and off </label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_turntaps_onoff')}
defaultValue={values.can_u_turntaps_onoff} maxlength="50" size="5" /></td>

    
            <td><label>Open done Latches</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_open_latches')}
defaultValue={values.can_u_open_latches} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={this.props.handleChange('grip_score')}
defaultValue={values.grip_score} maxlength="50" size="5" /></td>

    </tr>



    <tr>
      <td>Activities</td>
      <td><label>Work in office/house</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={this.props.handleChange('can_u_work_office_house')}
defaultValue={values.can_u_work_office_house} maxlength="50" size="5" /></td>
     
     <td><label>Run errands and shop</label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_run_errands')}
defaultValue={values.can_u_run_errands} maxlength="50" size="5" /></td>

    
            <td><label>Get in and out of bus</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={this.props.handleChange('can_u_get_inout_of_bus')}
defaultValue={values.can_u_get_inout_of_bus} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={this.props.handleChange('activities_score')}
defaultValue={values.activities_score} maxlength="50" size="5" /></td>

    </tr>
  </tbody>
</Table><br></br>



<h4>Modified CRD Pune Version HAQ Score</h4>

<Table striped bordered hover>
  <thead>
    <tr>
      <th>Modified CRD Pune Version HAQ Score</th>
      <th>/24</th>
      
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>Patient assessment pain(VSA)</td>
      <td><input type="number" 	onChange={this.props.handleChange('patient_assessment_pain')}
defaultValue={values.patient_assessment_pain} maxlength="50" size="10" />cm(on a scale of 10cm)</td>
     

    </tr>
    <tr>
      <td>Grip strength</td>
      <td><input type="number" 	onChange={this.props.handleChange('grip_strength_rt')}
defaultValue={values.grip_strength_rt} maxlength="50" size="10" />MM HG of RT  <br></br><br></br>
 <input type="number" 	onChange={this.props.handleChange('grip_strength_hg')}
defaultValue={values.grip_strength_hg} maxlength="50" size="10" />MM HG of LT
</td>


 
    </tr>


    <tr>
      <td>Early morning stiffness</td>
      <td><input type="number" 	onChange={this.props.handleChange('early_mrng_stiffness')}
defaultValue={values.early_mrng_stiffness} maxlength="50" size="10" />Mins</td>

 
    </tr>

    <tr>
      <td>Sleep</td>
      <td><input type="text" 	onChange={this.props.handleChange('assess_sleep')}
defaultValue={values.assess_sleep} maxlength="50" size="20"/> (normal/distributed/excess)</td>


    </tr>
   
    <tr>
      <td>General health assessment</td>
      <td><input type="number" 	onChange={this.props.handleChange('general_health_assessment')}
defaultValue={values.general_health_assessment} maxlength="50" size="10" />mm (on a 100 cm scale)</td>


    </tr>
   
  </tbody>
</Table>
<br></br>

<Row>
          <Col md="6">
        


          <Form.Group>
          <Form.Label>User ID</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('user_id')}  defaultValue={values.user_id}/>
          </Form.Group>


         
        </Col>

        
        <Col md="6">
       



          <Form.Group>
          <Form.Label>Rheumatology ID</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('rheuma_id')} defaultValue={values.rheuma_id}/>
          </Form.Group>


          
          </Col>
          </Row>
<br></br>



<Row>
<Col md="6">
<Button className="btn-fill"   onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button className="btn-fill" variant="success" onClick={this.props.saveAll}>Submit</Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Health_ass_ques;







