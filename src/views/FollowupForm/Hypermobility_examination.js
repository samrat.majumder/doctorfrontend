
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Hypermobility_examination extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
         <h5>Hypermobility: Information</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th></th>
      <th>RT</th>
      <th>LT</th>
      <th></th>
      <th>RT</th>
      <th>LT</th>
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>THUMB</td>
      <td><input type="text" 	onChange={this.props.handleChange('thumb_rt')}
defaultValue={values.thumb_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('thumb_lt')}
defaultValue={values.thumb_lt} maxlength="50" size="4" /></td>

      <td>KNEE</td>
            <td><input type="text" 						onChange={this.props.handleChange('hy_knee_rt')}
defaultValue={values.hy_knee_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hy_knee_lt')}
defaultValue={values.hy_knee_lt} maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>FINGER</td>
      <td><input type="text" 	onChange={this.props.handleChange('finger_rt')}
defaultValue={values.finger_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('finger_lt')}
defaultValue={values.finger_lt} maxlength="50" size="4" /></td>

      <td>ANKLE</td>
            <td><input type="text" 						onChange={this.props.handleChange('ankle_rt')}
defaultValue={values.ankle_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ankle_lt')}
defaultValue={values.ankle_lt} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td>PALM</td>
      <td><input type="text" 	onChange={this.props.handleChange('palm_rt')}
defaultValue={values.palm_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('palm_lt')}
defaultValue={values.palm_lt} maxlength="50" size="4" /></td>

      <td>OTHERS</td>
            <td><input type="text" 						onChange={this.props.handleChange('other_rt')}
defaultValue={values.other_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('other_lt')}
defaultValue={values.other_lt} maxlength="50" size="4" /></td>

    </tr>

    <tr>
      <td>ELBOW</td>
      <td><input type="text" 	onChange={this.props.handleChange('elbow_rt')}
defaultValue={values.elbow_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('elbow_lt')}
defaultValue={values.elbow_lt} maxlength="50" size="4" /></td>

      <td>SPINE</td>
            <td><input type="text" 						onChange={this.props.handleChange('spine_rt')}
defaultValue={values.spine_rt} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('spine_lt')}
defaultValue={values.spine_lt} maxlength="50" size="4" /></td>

    </tr>
   
   
  </tbody>
</Table>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Hypermobility_examination;







